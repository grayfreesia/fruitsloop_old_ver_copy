﻿using System;

public class ExecutionStatusChangedEventArgs : EventArgs
{
	#region Public Variables
	public CourseState.ExecutionStatus ExecutionStatus;
	#endregion

	#region Constructors
	public ExecutionStatusChangedEventArgs (CourseState.ExecutionStatus executionStatus)
	{
		ExecutionStatus = executionStatus;
	}
	#endregion
}
