﻿using System;

public class SessionChangedEventArgs : EventArgs
{
	public readonly CourseState.Session CurrentSession;

	public SessionChangedEventArgs (CourseState.Session currentSession)
	{
		CurrentSession = currentSession;
	}
}
