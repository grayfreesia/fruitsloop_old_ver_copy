﻿using System;

public class UnitChangedEventArgs : EventArgs
{
	#region Public Variables
	public readonly int CurrentUnitIndex;
	#endregion

	#region Constructors
	public UnitChangedEventArgs (int currentUnitIndex)
	{
		CurrentUnitIndex = currentUnitIndex;
	}
	#endregion
}
