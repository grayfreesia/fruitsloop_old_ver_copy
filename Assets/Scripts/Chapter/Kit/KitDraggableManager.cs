﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KitDraggableManager : MonoBehaviour
{
    KitDraggable[] kits;
	public static KitDraggableManager Instance{get; private set;}
	void Awake()
	{
		if(Instance == null)
		{
			Instance = this;
		}
	}
    void Start()
    {
        kits = GetComponentsInChildren<KitDraggable>();
        SetKitNum();
		SetInteractable(0);
    }

    void SetKitNum()
    {
        for (int i = 0; i < kits.Length; i++)
        {
            kits[i].kitNum = i;
        }
    }

    public void SetInteractable(int index)
    {
        for (int i = 0; i < kits.Length; i++)
        {
            if (i == index)
            {
                kits[i].GetComponent<SimpleButton>().EnableInteraction(true);
                // kits[i].GetComponentsInChildren<Image>()[0].raycastTarget = true;
				// kits[i].GetComponentsInChildren<Image>()[1].color = new Color(1, 1, 1, 1);
            }
            else
            {
                kits[i].GetComponent<SimpleButton>().EnableInteraction(false);
				// kits[i].GetComponentsInChildren<Image>()[0].raycastTarget = false;
				// kits[i].GetComponentsInChildren<Image>()[1].color = new Color(1, 1, 1, 0.5f);				
            }
        }
    }

}
