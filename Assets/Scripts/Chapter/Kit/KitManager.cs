﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitManager : MonoBehaviour
{
    public KitSlot[] slots;
    public KitDraggable draggingKit;
    public int currentNum = 0;
    public static KitManager Instance { get; private set; }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        slots = GetComponentsInChildren<KitSlot>();
    }

    void Start()
    {
        if (SceneController.Instance.ActiveSceneName.Equals("Tutorial")) {
            return;
        }
        
        SetKitArrange(PlayerPrefs.GetInt(SceneController.Instance.ActiveSceneName + "KitNum", 0));
    }

    public void KitUndo()
    {
        if (currentNum > 0)
        {
            SetKitArrange(currentNum - 1);
            PlayerPrefs.SetInt(SceneController.Instance.ActiveSceneName + "KitNum", currentNum);
        }
    }

    void SetKitArrange(int num)
    {
        currentNum = num;
        for (int i = 0; i < slots.Length; i++)
        {
            if (i < num)
            {
                slots[i].SetTransparancy(1f);
                slots[i].isAssembled = true;
                slots[i].SetLine(true);
            }
            else
            {
                slots[i].SetTransparancy(0f);
                slots[i].isAssembled = false;
                slots[i].SetLine(false);
            }
        }

        if (KitDraggableManager.Instance)
        {
            KitDraggableManager.Instance.SetInteractable(currentNum);
        }
    }

}
