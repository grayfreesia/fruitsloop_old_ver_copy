﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class KitSlot : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{

    Image image;
    public Image range;
    public bool isStandModule = false;
    public bool isAssembled = false;
    void Awake()
    {
        image = GetComponent<Image>();
        SetTransparancy(0f);
        SetLine (false);
        SetRaycastTarget(false);       
    }
    
    public void OnDrop(PointerEventData data)
    {
        if (data.dragging)
        {
            if (KitManager.Instance.draggingKit != null)
            {
                KitManager.Instance.draggingKit.OnDropTarget(this);
            }
        }
    }

    public void OnPointerEnter(PointerEventData data)
    {
        if (data.dragging)
        {
            if (KitManager.Instance.draggingKit != null)
            {
                KitManager.Instance.draggingKit.OnEnterTarget(this);
            }
        }
    }

    public void OnPointerExit(PointerEventData data)
    {
        if (data.dragging)
        {
            if (KitManager.Instance.draggingKit != null)
            {
                KitManager.Instance.draggingKit.OnExitTarget(this);
            }
        }
    }

    public void SetTransparancy(float num)
    {
        image.color = new Color(1f, 1f, 1f, num);
        if(isStandModule && num == 1f)
        {
            image.color = new Color(1f, 1f, 1f, 0f);
        }
    }

    public void SetRaycastTarget(bool on)
    {
        image.raycastTarget = on;
    }

    public void SetLine(bool on)
    {
        Transform[] lines = GetComponentsInChildren<Transform>(true);
        for(int i = 1; i < lines.Length; i++)
        {
            lines[i].gameObject.SetActive(on);
        }
        if(GetComponent<NeoPixel>() != null && on)
        {
            GetComponent<NeoPixel>().OffModule();
        }
    }

    public void SetRange(bool on)
    {
        if(range != null)
        {
            range.gameObject.SetActive(on);
        }
    }
}
