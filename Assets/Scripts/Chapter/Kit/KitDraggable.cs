﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class KitDraggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    public int kitNum = 0;
    public Vector2 draggingOffset = new Vector2(0F, 0f);
    private Image draggingImage;
    private Timer timer = new Timer(0.1f);
    private bool isPointerDown = false;
    private PointerEventData cashedPointerEventData;
    private bool isImageCreated = false;
    public bool IsImageCreated
    {
        get { return isImageCreated; }
        set { isImageCreated = value; }
    }
    private RectTransform canvasRectTransform;

    private ScrollRect scrollRectParent;
    private ScrollRect ScrollRectParent
    {
        get
        {
            if (scrollRectParent == null)
            {
                scrollRectParent = GetComponentInParent<ScrollRect>();
            }
            return scrollRectParent;
        }
    }

    #region Unity Methods
    private void Update()
    {
        if (isPointerDown)
        {
            // 블록 생성 버튼 다운시 타이머를 잰다.
            timer.Update(Time.deltaTime);
            if (timer.IsTimeout)
            {
                // 지정된 타이머 시간이 지나면
                // 블록 생성
                CreateDraggingImage(cashedPointerEventData);
                isImageCreated = true;

                // 타이머 초기화
                isPointerDown = false;
                timer.Reset();

                // 진동
                Vibration.Vibrate(40);
            }
        }
    }
    #endregion

    #region Private Methods
    private void CreateDraggingImage(PointerEventData pointerEventData)
    {
        if (draggingImage != null)
        {
            Destroy(draggingImage.gameObject);
        }

        draggingImage = Instantiate(KitManager.Instance.slots[kitNum].GetComponent<Image>(), transform.root) as Image;
        draggingImage.color = Color.white;
        draggingImage.transform.localScale *= KitManager.Instance.transform.localScale.x;
        draggingImage.gameObject.transform.SetParent(transform.root);
        draggingImage.gameObject.transform.SetAsLastSibling();
        draggingImage.raycastTarget = false;
        canvasRectTransform = draggingImage.gameObject.transform as RectTransform;

        KitManager.Instance.slots[kitNum].SetRaycastTarget(true);
        if (kitNum == KitManager.Instance.currentNum)
        {
            KitManager.Instance.slots[kitNum].SetTransparancy(.5f);
            KitManager.Instance.slots[kitNum].SetRange(true);

        }
        KitManager.Instance.draggingKit = this;

        UpdateDraggingImagePos(pointerEventData);
    }

    private void UpdateDraggingImagePos(PointerEventData pointerEventData)
    {
        if (draggingImage != null)
        {
            // 드래그 중인 아이콘의 스크린 좌표를 계산한다.
            Vector3 screenPos = pointerEventData.position + draggingOffset;
            // 스크린 좌표를 월드 좌표로 변환한다.
            Camera camera = pointerEventData.pressEventCamera;
            Vector3 newPos;
            if (RectTransformUtility.ScreenPointToWorldPointInRectangle(
                canvasRectTransform, screenPos, camera, out newPos))
            {
                // 드래그 중인 아이콘의 위치를 월드 좌표로 설정한다.
                draggingImage.transform.position = newPos;
                draggingImage.transform.rotation = canvasRectTransform.rotation;
            }
        }
        // draggingImage.gameObject.transform.position = pointerEventData.position;
    }
    #endregion

    #region Event handlers
    public void OnPointerDown(PointerEventData pointerEventData)
    {
        isPointerDown = true;
        cashedPointerEventData = pointerEventData;
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        isPointerDown = false;
        timer.Reset();
        if (draggingImage != null)
        {
            Destroy(draggingImage.gameObject);
            isImageCreated = false;

            KitManager.Instance.slots[kitNum].SetRaycastTarget(false);
            if (!KitManager.Instance.slots[kitNum].isAssembled && kitNum == KitManager.Instance.currentNum)
            {
                KitManager.Instance.slots[kitNum].SetTransparancy(0f);
                KitManager.Instance.slots[kitNum].SetRange(false);

            }
            //KitManager.Instance.draggingKit = null;
        }
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        isPointerDown = false;
        timer.Reset();
    }

    public void OnBeginDrag(PointerEventData pointerEventData)
    {
        if (!isImageCreated)
        {
            if (ScrollRectParent)
            {
                ScrollRectParent.OnBeginDrag(pointerEventData);
            }
        }
    }

    public void OnDrag(PointerEventData pointerEventData)
    {
        if (isImageCreated)
        {
            UpdateDraggingImagePos(pointerEventData);
        }
        else
        {
            if (ScrollRectParent)
            {
                ScrollRectParent.OnDrag(pointerEventData);
            }

            if (Mathf.Abs(pointerEventData.delta.y) > 4f)
            {
                isPointerDown = false;
                timer.Reset();
            }
        }
    }

    public void OnEndDrag(PointerEventData pointerEventData)
    {
        isImageCreated = false;

        if (draggingImage != null)
        {
            Destroy(draggingImage.gameObject);

            KitManager.Instance.slots[kitNum].SetRaycastTarget(false);
            if (!KitManager.Instance.slots[kitNum].isAssembled && kitNum == KitManager.Instance.currentNum)
            {
                KitManager.Instance.slots[kitNum].SetTransparancy(0f);
                KitManager.Instance.slots[kitNum].SetRange(false);

            }
            //KitManager.Instance.draggingKit = null;
        }

        if (ScrollRectParent)
        {
            ScrollRectParent.OnEndDrag(pointerEventData);
        }
    }
    #endregion

    #region Public Methods
    public void OnEnterTarget(KitSlot slot)
    {
        if (slot == KitManager.Instance.slots[kitNum] && kitNum == KitManager.Instance.currentNum)
        {
            slot.SetTransparancy(.8f);
        }
    }

    public void OnExitTarget(KitSlot slot)
    {
        if (slot == KitManager.Instance.slots[kitNum] && kitNum == KitManager.Instance.currentNum)
        {
            slot.SetTransparancy(.5f);
        }
    }

    public void OnDropTarget(KitSlot slot)
    {
        if (slot == KitManager.Instance.slots[kitNum] && kitNum == KitManager.Instance.currentNum)
        {
            slot.SetTransparancy(1f);
            slot.isAssembled = true;
            slot.SetLine(true);
            KitManager.Instance.currentNum++;
            if (KitDraggableManager.Instance)
            {
                KitDraggableManager.Instance.SetInteractable(KitManager.Instance.currentNum);
            }

            if (SceneController.Instance.ActiveSceneName.Equals("Tutorial")) {
                return;
            }

            PlayerPrefs.SetInt(SceneController.Instance.ActiveSceneName + "KitNum", KitManager.Instance.currentNum);
            //if complete
            if (KitManager.Instance.currentNum == KitManager.Instance.slots.Length)
            {
                string completeTitle = "후르츠루프";
                string completeMessage = "회로를 다 완성했어요.\n블록코딩 탭으로 이동 할까요?";
                AlertController.Show(completeTitle, completeMessage, new AlertOptions
                {
                    cancelButtonTitle = "머무르기",
                    okButtonTitle = "이동하기",
                    okButtonDelegate = () =>
                    {
                        CourseState.Instance.CurrentSession = CourseState.Session.BlockCoding;
                    },
                    animate = false,
                });
            }
        }
    }
    #endregion
}
