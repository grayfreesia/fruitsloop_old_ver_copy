﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CourseState : MonoBehaviour
{
    #region Public Variables
    public enum Session { Circuit, BlockCoding, Assembly }
    public enum ExecutionStatus { Stop, Play, Pause }
    #endregion

    #region Property
    public static CourseState Instance { get; private set; }

    private Session currentSession = Session.Circuit;
    public Session CurrentSession
    {
        get { return currentSession; }
        set
        {
            if (currentSession == value)
            {
                return;
            }
            currentSession = value;
            OnSessionChanged(new SessionChangedEventArgs(currentSession));
        }
    }

    private int currentUnitIndex = Entity.CurrentCourse.SavedUnitIndex;
    public int CurrentUnitIndex
    {
        get { return currentUnitIndex; }
        set
        {
            if (currentUnitIndex == value)
            {
                return;
            }
            currentUnitIndex = value;
            OnUnitChanged(new UnitChangedEventArgs(currentUnitIndex));
        }
    }

    // 실행 상태
    private ExecutionStatus currentExecutionStatus = ExecutionStatus.Stop;
    public ExecutionStatus CurrentExecutionStatus
    {
        get { return currentExecutionStatus; }
        set
        {
            if (currentExecutionStatus == value)
            {
                return;
            }
            currentExecutionStatus = value;
            OnExecutionStatusChanged(
                new ExecutionStatusChangedEventArgs(currentExecutionStatus));
        }
    }
    #endregion

    #region Fields
    public static event EventHandler<UnitChangedEventArgs> UnitChanged;
    public static event EventHandler<SessionChangedEventArgs> SessionChanged;
    public static event EventHandler<ExecutionStatusChangedEventArgs> ExecutionStatusChanged;
    #endregion

    #region Unity Methods
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        Initialize();
    }
    #endregion

    #region Event Triggers
    protected virtual void OnUnitChanged(UnitChangedEventArgs e)
    {
        var handler = UnitChanged;
        if (handler != null)
        {
            handler(this, e);
        }
    }

    protected virtual void OnSessionChanged(SessionChangedEventArgs e)
    {
        var handler = SessionChanged;
        if (handler != null)
        {
            handler(this, e);
        }
    }

    protected virtual void OnExecutionStatusChanged(ExecutionStatusChangedEventArgs e)
    {
        var handler = ExecutionStatusChanged;
        if (handler != null)
        {
            handler(this, e);
        }
    }
    #endregion

    #region Private Methods
    private void Initialize()
    {
        switch (SceneController.Instance.ActiveSceneName)
        {
            case "PaintGame":
                {
                    OnSessionChanged(new SessionChangedEventArgs(Session.BlockCoding));
                    OnUnitChanged(new UnitChangedEventArgs(Entity.CurrentCourse.SavedUnitIndex));
                    break;
                }
            default:
                {
                    OnSessionChanged(new SessionChangedEventArgs(Session.Circuit));
                    break;
                }
        }
    }
    #endregion
}
