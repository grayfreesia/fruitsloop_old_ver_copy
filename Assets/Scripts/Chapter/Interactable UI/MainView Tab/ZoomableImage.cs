﻿using UnityEngine;

namespace Lean.Touch
{
	// This script allows you to scale the current GameObject
	public class ZoomableImage : MonoBehaviour
	{
		public RectTransform Content;

		[Tooltip("Allows you to force rotation with a specific amount of fingers (0 = any)")]
		public int RequiredFingerCount;

		[Tooltip("Should the scale value be clamped?")]
		public bool ScaleClamp;

		[Tooltip("The minimum scale value on all axes")]
		public Vector3 ScaleMin;

		[Tooltip("The maximum scale value on all axes")]
		public Vector3 ScaleMax;

#if UNITY_EDITOR
		protected virtual void Reset()
		{
			Start();
		}
#endif

		protected virtual void Start()
		{
		}

		protected virtual void Update()
		{
			// Get the fingers we want to use
			var fingers = LeanTouch.GetFingers(true, RequiredFingerCount);

			// Calculate the scaling values based on these fingers
			var pinchScale   = LeanGesture.GetPinchScale(fingers);
			var screenCenter = LeanGesture.GetScreenCenter(fingers);

			// Perform the scaling
			Scale(pinchScale, screenCenter);
		}

		private void Scale(float pinchScale, Vector2 screenCenter)
		{
			// Make sure the scale is valid
			if (pinchScale > 0.0f)
			{
				var scale = Content.localScale;

				// Grow the local scale by scale
				scale *= pinchScale;

				if (ScaleClamp == true)
				{
					scale.x = Mathf.Clamp(scale.x, ScaleMin.x, ScaleMax.x);
					scale.y = Mathf.Clamp(scale.y, ScaleMin.y, ScaleMax.y);
					scale.z = Mathf.Clamp(scale.z, ScaleMin.z, ScaleMax.z);
				}

				Content.localScale = scale;
			}
		}
	}
}