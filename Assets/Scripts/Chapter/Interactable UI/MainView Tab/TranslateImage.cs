﻿using UnityEngine;

namespace Lean.Touch
{
    // This script allows you to transform the current GameObject
    public class TranslateImage : MonoBehaviour
    {
        public RectTransform Content;

        [Tooltip("Ignore fingers if the finger count doesn't match? (0 = any)")]
        public int RequiredFingerCount;

        private RectTransform cashedRectTransform;
        public RectTransform CashedRectTransform
        {
            get
            {
                if (cashedRectTransform == null)
                {
                    cashedRectTransform = transform as RectTransform;
                }
                return cashedRectTransform;
            }
        }

#if UNITY_EDITOR
        protected virtual void Reset()
        {
            Start();
        }
#endif

        protected virtual void Start()
        {
        }

        protected virtual void Update()
        {
            // Get the fingers we want to use
            var fingers = LeanTouch.GetFingers(true, RequiredFingerCount);

            // Calculate the screenDelta value based on these fingers
            var screenDelta = LeanGesture.GetScreenDelta(fingers);

            // Perform the translation
            Translate(screenDelta);
        }

        protected virtual void Translate(Vector2 screenDelta)
        {
            float BoundaryHalfWidth = (Content.rect.width * Content.localScale.x - CashedRectTransform.rect.width) * 0.5f;
            float BoundaryHalfHeight = (Content.rect.height * Content.localScale.y - CashedRectTransform.rect.height) * 0.5f;
            float posX = Content.anchoredPosition.x;
            float posY = Content.anchoredPosition.y;

            if (BoundaryHalfWidth < 0 || BoundaryHalfHeight < 0)
            {
                if (BoundaryHalfWidth >= 0)
                {
                    Content.anchoredPosition += new Vector2(screenDelta.x, 0f);
                }

				if (BoundaryHalfHeight >= 0)
				{
					Content.anchoredPosition += new Vector2(0f, screenDelta.y);
				}
            }
			else
			{
				Content.anchoredPosition += screenDelta;
			}

            if (BoundaryHalfWidth >= 0)
            {
                posX = Mathf.Clamp(Content.anchoredPosition.x, -BoundaryHalfWidth, BoundaryHalfWidth);
            }

            if (BoundaryHalfHeight >= 0)
            {
                posY = Mathf.Clamp(Content.anchoredPosition.y, -BoundaryHalfHeight, BoundaryHalfHeight);
            }

            Content.anchoredPosition = new Vector2(posX, posY);
        }
    }
}