﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class IntNumberPad : InputPad {
	[SerializeField] private int maxNumberOfDigits = 2;
	[SerializeField] private int minValue;
	[SerializeField] private int maxValue;
	[SerializeField] private int defaultValue;

	public override void Input(string number) {
		if (inputString.Length < maxNumberOfDigits) {
			inputString += number;
			connectedText.text = inputString;
		}
	}

	public override void Close() {
		// inputString = RemoveStartsWithZero(inputString);

		if (string.IsNullOrEmpty(inputString)) {
			inputString = defaultValue.ToString();
		}

		int intNumber = Int32.Parse(inputString);
		inputString = Mathf.Clamp(intNumber, minValue, maxValue).ToString();

		base.Close();
	}

	public void Backspace() {
		if (inputString.Length > 0) {
			inputString = inputString.Substring (0, inputString.Length - 1);
			connectedText.text = inputString;
		}
	}

	// private string RemoveStartsWithZero(string number) {
	// 	if (number.StartsWith("0")) {
	// 		number = number.Remove(0, 1);
	// 		return RemoveStartsWithZero (number);
	// 	}
	// 	return number;
	// }
}
