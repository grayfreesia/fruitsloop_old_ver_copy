﻿using UnityEngine;
using UnityEngine.UI;

public class InputPad : MonoBehaviour
{
	#region Fields
	public EasyTween easyTween;
	public Color highlightedColor;

	protected Color normalColor;
	protected Text connectedText;
	protected Image connectedImage;
	protected string inputString = string.Empty;
	#endregion

	#region Virtual Methods
	public virtual void Open (Image connectedImage, Text connectedText)
	{
		this.connectedImage = connectedImage;
		this.connectedText = connectedText;

		connectedText.text = string.Empty;
		inputString = string.Empty;
		normalColor = connectedImage.color;
		connectedImage.color = highlightedColor;

		easyTween.OpenCloseObjectAnimation();
	}

	public virtual void Input(string input)
	{
	}

	public virtual void Input(Image image)
	{
	}

	public virtual void Close()
	{
		connectedText.text = inputString;
		connectedImage.color = normalColor;
		easyTween.OpenCloseObjectAnimation();
	}
	#endregion
}
