﻿using UnityEngine;
using UnityEngine.UI;

public class InputPadController : MonoBehaviour
{
	public enum PadType { LoopNumber, Second, Sensor, Byte, Color }
	public InputPad loopNumberPad;
	public InputPad SecondNumberPad;
	public InputPad SensorNumberPad;
	public InputPad ByteNumberPad;
	public InputPad ColorPad;

	private InputPad inputPad;

	public static InputPadController Instance { get; private set; }
	private void Awake() { 
		if (Instance == null) {
            Instance = this;
        }
    }

	public void OpenPad (PadType type, Image connectedImage, Text connectedText)
	{
		switch (type)
		{
			case PadType.LoopNumber:
			{
				inputPad = loopNumberPad;
				break;
			}
			case PadType.Second:
			{
				inputPad = SecondNumberPad;
				break;
			}
			case PadType.Sensor:
			{
				inputPad = SensorNumberPad;
				break;
			}
			case PadType.Byte:
			{
				inputPad = ByteNumberPad;
				break;
			}
			case PadType.Color:
			{
				inputPad = ColorPad;
				break;
			}
			default:
			{
				break;
			}
		}

		inputPad.Open (connectedImage, connectedText);
	}

	public void ClosePad ()
	{
		if (inputPad != null)
		{
			inputPad.Close();
		}
	}
}
