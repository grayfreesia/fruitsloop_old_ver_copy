﻿using UnityEngine;
using UnityEngine.UI;

public class FloatNumberPad : InputPad {
	[SerializeField] private int maxNumberOfDigits = 3;
	[SerializeField] private float minValue = 0.1f;
	[SerializeField] private float maxValue = 99f;
	[SerializeField] private float defaultValue;

	public override void Input(string number) {
		if (inputString.Length < maxNumberOfDigits) {
			inputString += number;
			connectedText.text = inputString;
		}
	}

	public override void Close() {
		float floatNumber = 0f;
		bool canConvert = float.TryParse(inputString, out floatNumber);

		if (canConvert) {
			floatNumber = Mathf.Clamp(floatNumber, minValue, maxValue);
			inputString = floatNumber.ToString();
		} else {
			inputString = defaultValue.ToString();
		}

		base.Close();
	}

	public void Backspace() {
		if (inputString.Length > 0) {
			inputString = inputString.Substring (0, inputString.Length - 1);
			connectedText.text = inputString;
		}
	}
}
