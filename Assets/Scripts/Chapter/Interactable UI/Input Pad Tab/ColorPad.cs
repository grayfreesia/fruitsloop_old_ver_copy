﻿using UnityEngine;
using UnityEngine.UI;

public class ColorPad : InputPad
{
	private Color inputColor;

	private bool isColorInputed = false;

	public override void Input(Image input)
	{
		isColorInputed = true;
		inputColor = input.color;
		Close();
	}

	public override void Close()
	{
		//입력을 안했다면 하얀색을 입력한다
		if (!isColorInputed)
		{
			inputColor = Color.white;
		}
		connectedImage.color = inputColor;
		
		isColorInputed = false;
		easyTween.OpenCloseObjectAnimation();
	}
}
