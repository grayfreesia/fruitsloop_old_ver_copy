﻿using UnityEngine;
using UnityEngine.UI;

public class LoopNumberPad : InputPad
{
    [SerializeField] private int maxNumberOfDigits = 2;
    [SerializeField] private int defaultValue = 1;

    #region Override Methods
    public override void Input(string number) {
        if (number.Equals("infinite")) {
            inputString = "무한";
        }
        else {
            if (inputString.Length < maxNumberOfDigits) {
                inputString += number;
            }
        }

		connectedText.text = inputString;
    }

    public override void Close() {
        // 만약 첫째 자리가 0이면 0을 지운다
        inputString = RemoveStartsWithZero(inputString);

        // 만약 값이 0이거나 입력을 안했다면 1을 입력한다
        if (string.IsNullOrEmpty(inputString))
        {
            inputString = defaultValue.ToString();
        }

        base.Close();
    }
    #endregion

    #region Public Methods
    public void Backspace()
    {
		if (inputString.Equals("무한"))
		{
			inputString = "";
		}
		else
		{
			if (inputString.Length > 0)
			{
				inputString = inputString.Substring(0, inputString.Length - 1);
			}
		}

		connectedText.text = inputString;
    }
    #endregion

    #region Private Methods
    private string RemoveStartsWithZero(string number)
    {
        if (number.StartsWith("0"))
        {
            number = number.Remove(0, 1);
            return RemoveStartsWithZero(number);
        }
        return number;
    }
    #endregion
}
