﻿using UnityEngine;

public class ViewContainer : MonoBehaviour
{
	#region Fields
	public GameObject main;
	public GameObject kit;
	public GameObject blockPalette;
	public GameObject assembly;
	public GameObject assemblyModel;
	// public GameObject optionalController;
	#endregion

	#region Unity Meghods
	private void OnEnable()
	{
		CourseState.SessionChanged += SessionChanged;
	}

	private void OnDisable()
	{
		CourseState.SessionChanged -= SessionChanged;
	}
	#endregion

	#region Event Listeners
	private void SessionChanged (object sender, SessionChangedEventArgs e)
	{
		ChangeView (e.CurrentSession);
	}
	#endregion

	#region Public Methods
	public void ChangeView(CourseState.Session session)
	{
		switch (session)
		{
			case CourseState.Session.Circuit:
			{
				SetActive (main, true);
				SetActive (kit, true);
				SetActive (blockPalette, false);
				SetActive (assembly, false);
				SetActive (assemblyModel, false);
				// SetActive (optionalController, false);
				break;
			}
			case CourseState.Session.BlockCoding:
			{
				SetActive (main, true);
				SetActive (kit, false);
				SetActive (blockPalette, true);
				SetActive (assembly, false);
				SetActive (assemblyModel, false);
				// SetActive (optionalController, true);
				break;
			}
			case CourseState.Session.Assembly:
			{
				SetActive (main, false);
				SetActive (kit, false);
				SetActive (blockPalette, false);
				SetActive (assembly, true);
				SetActive (assemblyModel, true);
				// SetActive (optionalController, false);
				break;
			}
			default: break;
		}
	}
	#endregion

	#region Private Methods
	private void SetActive (GameObject view, bool on)
	{
		if (view != null)
		{
			view.SetActive (on);
		}
	}
	#endregion
}
