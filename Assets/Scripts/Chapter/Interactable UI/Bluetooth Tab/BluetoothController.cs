﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TechTweaking.Bluetooth;
using UnityEngine.UI;

public class BluetoothController : MonoBehaviour
{
    #region Fields
    // 연결 상태 열거형 변수
    public enum State { Disconnected, Connected, Connecting, Error }
    public State state = State.Disconnected;

    float connectionCheckTime;
    bool checkCoroutine;

    private BluetoothDevice device;
    public BluetoothDevice Device
    {
        get { return device; }
    }
    #endregion

    public static BluetoothController Instance { get; private set; }

    #region 블루투스 버튼의 UI를 변경하는 기능 구현
    // 블루투스 버튼 연결 상태 아이콘 이미지
    [SerializeField] private Image connectionStateIcon;
    // 연결된 상태 스프라이트
    [SerializeField] private Sprite connectedSprite;
    // 연결이 끊긴 상태 스프라이트
    [SerializeField] private Sprite disconnectedSprite;
    // 연결된 상태의 색
    [SerializeField] private Color connectedColor;
    // 블루투스 버튼의 이름 텍스트
    [SerializeField] private Text bluetoothButtonLabel;
    // 블루투스로 연결되는 센서 UI 화면
    [SerializeField] private OptionalViewController optionalView;
    // 블루투스 센서와 연결됨 이미지
    // [SerializeField] private GameObject connectedImage;
    // 블루투스 연결 중 메세지 팝업 오프너
    [SerializeField] private PopupOpener popupOpener;
    
    private Popup popup;


    private void SetConnected(bool connected)
    {
        // 아이콘 이미지 변경
        connectionStateIcon.sprite = connected ? connectedSprite : disconnectedSprite;
        // 아이콘 색 변경
        connectionStateIcon.color = connected ? connectedColor : Color.white;
        // 아이콘의 이름 변경
        bluetoothButtonLabel.text = connected ? device.Name : "블루투스";
        // 텍스트의 색 변경
        bluetoothButtonLabel.color = connected ? connectedColor : Color.white;

        // 블루투스 팝업창 기능
        if (connected) {
            // 연결 되었다면 블루투스 연결 팝업창을 닫는다.
            if (popup != null) {
                popup.CloseNotAnim();
                popup = null;
            }
        }

        // 센서와의 연결 기능
        if (optionalView != null) {
            optionalView.SetConnected(connected);
            // connectedImage.SetActive(connected);
        }
        // 연결 상태 변경
        state = connected ? State.Connected : State.Disconnected;
    }

    #endregion

    #region Unity Methods
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        BluetoothAdapter.askEnableBluetooth();//Ask user to enable Bluetooth
        BluetoothAdapter.OnDeviceOFF += HandleOnDeviceOff;
        BluetoothAdapter.OnDevicePicked += HandleOnDevicePicked; //To get what device the user picked out of the devices list
    }

    private void OnDestroy()
    {
        BluetoothAdapter.OnDevicePicked -= HandleOnDevicePicked;
        BluetoothAdapter.OnDeviceOFF -= HandleOnDeviceOff;
    }
    #endregion

    #region Event Handlers
    void HandleOnDeviceOff(BluetoothDevice dev)
    {
        if (!string.IsNullOrEmpty(dev.Name))
        {
            //connectedDeviceNameText.text =
            //    string.Format("{0}에 연결할 수 없습니다. 장치가 꺼져있습니다.", dev.Name);
            AlertController.Show("연결 실패", "블루투스 연결에 실패했습니다.", new AlertOptions {
                okButtonTitle = "확인", okButtonDelegate = () => {
                    if (popup != null) {
                        popup.CloseNotAnim();
                        popup = null;
                    }
                    SetConnected(false);
                }
            });
            state = State.Disconnected;
        }
        else if (!string.IsNullOrEmpty(dev.Name))
        {
            //connectedDeviceNameText.text =
            //    string.Format("{0}에 연결할 수 없습니다. 장치가 꺼져있습니다.", dev.MacAddress);
            AlertController.Show("연결 실패", "블루투스 연결에 실패했습니다.", new AlertOptions {
                okButtonTitle = "확인", okButtonDelegate = () => {
                    if (popup != null) {
                        popup.CloseNotAnim();
                        popup = null;
                    }
                    SetConnected(false);
                }
            });
            state = State.Disconnected;
        }
    }

    // 시스템 옵션에서 유저에 의해 블루투스 기기가 선택되었을 경우 호출
    void HandleOnDevicePicked(BluetoothDevice device)//Called when device is Picked by user
    {
        this.device = device;//save a global reference to the device
                             //this.device.UUID = UUID; //This is only required for Android to Android connection

        /*
		 * 10 equals the char '\n' which is a "new Line" in Ascci representation, 
		 * so the read() method will retun a packet that was ended by the byte 10. simply read() will read lines.
		 */
        device.setEndByte(10);

        //Assign the 'Coroutine' that will handle your reading Functionality, this will improve your code style
        //Other way would be listening to the event Bt.OnReadingStarted, and starting the courotine from there
        device.ReadingCoroutine = ManageConnection;
        //connectedDeviceNameText.text = device.Name;
        bluetoothButtonLabel.text = device.Name;

        Connect();
    }
    #endregion

    #region Public Methods
    public void OnClickBluetooth()
    {
        switch (state)
        {
            case State.Disconnected:
                {
                    ShowDevices();
                    break;
                }
            case State.Connected:
                {
                    Disconnect();
                    break;
                }
            case State.Connecting:
                {
                    Disconnect();
                    break;
                }
            case State.Error:
                {
                    SetConnected(false);
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    // 시스템 옵션에서 블루투스 기기를 선택하도록 하는 메서드
    public void ShowDevices()
    {
        //show a list of all devices//any picked device will be sent to this.HandleOnDevicePicked()
        BluetoothAdapter.showDevices();
    }

    public void Connect()//Connect to the public global variable "device" if it's not null.
    {
        if (device != null)
        {
            // 블루투스 연결 팝업창을 연다.
            popupOpener.OpenPopupNotAnim();
            popup = popupOpener.popup;
            
            device.connect();
            state = State.Connecting;
        }
    }

    public void Disconnect()//Disconnect the public global variable "device" if it's not null.
    {
        if (device != null)
            device.close();
    }

    public void SendCommand(string msg)
    {
        if (device != null)
        {
            device.send(System.Text.Encoding.ASCII.GetBytes(msg + (char)10));
        }
    }
    #endregion

    #region Private Methods
    //############### Reading Data  #####################
    //Please note that you don't have to use Couroutienes, you can just put your code in the Update() method
    IEnumerator ManageConnection(BluetoothDevice device)
    {
        //Manage Reading Coroutine

        // 연결 완료
        SetConnected(true);

        while (device.IsReading)
        {
            if (device.IsDataAvailable)
            {

                //because we called setEndByte(10)..read will always return a packet excluding the last byte 10. 10 equals '\n' so it will return lines. 
                byte[] msg = device.read();

                if (msg != null && msg.Length > 0)
                {
                    string content = System.Text.ASCIIEncoding.ASCII.GetString(msg);

                    string[] array = content.Split('_');
                    if (array[0] == "c")
                    {
                        if (Execution.Instance != null)
                        {
                            Debug.Log("cc1" + array[1]);
                            //Execution.Instance.cValue = int.Parse(array[1]);
                            int value;


                            if (SensorC.Instance != null && int.TryParse(array[1], out value))
                            {
                                SensorC.Instance.SetValue(value);
                                Debug.Log("cc2" + array[1]);
                            }
                        }
                    }
                    else if (array[0] == "u")
                    {

                        if (Execution.Instance != null)
                        {
                            //Debug.Log("uu" + array[1]);
                            //Execution.Instance.uValue = float.Parse(array[1]);
                            int value;
                            if (SensorU.Instance != null && int.TryParse(array[1], out value))
                            {
                                SensorU.Instance.SetValue(value);
                            }
                        }
                    }
                    else if (array[0] == "e")
                    {
                        // 업로드 실패 알림창
                        string uploadFailTitle = "전송 실패";
                        string uploadFailMessage = "전송에 실패했습니다!";
                        if (Upload.Instance.uploadProcess != null)
                        {
                            StopCoroutine(Upload.Instance.uploadProcess);
                        }
                        AlertController.Show(uploadFailTitle, uploadFailMessage, new AlertOptions
                        {
                            cancelButtonTitle = "다시시도",
                            cancelButtonDelegate = () =>
                            {
                                Upload.Instance.StartUploadProcess();
                            },
                            okButtonTitle = "확인",
                            okButtonDelegate = () =>
                            {
                                Upload.Instance.uploadProgressView.SetActive(false);
                            },
                            animate = false,
                        });
                    }
                    else if (array[0] == "o")
                    {
                        Debug.Log("o_");
                        if (!checkCoroutine)
                        {
                            StartCoroutine(CheckConnection());
                        }
                        else
                        {
                            connectionCheckTime = 0f;
                        }
                    }

                }
            }

            yield return null;
        }

        //Switch to Menue View after reading stoped

        // 연결 끊김
        SetConnected(false);
    }

    IEnumerator CheckConnection()
    {
        checkCoroutine = true;
        while (connectionCheckTime < 5f)
        {
            connectionCheckTime += .5f;
            yield return new WaitForSeconds(.5f);
        }
        //SetActive(false);
        //state = State.Disconnected;
        //수정 2018.08.20
        string communicationFailTitle = "연결 실패";
        string communicationFailMessage = @"블루투스 통신 상태가 좋지 않습니다!
        1. RX와 TX핀 연결이 제대로 되었는지 확인해주세요.
        2. 블루투스를 교체해 불량인지 확인해보세요.
        3. 아두이노를 교체해 불량인지 확인해보세요.";
        AlertController.Show(communicationFailTitle, communicationFailMessage, new AlertOptions
        {           
            okButtonTitle = "확인",            
            animate = false,
        });
        checkCoroutine = false;
        connectionCheckTime = 0f;
    }
    #endregion
}
