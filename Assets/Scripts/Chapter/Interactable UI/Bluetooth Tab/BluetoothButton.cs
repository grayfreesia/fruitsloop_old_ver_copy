﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Button))]
public class BluetoothButton : MonoBehaviour
{
	#region Fields
	public EasyTween tabEasyTween;
	public Image foldImage;
	public Sprite folded;
	public Sprite unfolded;

	private bool isFolded = true;
	#endregion

	#region Public Methods
	public void OnPressButton()
	{
		isFolded = !isFolded;
		foldImage.sprite = isFolded ? folded : unfolded;
		tabEasyTween.OpenCloseObjectAnimation();
	}
	#endregion
}
