﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Unit : SimpleButton, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	#region Fields
	public Image dotImage;
	public Text unitLabel;
	public Color selectedColor;
	public Color disabledColor;
	#endregion

	#region Properties
	private Session cashedIncludedSession;
	public Session IncludedSession
	{
		get
		{
			if (cashedIncludedSession == null)
			{
				cashedIncludedSession =
					GetComponentInParent<Session>();
			}
			return cashedIncludedSession;
		}
	}
	#endregion

	#region Public Methods
	public override void SetHighlight(bool isHighlighted)
	{
		if (interactable)
		{
			unitLabel.color = isHighlighted ? selectedColor : Color.black;
			dotImage.color = isHighlighted ? selectedColor : Color.black;
		}
		else
		{
			unitLabel.color = disabledColor;
			dotImage.color = disabledColor;
		}
	}
	#endregion

	#region 아래로 드래그 하면서 버튼이 클릭되지 않도록 하는 기능 구현
	private ScrollRect cachedScrollRect;
    public ScrollRect CachedScrollRect {
        get
        {
            if (cachedScrollRect == null)
            {
                cachedScrollRect = GetComponentInParent<ScrollRect>();
            }
            return cachedScrollRect;
        }
    }

	void IBeginDragHandler.OnBeginDrag(PointerEventData p) {
		if (CachedScrollRect != null) {
			CachedScrollRect.OnBeginDrag(p);
		}
	}

	void IDragHandler.OnDrag(PointerEventData p) {
		if (CachedScrollRect != null) {
			CachedScrollRect.OnDrag(p);

			if (Mathf.Abs(p.delta.y) > 4f) {
				isPointerDown = false;
			}
		}
	}

	void IEndDragHandler.OnEndDrag(PointerEventData p) {
		if (CachedScrollRect != null) {
			CachedScrollRect.OnEndDrag(p);
		}
	}
	#endregion
}
