﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SessionLevelController : MonoBehaviour
{
    [SerializeField] private EasyTween sideTabEasyTween;
    [SerializeField] private Text gameTitleLabel;
	private bool isOpened = false;
    private Coroutine closeCoroutine;


    #region Properties
    private Unit[] cachedUnits;
    public Unit[] CachedUnits
    {
        get
        {
            if (cachedUnits == null)
            {
                cachedUnits =
                    GetComponentsInChildren<Unit>();
            }
            return cachedUnits;
        }
    }

    private Session[] cachedSessions;
    public Session[] CachedSessions
    {
        get
        {
            if (cachedSessions == null)
            {
                cachedSessions =
                    GetComponentsInChildren<Session>();
            }
            return cachedSessions;
        }
    }
    #endregion

	#region Unity Methods
    private void OnEnable()
    {
        CourseState.UnitChanged += UnitChanged;

		// 세션 버튼 이벤트 핸들러 등록
		for (int i = 0; i < CachedUnits.Length; i++)
		{
			var index = i;
			CachedUnits[i].onClicked.AddListener(
				() => OnUnitSelect (index));
		}
    }

    private void OnDisable()
    {
        CourseState.UnitChanged -= UnitChanged;

		// 세션 버튼 이벤트 핸들러 제거
		for (int i = 0; i < CachedUnits.Length; i++)
		{
			CachedUnits[i].onClicked.RemoveAllListeners();
		}
    }
    #endregion

	#region Event Listeners
    private void UnitChanged (object sender, UnitChangedEventArgs e)
    {
        MainBlockZone.Instance.DeleteAllImmediately();
        ChangeSessionUI (e.CurrentUnitIndex);
    }
    #endregion

	#region Event Handlers
    private void OnUnitSelect (int index)
    {
        // 자동으로 세션탭이 닫히는 코르틴을 실행한다.
        // if (closeCoroutine != null)
        // {
        //     StopCoroutine (closeCoroutine);
        // }
        // closeCoroutine = StartCoroutine (Close());

        if (CourseState.Instance.CurrentUnitIndex == index) {
            return;
        }

        CourseState.Instance.CurrentUnitIndex = index;

        // 자동으로 닫힘
        sideTabEasyTween.OpenCloseObjectAnimation();
    }
	#endregion

	#region Public Methods
    public void SetActive (bool on)
    {
        isOpened = on;
    }
    #endregion

    #region Private Methods
    private IEnumerator Close()
    {
        Timer timer = new Timer (1f);
        while (true)
        {
            if (isOpened)
            {
                timer.Update (Time.deltaTime);
                if (timer.IsTimeout)
                {
                    sideTabEasyTween.OpenCloseObjectAnimation();
                    // sideTabButton.easyTween.OpenCloseObjectAnimation();
                    timer.Reset();
                }
            }
            else
            {
                yield break;
            }
            yield return null;
        }
    }

	private void ChangeSessionUI(int currentUnitIndex)
    {
        // Unit 의 UI를 갱신한다.
        for (int i = 0; i < CachedUnits.Length; i++)
        {
            bool interactable = i <= Entity.CurrentCourse.SavedUnitIndex;
            CachedUnits[i].interactable = interactable;
            CachedUnits[i].SetHighlight(i == currentUnitIndex);
        }

		// Session Tab의 UI를 갱신한다.
		for (int i = 0; i < CachedSessions.Length; i++)
		{
			bool active =
				i <= CachedUnits[Entity.CurrentCourse.SavedUnitIndex].IncludedSession.SessionIndex;
			CachedSessions[i].SetActive (active);
		}
		
        gameTitleLabel.text = CachedUnits[currentUnitIndex].unitLabel.text;
		// sideTabButton.sessionText.text = CachedUnits[currentUnitIndex].IncludedSession.sessionLabel.text;
    }
	#endregion
}
