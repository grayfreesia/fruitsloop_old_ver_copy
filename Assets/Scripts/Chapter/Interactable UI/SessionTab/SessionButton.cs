﻿using UnityEngine;
using UnityEngine.UI;

public class SessionButton : SimpleButton
{
	public CourseState.Session session;

	public Image sessionImage;
	public Text sessionLabel;
	public Color highlightedColor;

	public override void SetHighlight(bool isHighlighted) {
		sessionImage.color = isHighlighted ? highlightedColor : Color.white;
		sessionLabel.color = isHighlighted ? highlightedColor : Color.white;
	}
}
