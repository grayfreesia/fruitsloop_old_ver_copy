﻿// using System.Collections;
// using UnityEngine;
// using UnityEngine.UI;

// public class SessionController : MonoBehaviour
// {
// 	#region Fields
// 	public SideTabButton sideTabButton;
//     private bool isOpened = false;
//     private Coroutine closeCoroutine;
// 	#endregion

//     #region Properties
//     private SessionButton[] cashedSessionButtons;
//     public SessionButton[] CashedSessionButtons
//     {
//         get
//         {
//             if (cashedSessionButtons == null)
//             {
//                 cashedSessionButtons =
//                     GetComponentsInChildren<SessionButton>();
//             }
//             return cashedSessionButtons;
//         }
//     }
//     #endregion

//     #region Unity Methods
//     private void OnEnable()
//     {
//         CourseState.SessionChanged += SessionChanged;

// 		// 세션 버튼 이벤트 핸들러 등록
// 		for (int i = 0; i < CashedSessionButtons.Length; i++)
// 		{
// 			var session = CashedSessionButtons[i].session;
// 			CashedSessionButtons[i].GetComponent<Button>().onClick.AddListener(
// 				() => OnSessionSelect (session));
// 		}
//     }

//     private void OnDisable()
//     {
//         CourseState.SessionChanged -= SessionChanged;

// 		// 세션 버튼 이벤트 핸들러 제거
// 		for (int i = 0; i < CashedSessionButtons.Length; i++)
// 		{
// 			CashedSessionButtons[i].GetComponent<Button>().onClick.RemoveAllListeners();
// 		}
//     }
//     #endregion

//     #region Event Listeners
//     private void SessionChanged (object sender, SessionChangedEventArgs e)
//     {
//         ChangeSessionUI (e.CurrentSession);
//     }
//     #endregion

// 	#region Event Handlers
//     private void OnSessionSelect (CourseState.Session session)
//     {
//         // 자동으로 세션탭이 닫히는 코르틴을 실행한다.
//         if (closeCoroutine != null)
//         {
//             StopCoroutine (closeCoroutine);
//         }
//         closeCoroutine = StartCoroutine (Close());

//         if (CourseState.Instance.CurrentSession == session) {
//             // 같은 세션을 눌렀을 경우
//             return;
//         }

//         // 실행중인 블록팔레트가 있으면 실행을 멈춘다.
//         if (CourseState.Instance.CurrentExecutionStatus == CourseState.ExecutionStatus.Play
//             || CourseState.Instance.CurrentExecutionStatus == CourseState.ExecutionStatus.Pause)
//         {
//             // CourseState.Instance.CurrentExecutionStatus = CourseState.ExecutionStatus.Stop;
//             AlertController.Show("후르츠루프", "현재 코딩 블록이 실행 중입니다!");
//             return;
//         }

//         CourseState.Instance.CurrentSession = session;
//     }
// 	#endregion

//     #region Public Methods
//     public void SetActive (bool on)
//     {
//         isOpened = on;
//     }
//     #endregion

//     #region Private Methods
//     private IEnumerator Close()
//     {
//         Timer timer = new Timer (1f);
//         while (true)
//         {
//             if (isOpened)
//             {
//                 timer.Update (Time.deltaTime);
//                 if (timer.IsTimeout)
//                 {
//                     sideTabButton.easyTween.OpenCloseObjectAnimation();
//                     timer.Reset();
//                 }
//             }
//             else
//             {
//                 yield break;
//             }
//             yield return null;
//         }
//     }

//     private void ChangeSessionUI (CourseState.Session session)
//     {
//         for (int i = 0; i < CashedSessionButtons.Length; i++)
//         {
//             CashedSessionButtons[i].SetActive (CashedSessionButtons[i].session == session);

//             if (CashedSessionButtons[i].session == session)
//             {
//                 sideTabButton.sessionText.text = CashedSessionButtons[i].label.text;
//             }
//         }
//     }
//     #endregion
// }
