﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Session : MonoBehaviour
{
	#region Fields
	public Image sessionImage;
	public Text sessionLabel;
	public Color interactiveColor;
	public Color disabledColor;
	#endregion

	#region Properties
	public int SessionIndex
	{
		get { return transform.GetSiblingIndex(); }
	}
	#endregion

	#region Public Methods
	public void SetActive (bool on)
	{
		sessionImage.color = on ? interactiveColor : disabledColor;
		sessionLabel.color = on ? Color.black : disabledColor;
	}
	#endregion
}
