﻿using UnityEngine;
using UnityEngine.UI;

public class ExecuteButton : SimpleButton
{
    #region Fields
    public Image executeImage;
    public Sprite execute;
    public Sprite stop;
    public Text buttonLabel;

    [SerializeField] private Color executedColor;
    #endregion

    #region Consts
    private readonly string executeLabel = "시뮬레이션 실행";
    private readonly string stopLabel = "시뮬레이션 정지";
    #endregion

    #region Unity Methods
    private void OnEnable()
    {
        CourseState.ExecutionStatusChanged += ExecutionStatusChanged;
        onClicked.AddListener(OnExecuteButtonClick);
    }

    private void OnDisable()
    {
        CourseState.ExecutionStatusChanged -= ExecutionStatusChanged;
        onClicked.RemoveAllListeners();
    }
    #endregion

    #region Event Listeners
    private void ExecutionStatusChanged (object sender, ExecutionStatusChangedEventArgs e)
    {
        switch (e.ExecutionStatus)
        {
            case CourseState.ExecutionStatus.Stop:
            {
                SetHighlight(false);
                break;
            }
            case CourseState.ExecutionStatus.Play:
            {
                SetHighlight(true);
                break;
            }
            default:
            {
                SetHighlight(false);
                break;
            }
        }
    }
    #endregion

    public override void SetHighlight(bool isHighlighted) {
            executeImage.sprite = isHighlighted ? stop : execute;
            executeImage.color = isHighlighted ? executedColor : Color.white;
            buttonLabel.text = isHighlighted ? stopLabel : executeLabel;
            buttonLabel.color = isHighlighted ? executedColor : Color.white;
    }

    #region Event Handlers
    public void OnExecuteButtonClick()
    {
        switch (CourseState.Instance.CurrentExecutionStatus)
        {
            case CourseState.ExecutionStatus.Stop:
            {
                CourseState.Instance.CurrentExecutionStatus = CourseState.ExecutionStatus.Play;
                break;
            }
            case CourseState.ExecutionStatus.Play:
            {
                CourseState.Instance.CurrentExecutionStatus = CourseState.ExecutionStatus.Stop;
                break;
            }
            default:
            {
                Debug.Log ("Current execution state is pause.");
                break;
            }
        }
    }
    #endregion
}
