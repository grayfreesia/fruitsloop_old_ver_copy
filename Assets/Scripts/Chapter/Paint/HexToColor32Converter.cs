﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HexToColor32Converter {

	static bool isWrongChar = false;
	public static Color32 SoftR
	{
		get
		{
			return Convert("D50000");
		}
	}
	public static Color32 SoftG
	{
		get
		{
			return Convert("00C853");
		}
	}
	public static Color32 SoftB
	{
		get
		{
			return Convert("2962FF");
		}
	}
	public static Color32 Convert(string hex)
	{
		
		if(hex.Length != 6)
		{
			Debug.LogWarning("Hex input error : Input 6-long string");
			return Color.white;
		}
		byte r = (byte)(HexToDec(hex[0]) * 16 + HexToDec(hex[1]));
		byte g = (byte)(HexToDec(hex[2]) * 16 + HexToDec(hex[3]));
		byte b = (byte)(HexToDec(hex[4]) * 16 + HexToDec(hex[5]));

		if(isWrongChar)
		{
			Debug.LogWarning("Hex input error : Wrong hexcode");
			return Color.white;
		}

		return new Color32(r, g, b, 255);

	}

	public static int HexToDec(char h)
	{
		switch(h)
		{
			case '0':
			{
				return 0;
			}
			case '1':
			{
				return 1;
			}
			case '2':
			{
				return 2;
			}
			case '3':
			{
				return 3;
			}
			case '4':
			{
				return 4;
			}
			case '5':
			{
				return 5;
			}
			case '6':
			{
				return 6;
			}
			case '7':
			{
				return 7;
			}
			case '8':
			{
				return 8;
			}
			case '9':
			{
				return 9;
			}
			case 'A':
			case 'a':
			{
				return 10;
			}
			case 'B':
			case 'b':
			{
				return 11;
			}
			case 'C':
			case 'c':
			{
				return 12;
			}
			case 'D':
			case 'd':
			{
				return 13;
			}
			case 'E':
			case 'e':
			{
				return 14;
			}
			case 'F':
			case 'f':
			{
				return 15;
			}
			default:
			{			
				isWrongChar = true;
				return 16;
			}
		}
	}
	
}
