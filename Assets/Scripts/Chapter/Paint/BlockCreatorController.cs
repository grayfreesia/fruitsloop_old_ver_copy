﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCreatorController : MonoBehaviour
{
	public GameObject loopCreatorBlockContainer;
	public GameObject loopBlock;
	public GameObject conditionCreatorBlockContainer;
	public GameObject conditionBlock;
	public GameObject paintRGBColorCreatorblock;
	public GameObject paintColorCreatorblock;
	public GameObject nextLineCreatorblock;
	public BlockExplainViewController blockExplainView;

	#region Unity Methods
	private void OnEnable()
	{
		CourseState.UnitChanged += UnitChanged;
	}

	private void OnDisable()
	{
		CourseState.UnitChanged -= UnitChanged;
	}
	#endregion

	#region Event Listeners
	private void UnitChanged (object sender, UnitChangedEventArgs e)
    {
		switch (e.CurrentUnitIndex)
		{
			// 순차
			case 0:
			{
				loopCreatorBlockContainer.SetActive (false);
				conditionCreatorBlockContainer.SetActive (false);
				paintRGBColorCreatorblock.SetActive (true);
				paintColorCreatorblock.SetActive (false);
				nextLineCreatorblock.SetActive (false);
				break;
			}
			case 1:
			case 2:
			case 3:
			{
				loopCreatorBlockContainer.SetActive (false);
				conditionCreatorBlockContainer.SetActive (false);
				paintRGBColorCreatorblock.SetActive (true);
				paintColorCreatorblock.SetActive (false);
				nextLineCreatorblock.SetActive (true);
				break;
			}
			// 반복
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			{
				loopCreatorBlockContainer.SetActive (true);
				conditionCreatorBlockContainer.SetActive (false);
				paintRGBColorCreatorblock.SetActive (true);
				paintColorCreatorblock.SetActive (false);
				nextLineCreatorblock.SetActive (true);

				if (e.CurrentUnitIndex == 4) {
					blockExplainView.Show(loopBlock, "반복하기 블록을 이용해 보세요.");
				}
				break;
			}
			// 조건
			case 9:
			case 10:
			case 11:
			{
				loopCreatorBlockContainer.SetActive (true);
				conditionCreatorBlockContainer.SetActive (true);
				paintRGBColorCreatorblock.SetActive (true);
				paintColorCreatorblock.SetActive (false);
				nextLineCreatorblock.SetActive (true);

				if (e.CurrentUnitIndex == 9) {
					blockExplainView.Show(conditionBlock, "조건 블록을 이용해 보세요.");
				}
				break;
			}
			// 자유
			case 12:
			{
				loopCreatorBlockContainer.SetActive (true);
				conditionCreatorBlockContainer.SetActive (true);
				paintRGBColorCreatorblock.SetActive (false);
				paintColorCreatorblock.SetActive (true);
				nextLineCreatorblock.SetActive (true);

				string title = "후르츠루프!";
				string message = "모든 단계를 잘 따라그렸어요! 이제 자유롭게 그리고 싶은 그림을 그려보세요. 처음부터 다시 하고 싶으면 다시하기 버튼을 눌러주세요.";
				AlertController.Show(title, message, new AlertOptions
				{
					cancelButtonTitle = "다시하기",
					cancelButtonDelegate = () =>
					{
						PlayerPrefs.SetInt(Entity.CurrentCourse.CourseName, 0);
						Entity.CurrentCourse.Completed = false;
						Entity.CurrentCourse.SavedUnitIndex = 0;
						CourseState.Instance.CurrentUnitIndex = 0;
					},
					okButtonTitle = "확인",
					animate = false,
					ignoreOpenedAlertView = true,
				});
				break;
			}
		}
    }
	#endregion
}
