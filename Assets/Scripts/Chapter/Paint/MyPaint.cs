﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyPaint : MonoBehaviour
{
    public GameObject penguin;
    public readonly int size = 12;
    private Color canvasColor;

    public int cursor = 0;
    public int Cursor
    {
        get
        {
            return cursor;
        }
        set
        {
            cursor = value;
            PenguinMove();
        }
    }
    public RawImage[] grids;
    public static MyPaint Instance { get; private set; }
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        grids = GetComponentsInChildren<RawImage>();
        // canvasColor = GetComponentInParent<Image>().color; 

    }


    private void OnEnable()
    {
        CourseState.ExecutionStatusChanged += ExecutionStatusChanged;
        CourseState.UnitChanged += UnitChanged;
    }

    private void OnDisable()
    {
        CourseState.ExecutionStatusChanged -= ExecutionStatusChanged;
        CourseState.UnitChanged -= UnitChanged;
    }

    private void ExecutionStatusChanged(object sender, ExecutionStatusChangedEventArgs e)
    {
        switch (e.ExecutionStatus)
        {
            case CourseState.ExecutionStatus.Stop:
                {
                    break;
                }
            case CourseState.ExecutionStatus.Play:
                {
                    SetClear();
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    private void UnitChanged(object sender, UnitChangedEventArgs e)
    {
        SetClear();
    }

    public void SetColor(Color color)
    {
        if (Cursor >= grids.Length)
        {
            Debug.Log("out of length 36");
            CourseState.Instance.CurrentExecutionStatus = CourseState.ExecutionStatus.Stop;
        }
        else
        {
            grids[Cursor++].color = color;
        }
    }

    public void newLine()
    {
        Cursor += size - (Cursor % size);
    }

    public void SetClear()
    {
        for (int i = 0; i < grids.Length; i++)
        {
            // grids[i].color = canvasColor;
            grids[i].color = new Color(0f,0f,0f,0f);
        }
        Cursor = 0;
    }

    private void PenguinMove()
    {
        if (Cursor < grids.Length)
        {
            penguin.transform.position = grids[Cursor].transform.position;
        }
        
    }

    IEnumerator Start()
    {
        yield return null;
        SetClear();
    }

    public void ColHigh()
    {
        StartCoroutine(ColHighlight());
    }
    IEnumerator ColHighlight()
    {
        Color[] c = new Color[6];
        for(int i = 0; i < 6; i++ )
        {
            c[i] = MyPaint.Instance.grids[i * 12 + MyPaint.Instance.Cursor % 12].color;
            MyPaint.Instance.grids[i * 12 + MyPaint.Instance.Cursor % 12].color = new Color32(113, 192, 255, 255);
        }
        yield return new WaitForSeconds(Execution.Instance.intervalTime/10f);
        for(int i = 0; i < 6; i++ )
        {
            MyPaint.Instance.grids[i * 12 + MyPaint.Instance.Cursor % 12].color = c[i];           
        }
        
    }

    public void LineHigh()
    {
        StartCoroutine(LineHighlight());
    }
    IEnumerator LineHighlight()
    {
        Color[] c = new Color[12];
        Debug.Log((MyPaint.Instance.Cursor - MyPaint.Instance.Cursor%12) + ","+ (MyPaint.Instance.Cursor - MyPaint.Instance.Cursor%12 + 12));
        for(int i = MyPaint.Instance.Cursor - MyPaint.Instance.Cursor%12; i < MyPaint.Instance.Cursor - MyPaint.Instance.Cursor%12 + 12; i++ )
        {
            c[i%12] = MyPaint.Instance.grids[i].color;
            MyPaint.Instance.grids[i].color = new Color32(113, 192, 255, 255);
        }
        yield return new WaitForSeconds(Execution.Instance.intervalTime/10f);
        for(int i = MyPaint.Instance.Cursor - MyPaint.Instance.Cursor%12; i < MyPaint.Instance.Cursor - MyPaint.Instance.Cursor%12 + 12; i++ )
        {
            MyPaint.Instance.grids[i].color = c[i%12];           
        }
        
    }
}
