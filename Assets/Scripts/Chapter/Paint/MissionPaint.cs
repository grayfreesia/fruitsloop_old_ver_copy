﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionPaint : MonoBehaviour
{
    int cursor = 0;
    int Cursor 
    {
        get
        {
            return cursor;
        }
        set
        {
            cursor = value;
            PenguinMove();
        }
    }
    readonly int size = 12;

    int[][] missionArray = new int[13][];
    // int level = 0;
    private Color canvasColor;
    IEnumerator paint;
    public RawImage[] grids;

    public GameObject pengcasso;
    Animator anim;
    public static MissionPaint Instance { get; private set; }

    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        grids = GetComponentsInChildren<RawImage>();
        // canvasColor = GetComponentInParent<Image>().color;
        InitMissionArray();   
        if(pengcasso != null)
        {
            anim = pengcasso.GetComponentInChildren<Animator>();
        }    
        Cursor = 0;
    }

    private void OnEnable()
    {
        CourseState.ExecutionStatusChanged += ExecutionStatusChanged;
        CourseState.UnitChanged += UnitChanged;
    }

    private void OnDisable()
    {
        CourseState.ExecutionStatusChanged -= ExecutionStatusChanged;
        CourseState.UnitChanged -= UnitChanged;
    }

    private void ExecutionStatusChanged(object sender, ExecutionStatusChangedEventArgs e)
    {
        switch (e.ExecutionStatus)
        {
            case CourseState.ExecutionStatus.Stop:
                {
                    MissionCheck();
                    break;
                }
            case CourseState.ExecutionStatus.Play:
                {
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    private void UnitChanged(object sender, UnitChangedEventArgs e)
    {
        GetLevelPaint();
    }

    public void GetLevelPaint()
    {
        if(CourseState.Instance.CurrentExecutionStatus == CourseState.ExecutionStatus.Play)
        {
            return;
        }

        if(paint != null)
        {
            StopCoroutine(paint);
        }
        for (int i = 0; i < grids.Length; i++)
        {
            // grids[i].color = canvasColor;
            grids[i].color = new Color(0f,0f,0f,0f);
        }
        Cursor = 0;
        paint = PaintLevel();
        StartCoroutine(paint);
    }

    IEnumerator PaintLevel()
    {
        anim.SetBool("isPainting", true);
        for (int i = 0; i < missionArray[CourseState.Instance.CurrentUnitIndex].Length; i++)
        {
            SetColor(missionArray[CourseState.Instance.CurrentUnitIndex][i]);
            yield return new WaitForSeconds(0.25f);
        }
        anim.SetBool("isPainting", false);
    }

    void InitMissionArray()
    {
        missionArray[0] = new int[] { 1, 1, 1 };
        missionArray[1] = new int[] { 1, 1, 1, 0, 1, 1, 1 };
        missionArray[2] = new int[] { 1, 2, 3 };
        missionArray[3] = new int[] { 1, 2, 3, 0, 1, 2, 3 };     
        missionArray[4] = new int[] { 1, 1, 1, 1, 1, 1 };
        missionArray[5] = new int[] { 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 };
        missionArray[6] = new int[] { 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3 };
        missionArray[7] = new int[] { 3, 3, 3, 3, 3, 0, 3, 3, 3, 3, 3, 0, 3, 3, 3, 3, 3, 0, 3, 3, 3, 3, 3, 0, 3, 3, 3, 3, 3 };
        missionArray[8] = new int[] { 1, 1, 1, 1, 1, 0, 2, 2, 2, 2, 2, 0, 1, 1, 1, 1, 1, 0, 2, 2, 2, 2, 2, 0, 1, 1, 1, 1, 1, 0, 2, 2, 2, 2, 2 };
        missionArray[9] = new int[] { 3, 3, 3, 3, 3, 0, 0, 3, 3, 3, 3, 3, 0, 0, 3, 3, 3, 3, 3, 0 };
        missionArray[10] = new int[] { 1, 1, 1, 1, 1, 0, 3, 3, 3, 3, 3, 0, 1, 1, 1, 1, 1, 0, 3, 3, 3, 3, 3, 0, 1, 1, 1, 1, 1, 0, 3, 3, 3, 3, 3 };
        missionArray[11] = new int[] { 2, 3, 2, 3, 2, 3, 0, 3, 2, 3, 2, 3, 2, 0, 2, 3, 2, 3, 2, 3, 0, 3, 2, 3, 2, 3, 2, 0, 2, 3, 2, 3, 2, 3, 0, 3, 2, 3, 2, 3, 2};
        missionArray[12] = new int[] { 5, 5, 5, 3, 3, 3, 5, 5, 5, 0, 5, 5, 3, 5, 3, 5, 3, 5, 5, 0, 5, 5, 3, 3, 4, 3, 3, 5, 5, 0, 5, 3, 3, 5, 5, 5, 3, 3, 5, 0, 5, 5, 3, 5, 5, 5, 3, 5, 5, 0, 5, 5, 4, 4, 3, 4, 4, 5, 5};
    }

    void SetColor(int color)
    {
        switch (color)
        {
            case 0:
                {
                    newLine();
                    break;
                }
            case 1:
                {
                    grids[Cursor++].color = HexToColor32Converter.SoftR;
                    break;
                }
            case 2:
                {
                    grids[Cursor++].color = HexToColor32Converter.SoftG;
                    break;
                }
            case 3:
                {
                    grids[Cursor++].color = HexToColor32Converter.SoftB;
                    break;
                }
            case 4:
                {
                    grids[Cursor++].color = Color.yellow;
                    break;
                }
            case 5:
                {
                    grids[Cursor++].color = Color.white;
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    public void newLine()
    {
        Cursor += size - (Cursor % size);
    }

    public void MissionCheck()
    {
        for (int i = 0; i < grids.Length; i++)
        {
            if (grids[i].color != MyPaint.Instance.grids[i].color)
            {
                // Debug.Log("fail");
                return;
            }
        }

        // success
        if (CourseState.Instance.CurrentUnitIndex == Entity.CurrentCourse.SavedUnitIndex)
        {
            PlayerPrefs.SetInt(Entity.CurrentCourse.CourseName, Entity.CurrentCourse.SavedUnitIndex + 1);
            Entity.CurrentCourse.SavedUnitIndex = Mathf.Min(
                Entity.CurrentCourse.SavedUnitIndex + 1, Entity.CurrentCourse.UnitCount - 1);
        }

        AlertController.Show ("후르츠 루프!", "잘했어요! 참 쉽죠?", new AlertOptions
        {
            okButtonTitle = "다음 단계로", okButtonDelegate = () =>
            {
                CourseState.Instance.CurrentUnitIndex = Mathf.Min(
                        CourseState.Instance.CurrentUnitIndex + 1, Entity.CurrentCourse.UnitCount - 1);
            },
            viewPosition = new Vector2 (0f, 70f),
            animate = false,
            overLayActive = true,
        });
    }


    private void PenguinMove()
    {
        if (Cursor < grids.Length)
        {
            pengcasso.transform.position = grids[Cursor].transform.position;
        }
        
    }

}
