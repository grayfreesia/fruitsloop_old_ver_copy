﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;

public class SaveLoad : MonoBehaviour
{
    BlockSerializer serializer;
    BlockDeserializer deserializer;
    public static List<SerializedBlock> blockList = new List<SerializedBlock>();

    public static SaveLoad Instance { get; private set; }
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        serializer = GetComponentInChildren<BlockSerializer>();
        deserializer = GetComponentInChildren<BlockDeserializer>();

    }

    public void Save()
    {
        if (MainBlockZone.Instance != null)
        {
            Block[] blocks = MainBlockZone.Instance.Blocks;
            blockList.Clear();
            for (int i = 0; i < blocks.Length; i++)
            {
                blockList.Add(serializer.GetSerializedData(blocks[i]));
            }
            BinaryFormatter bf = new BinaryFormatter();
            //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
            FileStream file = File.Create(Application.persistentDataPath + "/" + SceneManager.GetActiveScene().name + ".gd"); //you can call it anything you want
            bf.Serialize(file, blockList);
            file.Close();
        }
    }

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/" + SceneManager.GetActiveScene().name + ".gd"))
        {
            deserializer.stack.Clear();
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/" + SceneManager.GetActiveScene().name + ".gd", FileMode.Open);
            blockList = (List<SerializedBlock>)bf.Deserialize(file);
            file.Close();
            for (int i = 0; i < blockList.Count; i++)
            {
                deserializer.Deserialize(blockList[i]);
            }
        }
    }
}