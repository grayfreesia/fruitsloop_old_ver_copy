﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Card : SimpleButton, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	#region Public Variables
	public EasyTween easyTween;
	public List<GameObject> Stars = new List<GameObject>();

	[Header ("Card Information")]
	public CardInfo Info;
	#endregion

	#region Nested Types
	[System.Serializable]
	public struct CardInfo
	{
		[Tooltip("This is scnene trigger name. it must be used english and it have to same with scene name.")]
		public string courseName;
		public HomeState.Category category;
	}
	#endregion

	private ScrollRect cachedScrollRect;
    public ScrollRect CachedScrollRect
    {
        get
        {
            if (cachedScrollRect == null)
            {
                cachedScrollRect = GetComponentInParent<ScrollRect>();
            }
            return cachedScrollRect;
        }
    }

	protected override void Awake() {
		base.Awake();
	}

	#region Event Handlers
	public override void OnPointerUp(PointerEventData eventData) {
		if (isPointerDown) {
			onClicked.Invoke();
		}
		canvasGroup.alpha = 1f;
	}

	void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) {
		if (CachedScrollRect != null) {
			CachedScrollRect.OnBeginDrag(eventData);
		}
	}

	void IDragHandler.OnDrag(PointerEventData eventData) {
		if (CachedScrollRect != null) {
			CachedScrollRect.OnDrag(eventData);

			if (Mathf.Abs(eventData.delta.x) > 4f) {
				isPointerDown = false;
			}
		}
	}

	void IEndDragHandler.OnEndDrag(PointerEventData eventData) {
		if (CachedScrollRect != null) {
			CachedScrollRect.OnEndDrag(eventData);
		}
	}
	#endregion
}