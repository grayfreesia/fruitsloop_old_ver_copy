﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CardContainer : MonoBehaviour
{
    #region Fields
    public List<Card> cards = new List<Card>();
    private List<Card> cachedCards = new List<Card>();
    #endregion

    #region Event Listeners
    // 현재 카테고리 변경시 호출된다.
    private void CurrentCategoryChanged(object sender, CurrentCategoryChangedEventArgs e)
    {
        // 카드를 현재 카테고리로 정렬한다.
        SortCardsByCategory(e.CurrentCategory);
    }
    #endregion

    #region Event Handlers
    // 카드 선택시 호출된다.
    private void OnCardSelect(Card selectedCard)
    {
        // 누른 카드의 course 이름에 따라 현재의 course를 임시 저장한다.
        for (int i = 0; i < Entity.Courses.Length; i++)
        {
            if (Entity.Courses[i].CourseName == selectedCard.Info.courseName)
            {
                Entity.CurrentCourse = Entity.Courses[i];
                break;
            }
        }
        
        SceneController.Instance.LoadSceneByCourseName (selectedCard.Info.courseName);
    }
    #endregion

    #region Unity Methods
    private void Awake()
    {
        CreateCards();
    }

    private void OnEnable()
    {
        HomeState.CurrentCategoryChanged += CurrentCategoryChanged;

        // 카드 버튼 이벤트 및 카드의 Play 버튼 이벤트 등록
        for (int i = 0; i < cachedCards.Count; i++)
        {
            // 카드 클릭 이벤트 리스너 등록
            var card = cachedCards[i];
            cachedCards[i].onClicked.AddListener(
                () => OnCardSelect(card));
        }
    }

    private void OnDisable()
    {
        HomeState.CurrentCategoryChanged -= CurrentCategoryChanged;

        for (int i = 0; i < cachedCards.Count; i++)
        {
            // 카드 버튼 이벤트 리스너 해제
            cachedCards[i].onClicked.RemoveAllListeners();
        }
    }
    #endregion

    #region Private Methods
    public void CreateCards()
    {
        cachedCards.Clear();
        for (int i = 0; i < cards.Count; i++) {
                var card = Instantiate(cards[i].gameObject, transform).GetComponent<Card>();
                cachedCards.Add(card);
        }

        // bool tutorialCompleted = PlayerPrefs.GetInt("Tutorial", 0) == 0 ? false : true;

        // if (!tutorialCompleted) {
        //     for (int i = 0; i < cards.Count; i++) {
        //         if (cards[i].Info.courseName.Equals("Tutorial")) {
        //             var card = Instantiate(cards[i].gameObject, transform).GetComponent<Card>();
        //             cachedCards.Add(card);
        //         }
        //     }
        // } else {
        //     // Cards 생성한다.
        //     // 생성한 카드는 같은 크기의 List에 같은 순서로 임시 저장한다.
        //     for (int i = 0; i < cards.Count; i++)
        //     {
        //         var card = Instantiate(cards[i].gameObject, transform).GetComponent<Card>();
        //         cachedCards.Add(card);

        //         // if (cachedCards[i].Stars[0].transform.parent.gameObject.activeSelf)
        //         // {
        //         //     // 카드(코스)의 완료정도에 따라 별을 표시한다.
        //         //     var activeStartCount = ActiveStarsByPercent(Entity.Courses[i].CompletedPercent);
        //         //     for (int j = 0; j < activeStartCount; j++)
        //         //     {
        //         //         cachedCards[i].Stars[j].SetActive(true);
        //         //     }
        //         // }
        //     }
        // }
    }

    private int ActiveStarsByPercent(int percent)
    {
        var activeStartCount = 0;
        if (percent <= 33)
        {
            // 별 0개
            activeStartCount = 0;
        }
        else if (percent > 33 && percent <= 66)
        {
            // 별 1개
            activeStartCount = 1;
        }
        else if (percent > 66 && percent <= 99)
        {
            // 별 2개
            activeStartCount = 2;
        }
        else if (percent == 100)
        {
            // 별 3개
            activeStartCount = 3;
        }
        return activeStartCount;
    }

    // 카드를 카테고리에 따라 정렬한다.
    private void SortCardsByCategory(HomeState.Category category)
    {
        // category 가 ALL 이면 카드를 모두 on한다.
        // 다른 경우에는 카테고리에 해당하는 카드만 모두 on한다.
        if (category == HomeState.Category.All)
        {
            for (int i = 0; i < cachedCards.Count; i++)
            {
                SetActiveAnimation(cachedCards[i]);
            }
        }
        else
        {
            for (int i = 0; i < cachedCards.Count; i++)
            {
                if (cachedCards[i].Info.category == category)
                {
                    SetActiveAnimation(cachedCards[i]);
                }
                else
                {
                    cachedCards[i].transform.localScale = Vector3.zero;
                    cachedCards[i].gameObject.SetActive (false);
                }
            }
        }

        // 카드 내용물을 카테고리 카드수에 따라
        // 카드수가 한 화면 최대 카드 개수 이상이면 왼쪽정렬
        // 그 미만이면 가운데 정렬 한다.
        // AlignByChildCount(ActiveCardCount);
    }

    // private void AlignByChildCount(int childCount)
    // {
    //     float pivotX = childCount > MaxCardCountOnScreen ? 0f : 0.5f;
    //     RectTransform rect = transform as RectTransform;
    //     rect.pivot = new Vector2(pivotX, 0f);
    //     rect.anchoredPosition = new Vector2(0f, rect.anchoredPosition.y);
    // }

    // 카드 활성화 애니메이션
    private void SetActiveAnimation(Card card)
    {
        card.easyTween.OpenCloseObjectAnimation();
    }
    #endregion
}
