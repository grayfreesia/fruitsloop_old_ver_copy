﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Button))]
public class CategoryButton : MonoBehaviour
{
	#region Public variable
	// 버튼 카테고리.
	public HomeState.Category Category;

	// 버튼 텍스트
	public Text CategoryLabel;
	
	// 활성화 상태 애니메이션 스크립트.
	public EasyTween ImageSelected;
	#endregion
}