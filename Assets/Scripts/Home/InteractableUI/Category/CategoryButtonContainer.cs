﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CategoryButtonContainer : MonoBehaviour
{
	#region Public Variables
	public Color normalColor;
	public Color selectedColor;
	#endregion

	#region Fields
	private CategoryButton[] categoryButtons;
	#endregion

	#region Unity Methods
	private void OnEnable()
	{
		HomeState.CurrentCategoryChanged += CurrentCategoryChanged;

		// 정렬 버튼 클릭 이벤트를 등록한다.
		categoryButtons = GetComponentsInChildren<CategoryButton>();
		for (int i = 0; i < categoryButtons.Length; i++)
		{
			var category = categoryButtons[i].Category;
			categoryButtons[i].GetComponent<Button>().onClick.AddListener (
				() => OnCategoryButtonClick (category));
		}
	}

	private void OnDisable()
	{
		HomeState.CurrentCategoryChanged -= CurrentCategoryChanged;

		// 정렬 버튼 클릭 이벤트를 삭제한다.
		for (int i = 0; i < categoryButtons.Length; i++)
		{
			categoryButtons[i].GetComponent<Button>().onClick.RemoveAllListeners();
		}
	}
	#endregion

	#region Event Listeners
	// 정렬 버튼 클릭시 호출된다.
	// 현재 카테고리 변경시 호출된다.
	private void CurrentCategoryChanged (object sender, CurrentCategoryChangedEventArgs e)
	{
		ChangeCurrentCategoryUI (e.LastCategory, e.CurrentCategory);
	}
	#endregion

	#region Event Handlers
	private void OnCategoryButtonClick (HomeState.Category category)
	{
		// State 변경한다.
		HomeState.Instance.CurrentCategory = category;
	}
	#endregion

	#region Private Methods
	// 현재 카테고리 UI를 바꾼다.
	private void ChangeCurrentCategoryUI (HomeState.Category lastCategory, HomeState.Category currentCategory)
	{
		for (int i = 0; i < categoryButtons.Length; i++)
		{
			// 이전에 활성화 된 버튼은 비활성화 애니매이션 실행.
			// 현재 활성화 된 버튼은 활성화 애니메이션 실행.
			var categoryButton = categoryButtons[i];
			if (categoryButton.Category == currentCategory || categoryButton.Category == lastCategory)
			{
				if (categoryButton.Category == currentCategory)
				{
					categoryButton.CategoryLabel.color = selectedColor;
				}
				else
				{
					categoryButton.CategoryLabel.color = normalColor;
				}
				categoryButton.ImageSelected.OpenCloseObjectAnimation();
			}
		}
	}
	#endregion
}
