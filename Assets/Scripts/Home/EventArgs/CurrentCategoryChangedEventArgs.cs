﻿using System;

public class CurrentCategoryChangedEventArgs : EventArgs
{
	#region Public Variables
	public readonly HomeState.Category LastCategory;
	public readonly HomeState.Category CurrentCategory;
	#endregion

	#region Constructors
	public CurrentCategoryChangedEventArgs (HomeState.Category lastCategory, HomeState.Category currentCategory)
	{
		LastCategory = lastCategory;
		CurrentCategory = currentCategory;
	}
	#endregion
}
