﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HomeState : MonoBehaviour
{
	#region Public Variables
	public enum Category { All, Basic, InputDevice, OutputDevice, ControlDevice, AcrylicLamp }
	#endregion

	#region Properties
	public static HomeState Instance { get; private set; }

	// Home 씬에서 관리하는 State
	// 현재 카테고리
	private Category currentCategory = Category.All;
	public Category CurrentCategory
	{
		get { return currentCategory; }
		set
		{
			if (currentCategory == value)
			{
				return;
			}
			var lastCategory = currentCategory;
			currentCategory = value;
			OnCurrentCategoryChanged (new CurrentCategoryChangedEventArgs (lastCategory, currentCategory));
		}
	}
	#endregion
	
	#region Fields
	public static event EventHandler<CurrentCategoryChangedEventArgs> CurrentCategoryChanged;
	#endregion


	#region Event Triggers
	protected virtual void OnCurrentCategoryChanged (CurrentCategoryChangedEventArgs e)
	{
		var handler = CurrentCategoryChanged;
		if (handler != null)
		{
			handler (this, e);
		}
	}
	#endregion

	#region Unity Methods
	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			Application.targetFrameRate = 60;
		}
	}

	private void Start()
	{
		// 홈화면 선택한 카테고리를 All로 초기화한다.
		OnCurrentCategoryChanged (new CurrentCategoryChangedEventArgs (Category.All, Category.All));
	}
	#endregion
}
