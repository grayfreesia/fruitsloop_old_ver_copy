﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
	public static SceneController Instance { get; private set; }


	public string ActiveSceneName { get { return SceneManager.GetActiveScene().name; } }

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	public void LoadSceneByCourseName (string sceneName)
	{
		StartCoroutine(LoadSceneAsyncByName(sceneName));
	}

	public void LoadLobbyScene()
	{
		SceneManager.LoadScene (0);
	}

	#region 로딩창 기능 구현
	[SerializeField] private GameObject loadingScreen;
	[SerializeField] private Text hintMessageLabel;

	private readonly string[] tipMessagePool = {
		"센서값은 무한반복 블록을 이용해서 계속 관찰하세요.",
		"아두이노에 소스코드가 업로드 되어있어야 코딩블록을 전송할수 있어요.",
		"기다리기 블록은 일정시간 실행을 일시정지해요.",
		"블루투스의 초기 비밀번호는 '1234'입니다.",
	};

	public IEnumerator LoadSceneAsyncByName(string sceneName)
	{
		AsyncOperation operation = SceneManager.LoadSceneAsync (sceneName);
		operation.allowSceneActivation = false;

		loadingScreen.SetActive (true);
		hintMessageLabel.text = tipMessagePool[Random.Range(0, tipMessagePool.Length)];

		while (!operation.isDone)
		{
			if (operation.progress >= 0.9f) {
				yield return new WaitForSeconds(3f);
				operation.allowSceneActivation = true;
			}
			yield return null;
		}
	}
	#endregion
}
