﻿using UnityEngine;

public struct Timer
{
	#region Fields
	private float elapsedTime;
	private float limitTime;
	#endregion

	#region Properties
	public bool IsTimeout
	{
		get { return elapsedTime >= limitTime; }
	}
	#endregion
	
	#region Public Methods
	public Timer (float limitTime)
	{
		elapsedTime = 0f;
		this.limitTime = limitTime;
	}

	public void Update(float deltaTime)
	{
		elapsedTime = Mathf.Min (elapsedTime + deltaTime, limitTime);
	}

	public void Reset()
	{
		elapsedTime = 0f;
	}
	#endregion
}
