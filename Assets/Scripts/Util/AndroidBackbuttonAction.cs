﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidBackbuttonAction : MonoBehaviour {
	private bool isOpened = false;

	private void OnDestroy() {
		isOpened = false;
	}

	private void Update () {
#if UNITY_ANDROID
		if (Input.GetKey(KeyCode.Escape) && !isOpened) {
			string title;
			string message;
			
			switch (SceneController.Instance.ActiveSceneName) {
				case "Lobby": {
					isOpened = true;
					title = "후르츠루프";
					message = "정말로 앱을 종료하시겠어요?";

					AlertController.Show(title, message, new AlertOptions {
						cancelButtonTitle = "아니요", cancelButtonDelegate = () => {
							isOpened = false;
						},
						okButtonTitle = "네", okButtonDelegate = () => {
							isOpened = false;
							Application.Quit();
						},
						animate = false,
						ignoreOpenedAlertView = true,
					});
					break;
				}
				case "Tutorial": {
					isOpened = true;
					title = "후르츠루프";
					message = "정말로 튜토리얼을 종료하고 홈으로 가시겠어요?";
					AlertController.Show(title, message, new AlertOptions {
						cancelButtonTitle = "아니요", cancelButtonDelegate = () => {
							isOpened = false;
						},
						okButtonTitle = "네", okButtonDelegate = () => {
							//정보 저장
							PlayerPrefs.SetInt("Tutorial", 1);
							isOpened = false;
							SceneController.Instance.LoadLobbyScene();
						},
						animate = false,
						ignoreOpenedAlertView = true,
					});
					break;
				}
				default: {
					isOpened = true;
					title = "후르츠루프";
					message = "진행하고 있는 코스를 종료하고 홈으로 돌아가시겠어요? 현재까지의 진행상황은 저장됩니다.";
					AlertController.Show (title, message, new AlertOptions
					{
						cancelButtonTitle = "아니요", cancelButtonDelegate = () => {
							isOpened = false;
						},
						okButtonTitle = "네", okButtonDelegate = () =>
						{
							isOpened = false;
							SaveLoad.Instance.Save();
							SceneController.Instance.LoadLobbyScene();
						},
						animate = false,
						ignoreOpenedAlertView = true,
					});
					break;
				}
			}
		}
#endif
	}
}
