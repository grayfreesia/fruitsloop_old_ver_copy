﻿using UnityEngine;

public class OpenURL : MonoBehaviour {
	public void OpenAfterQuestion(string url) {
		string title = "후르츠루프";
		string message = "홈페이지 링크를 브라우저로 열까요?";
		AlertController.Show(title, message, new AlertOptions {
			cancelButtonTitle = "아니요",
			okButtonTitle = "열기", okButtonDelegate = () => {
				Application.OpenURL(url);
			},
		});
	}

	public void Open(string url) {
		Application.OpenURL(url);
	}
}