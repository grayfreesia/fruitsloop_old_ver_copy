﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralShare : MonoBehaviour {
	private const string subject = "즐거운 피지컬 코딩과 메이커 교육앱 후르츠 루프! 지금 시작해 보세요";
	private const string body = "https://play.google.com/store/apps/details?id=com.CEREALLAB.FruitsLoop&showAllReviews=true";

	public void Share() {
		StartCoroutine(ShareAndroidText());
	}

	private IEnumerator ShareAndroidText() {
		yield return new WaitForEndOfFrame();
#if UNITY_ANDROID && !UNITY_EDITOR
		using (AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent"))
		using (AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent")) {
			intentObject.Call<AndroidJavaObject>("setAction", intentObject.GetStatic<string>("ACTION_SEND"));
			intentObject.Call<AndroidJavaObject>("setType", "text/plain");
			intentObject.Call<AndroidJavaObject>("putExtra", intentObject.GetStatic<string>("EXTRA_SUBJECT"), subject);
			intentObject.Call<AndroidJavaObject>("putExtra", intentObject.GetStatic<string>("EXTRA_TEXT"), body);
			using (AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
			using (AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity"))
			using (AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Share Via"))
				currentActivity.Call("startActivity", jChooser);
		}
		// AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		// AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
		// intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		// intentObject.Call<AndroidJavaObject>("setType", "text/plain");
		// intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
		// intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), body);
		// AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		// AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
		// AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Share Via");
		// currentActivity.Call("startActivity", jChooser);
#endif
	}
}
