﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalAssemblyPart : MonoBehaviour
{
	private DroppableAssemblyPart[] cashedDroppableAssemblyParts;
	public DroppableAssemblyPart[] CashedDroppableAssemblyParts
	{
		get
		{
			if (cashedDroppableAssemblyParts == null)
			{
				cashedDroppableAssemblyParts = GetComponentsInChildren<DroppableAssemblyPart>();
			}
			return cashedDroppableAssemblyParts;
		}
	}

	public bool IsRemainAllIntermediateAssemblyPart
	{
		get
		{
			for (int i = 0; i < CashedDroppableAssemblyParts.Length; i++)
			{
				if (!CashedDroppableAssemblyParts[i].body.activeSelf)
				{
					return true;
				}
			}
			return false;
		}
	}
}
