﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class DraggableAssemblyPart : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    #region Fields
    public GameObject prefabToDrag;
    public DroppableAssemblyPart[] targets;
    public int assemblyIndex;
    public Image image;

    private GameObject draggingObject;
    private Timer timer = new Timer(0.1f);
    private bool isPointerDown = false;
    private PointerEventData cashedPointerEventData;
    private bool isObjectCreated = false;

    private readonly Color InteractableColor = Color.white;
    private readonly Color UnInteractableColor = new Color(1f, 1f, 1f, 0.3f);
    #endregion

    #region Properties
    private bool interactable = false;
    public bool Interactable
    {
        get { return interactable; }
        set { interactable = value; }
    }

    private ScrollRect cachedScrollRect;
    public ScrollRect CachedScrollRect
    {
        get
        {
            if (cachedScrollRect == null)
            {
                cachedScrollRect = GetComponentInParent<ScrollRect>();
            }
            return cachedScrollRect;
        }
    }

    private Camera cachedCamera;
    public Camera CachedCamera
    {
        get
        {
            if (cachedCamera == null)
            {
                cachedCamera = Camera.main;
            }
            return cachedCamera;
        }
    }

    private Image cachedRaycastImage;
    public Image CachedRaycastImage {
        get {
            if (cachedRaycastImage == null) {
                cachedRaycastImage = GetComponentInParent<RotatableObject>().GetComponent<Image>();
            }
            return cachedRaycastImage;
        }
    }

    public bool IsRemainAssemblyPart
    {
        get
        {
            for (int i = 0; i < targets.Length; i++)
            {
                if (!targets[i].body.activeSelf)
                {
                    return true;
                }
            }
            return false;
        }
    }

    public int RemainAssemblyPartCount
    {
        get
        {
            int count = 0;
            for (int i = 0; i < targets.Length; i++)
            {
                if (!targets[i].body.activeSelf)
                {
                    count++;
                }
            }
            return count;
        }
    }
    #endregion

    #region Unity Methods
    private void Update()
    {
        if (isPointerDown && Interactable)
        {
            // 블록 생성 버튼 다운시 타이머를 잰다.
            timer.Update(Time.deltaTime);
            if (timer.IsTimeout)
            {
                // 지정된 타이머 시간이 지나면
                // 블록 생성
                CreateDraggingObject(cashedPointerEventData);
                isObjectCreated = true;
                CachedRaycastImage.raycastTarget = false;

                // 타이머 초기화
                isPointerDown = false;
                timer.Reset();

                // 진동
                Vibration.Vibrate(40);
            }
        }
    }
    #endregion

    #region Public Methods
    public void SetInteractable(bool on)
    {
        Interactable = on;
        UpdateUI(false);
    }
    #endregion

    #region Private Methods
    private void CreateDraggingObject(PointerEventData pointerEventData)
    {
        if (draggingObject != null)
        {
            Destroy(draggingObject);
        }

        draggingObject = Instantiate(prefabToDrag);
        draggingObject.transform.rotation = targets[0].body.transform.rotation;
        draggingObject.transform.localScale = targets[0].body.transform.lossyScale;
        SetActiveTargetBoundary(true);
        UpdateUI (true);
        UpdateDraggingObjectPos(pointerEventData);
    }

    private void SetActiveTargetBoundary(bool on)
    {
        for (int i = 0; i < targets.Length; i++)
        {
            targets[i].Boundary.enabled = on ? !targets[i].body.activeSelf : false;
        }
    }

    private void UpdateDraggingObjectPos(PointerEventData pointerEventData)
    {
        Vector2 screenPos = pointerEventData.position;
        Vector3 newPos = CachedCamera.ScreenToWorldPoint(new Vector3(screenPos.x, screenPos.y, 10f));
        draggingObject.transform.position = newPos;
    }

    private void UpdateUI(bool begin)
    {
        if (begin)
        {
            if (targets.Length > 1)
            {
                GetComponentInChildren<Text>().text = (RemainAssemblyPartCount - 1).ToString();
                if (RemainAssemblyPartCount - 1 < 1)
                {
                    image.color = UnInteractableColor;
                }
            }
            else
            {
                image.color = UnInteractableColor;
            }
        }
        else
        {
            if (targets.Length > 1)
            {
                GetComponentInChildren<Text>().text = RemainAssemblyPartCount.ToString();
            }
            image.color = IsRemainAssemblyPart && Interactable ? InteractableColor : UnInteractableColor;
        }
    }
    #endregion

    #region Event Handlers
    public void OnPointerDown(PointerEventData pointerEventData)
    {
        isPointerDown = true;
        cashedPointerEventData = pointerEventData;
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        isPointerDown = false;
        timer.Reset();
        if (draggingObject != null)
        {
            Destroy(draggingObject);
            isObjectCreated = false;
            CachedRaycastImage.raycastTarget = true;
            SetActiveTargetBoundary(false);

            // UI Update
            UpdateUI (false);
        }
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        isPointerDown = false;
        timer.Reset();
    }

    public void OnBeginDrag(PointerEventData pointerEventData)
    {
        if (!isObjectCreated)
        {
            CachedScrollRect.OnBeginDrag(pointerEventData);
        }
    }

    public void OnDrag(PointerEventData pointerEventData)
    {
        if (isObjectCreated)
        {
            UpdateDraggingObjectPos(pointerEventData);
        }
        else
        {
            CachedScrollRect.OnDrag(pointerEventData);
            if (Mathf.Abs(pointerEventData.delta.y) > 4f)
            {
                isPointerDown = false;
                timer.Reset();
            }
        }
    }

    public void OnEndDrag(PointerEventData pointerEventData)
    {
        CachedScrollRect.OnEndDrag(pointerEventData);
    }
    #endregion
}
