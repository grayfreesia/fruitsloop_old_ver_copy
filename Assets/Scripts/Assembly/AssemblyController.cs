﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AssemblyController : MonoBehaviour
{
	#region Fields
	public IntermediateAssemblyPart intermediateAssemblyPart;
	public FinalAssemblyPart finalAssemblyPart;
	public static AssemblyController Instance { get; private set; }

	public GameObject[] intermediateAssemblyParts;
	#endregion

	#region Properties
	private int currentAssemblyIndex = 1;
	public int CurrentAssemblyIndex
	{
		get { return currentAssemblyIndex; }
		set { currentAssemblyIndex = value; }
	}

	private DraggableAssemblyPart[] cashedDraggableAssemblyParts;
	public DraggableAssemblyPart[] CashedDraggableAssemblyParts
	{
		get
		{
			if (cashedDraggableAssemblyParts == null)
			{
				cashedDraggableAssemblyParts = GetComponentsInChildren<DraggableAssemblyPart>(true);
			}
			return cashedDraggableAssemblyParts;
		}
	}

	public DraggableAssemblyPart[] ActiveDraggableAssemblyParts
	{
		get
		{
			return Array.FindAll (CashedDraggableAssemblyParts, part => part.assemblyIndex == CurrentAssemblyIndex);
		}
	}
	#endregion

	#region Unity Methods
	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	private void Start()
	{
		UpdateInteractableParts();
	}
	#endregion

	#region Public Methods
	public void UpdateInteractableParts()
	{
		for (int i = 0; i < CashedDraggableAssemblyParts.Length; i++)
		{
			CashedDraggableAssemblyParts[i].SetInteractable (CashedDraggableAssemblyParts[i].assemblyIndex == CurrentAssemblyIndex);
		}

		// 중간 부품 모두 조립했을 경우
		if (intermediateAssemblyPart != null) {
			if (!intermediateAssemblyPart.IsRemainAllIntermediateAssemblyPart && intermediateAssemblyPart.gameObject.activeSelf)
			{
				intermediateAssemblyPart.CompleteAnimate();
				for (int i = 0; i < intermediateAssemblyParts.Length; i++) {
					intermediateAssemblyParts[i].SetActive(false);
				}
			}
		}

		// 최종 부품 모두 조립했을 경우
		if (!finalAssemblyPart.IsRemainAllIntermediateAssemblyPart && finalAssemblyPart.gameObject.activeSelf)
		{
		}
	}
	#endregion
}
