﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DroppableAssemblyPart : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDropHandler
{
	#region Fields
	public GameObject body;
	public GameObject guide;
	#endregion
	
	#region Properties
	private Collider boundary;
	public Collider Boundary
	{
		get
		{
			if (boundary == null)
			{
				boundary = GetComponent<Collider>();
			}
			return boundary;
		}
	}
	#endregion

	#region Unity Methods
	private void Awake()
	{
		// body.SetActive (false);
		// guide.SetActive (false);
		Boundary.enabled = false;
	}
	#endregion

	#region Event Handlers
	public void OnPointerEnter (PointerEventData pointerEventData)
	{
		if (pointerEventData.dragging)
		{
			guide.SetActive (true);
		}
	}

	public void OnPointerExit (PointerEventData pointerEventData)
	{
		if (pointerEventData.dragging)
		{
			guide.SetActive (false);
		}
	}

	public void OnDrop (PointerEventData pointerEventData)
	{
		guide.SetActive (false);
		body.SetActive (true);

		DraggableAssemblyPart draggable = pointerEventData.pointerDrag.GetComponent<DraggableAssemblyPart>();
		if (!draggable.IsRemainAssemblyPart)
		{
			draggable.SetInteractable (false);
		}
		
		// 모든 부품 조립시
		if (!IsRemainAllAssemblyPart())
		{
			AssemblyController.Instance.CurrentAssemblyIndex++;
			AssemblyController.Instance.UpdateInteractableParts();
		}
	}
	#endregion

	#region Private Methods
	private bool IsRemainAllAssemblyPart()
	{
		for (int i = 0; i < AssemblyController.Instance.ActiveDraggableAssemblyParts.Length; i++)
		{
			if (AssemblyController.Instance.ActiveDraggableAssemblyParts[i].IsRemainAssemblyPart)
			{
				return true;
			}
		}
		return false;
	}
	#endregion
}
