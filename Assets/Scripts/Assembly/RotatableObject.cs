﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using UnityEngine.EventSystems;

public class RotatableObject : MonoBehaviour, IDragHandler
{
    public Transform objectTransform;
    public float sensitivity = 10f;

    public void OnDrag(PointerEventData pointerEventData) {
        if (Lean.Touch.LeanTouch.GetFingers(true).Count == 1) {
            float rotX = pointerEventData.delta.x * sensitivity * Mathf.Deg2Rad;
            float rotY = pointerEventData.delta.y * sensitivity * Mathf.Deg2Rad;

            objectTransform.Rotate(objectTransform.up, -rotX, Space.World);
            objectTransform.Rotate(Vector3.right, rotY, Space.World);
        }
    }
}
