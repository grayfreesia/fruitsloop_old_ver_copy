﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntermediateAssemblyPart : MonoBehaviour
{
	public GameObject draggableAssemblyPartImage;

	private DroppableAssemblyPart[] cashedDroppableAssemblyParts;
	public DroppableAssemblyPart[] CashedDroppableAssemblyParts
	{
		get
		{
			if (cashedDroppableAssemblyParts == null)
			{
				cashedDroppableAssemblyParts = GetComponentsInChildren<DroppableAssemblyPart>();
			}
			return cashedDroppableAssemblyParts;
		}
	}

	public bool IsRemainAllIntermediateAssemblyPart
	{
		get
		{
			for (int i = 0; i < CashedDroppableAssemblyParts.Length; i++)
			{
				if (!CashedDroppableAssemblyParts[i].body.activeSelf)
				{
					return true;
				}
			}
			return false;
		}
	}

	public void CompleteAnimate()
	{
		draggableAssemblyPartImage.SetActive (true);
		transform.SetParent (transform.parent.parent);
		Vector3 endPos = new Vector3 (8.9f, 3.2f, 0f);
		iTween.ScaleTo (gameObject, Vector3.one * 20, 1.5f);
		iTween.MoveTo (gameObject, iTween.Hash (
			"position", endPos, "time", 1.5f, "oncomplete", "EndAnimate", "easetype", iTween.EaseType.easeInQuad
		));
	}

	private void EndAnimate()
	{
		gameObject.SetActive (false);
		AssemblyController.Instance.finalAssemblyPart.gameObject.SetActive (true);
	}
}
