﻿using UnityEngine;
using UnityEngine.Events;
using Lean.Touch;

public class DoubleTapRecenter : MonoBehaviour
{
    // Event signature
    [System.Serializable]
    public class LeanFingerEvent : UnityEvent<LeanFinger> { }

    [Tooltip("If the finger is over the GUI, ignore it?")]
    public bool IgnoreIfOverGui;

    [Tooltip("If the finger started over the GUI, ignore it?")]
    public bool IgnoreIfStartedOverGui;

    [Tooltip("How many times must this finger tap before OnFingerTap gets called? (0 = every time)")]
    public int RequiredTapCount = 0;

    [Tooltip("How many times repeating must this finger tap before OnFingerTap gets called? (e.g. 2 = 2, 4, 6, 8, etc) (0 = every time)")]
    public int RequiredTapInterval;

    public LeanFingerEvent OnFingerTap;

    private Vector3 initialPosition;
	private Vector3 initialScale;

    #region Unity Methods
    private void Awake()
    {
		initialPosition = transform.position;
		initialScale = transform.lossyScale;
    }

    protected virtual void OnEnable()
    {
        // Hook events
        LeanTouch.OnFingerTap += FingerTap;
    }

    protected virtual void OnDisable()
    {
        // Unhook events
        LeanTouch.OnFingerTap -= FingerTap;
    }
    #endregion

    #region Event Trigger
    private void FingerTap(LeanFinger finger)
    {
        // Ignore?
        if (IgnoreIfOverGui == true && finger.IsOverGui == true)
        {
            return;
        }

        if (IgnoreIfStartedOverGui == true && finger.StartedOverGui == true)
        {
            return;
        }

        if (RequiredTapCount > 0 && finger.TapCount != RequiredTapCount)
        {
            return;
        }

        if (RequiredTapInterval > 0 && (finger.TapCount % RequiredTapInterval) != 0)
        {
            return;
        }

        // Call event
        OnFingerTap.Invoke(finger);
    }
    #endregion

    #region Public Methods
    public void Recenter()
    {
		transform.position = initialPosition;
		transform.localScale = initialScale;
    }
    #endregion
}
