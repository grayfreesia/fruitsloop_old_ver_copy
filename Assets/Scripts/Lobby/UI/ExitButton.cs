﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitButton : SimpleButton {
	public void Exit() {
		string title = "후르츠루프";
		string message = "진행하고 있는 코스를 종료하고 홈으로 돌아가시겠어요? 현재까지의 진행상황은 저장됩니다.";

		AlertController.Show (title, message, new AlertOptions {
			cancelButtonTitle = "아니요",
			okButtonTitle = "네", okButtonDelegate = () => {
				SaveLoad.Instance.Save();
				SceneController.Instance.LoadLobbyScene();
			},
		});
	}
}
