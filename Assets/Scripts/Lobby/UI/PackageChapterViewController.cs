﻿using UnityEngine;
using UnityEngine.UI;

public class PackageChapterViewController : ViewController {
	[SerializeField] private LobbyViewManager lobbyViewManager;

	// 패키지 제목을 표시하는 텍스트
	[SerializeField] private Text titleLabel;

	// 패키지의 챕터 카드의 스크롤
	[SerializeField] private ScrollRect scroll;

	[SerializeField] private GameObject firstGuide;
	[SerializeField] private GameObject uploadSourceCodeButton;

	// 패키지 데이터를 저장
	private PackageData packageData;
	
	// 패키지 챕터 보기 화면의 내용을 갱신하는 메서드
	public void UpdateContent(PackageData packageData) {
		this.packageData = packageData;

		// 패키지 제목
		titleLabel.text = packageData.title;

		// 챕터 카드를 모두 삭제
		foreach (Transform child in scroll.content) {
			Destroy(child.gameObject);
		}

		// 챕터 카드를 모두 생성
		for (int i = 0; i < packageData.chapterCards.Count; i++) {
			Instantiate(packageData.chapterCards[i].gameObject, scroll.content);
		}

		// scroll rect 를 처음으로
		scroll.horizontalNormalizedPosition = 0f;

		// Entity에 저장
		Entity.packageData = packageData;

#if UNITY_ANDROID || UNITY_EDITOR
		if (packageData.name == "game") {
			uploadSourceCodeButton.SetActive(false);
			firstGuide.SetActive(false);
		} else {
			uploadSourceCodeButton.SetActive(true);
			if (PlayerPrefs.GetInt("hasPlayed", 0) == 0) {
				// 앱이 처음이면
				firstGuide.SetActive(true);
				PlayerPrefs.SetInt("hasPlayed", 1);
			} else {
				// 앱이 처음이 아니면
				firstGuide.SetActive(false);
			}
		}
#endif

#if UNITY_IOS
		uploadSourceCodeButton.SetActive(false);
		firstGuide.SetActive(false);
#endif
	}

	// 뒤로가기 버튼을 눌렀을 때 호출되는 메서드
	public void OnClickBackButton() {
		lobbyViewManager.Pop();
	}
}
