﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyViewManager : MonoBehaviour {
	// 화면 전환 효과 시간
	[SerializeField] private float duration = 0.5f;

	// 화면의 계층을 저장하는 스택
	private Stack<ViewController> stackedView = new Stack<ViewController>();
	// 현재 화면을 저장
	private ViewController currentView = null;

	// 다음 계층의 뷰로 옮겨가는 처리를 수행하는 메서드
	public void Push(ViewController newView, bool animation = true) {
		if (currentView == null) {
			// 첫 뷰는 애니메이션 없이 표시한다.
			currentView = newView;
			return;
		}

		// 화면 변경 효과 실행
		var lastView = currentView;
		stackedView.Push(lastView);

		if (animation) {
			StartCoroutine(RunFadeTo(lastView, newView, duration));
		} else {
			ChangeView(lastView, newView);
		}

		// 새로운 뷰를 현재의 뷰로 저장
		currentView = newView;
	}

	// 이전 계층의 뷰로 되돌아가는 처리를 수행하는 메서드
	public void Pop() {
		if (stackedView.Count < 1) {
			// 이전 계층의 뷰가 없으므로 아무것도 없는 상태
			return;
		}

		// 화면 변경 효과 실행
		var lastView = currentView;
		var poppedView = stackedView.Pop();
		StartCoroutine(RunFadeTo(lastView, poppedView, duration));

		// 스택에서 가져온 뷰를 현재의 뷰로 저장
		currentView = poppedView;
	}

	// 애니매이션 없이 화면 전환
	private void ChangeView(ViewController fromView, ViewController toView) {
		// 화면 전환에 사용될 캔버스 그룹을 불러온다.
		var fromViewCanvasGroup = fromView.GetComponent<CanvasGroup>();
		var toViewCanvasGroup = toView.GetComponent<CanvasGroup>();

		fromViewCanvasGroup.blocksRaycasts = false;
		fromViewCanvasGroup.alpha = 0f;
		toViewCanvasGroup.blocksRaycasts = true;
		toViewCanvasGroup.alpha = 1f;
	}

	// 화면 전환 효과 기능
	private IEnumerator RunFadeTo(ViewController fromView, ViewController toView, float duration) {
		// 화면 전환에 사용될 캔버스 그룹을 불러온다.
		var fromViewCanvasGroup = fromView.GetComponent<CanvasGroup>();
		var toViewCanvasGroup = toView.GetComponent<CanvasGroup>();

		// 화면 전환 동안 현재 화면의 인터랙션을 차단한다.
		fromViewCanvasGroup.blocksRaycasts = false;

		// 현재 화면 투명 화면 전환
		var time = 0f;
		var halfDuration = duration / 2f;
		while (time < halfDuration) {
			time += Time.deltaTime;
			fromViewCanvasGroup.alpha = Mathf.InverseLerp(1f, 0f, time / halfDuration);
			yield return new WaitForEndOfFrame();
		}

		fromViewCanvasGroup.alpha = 0f;
		yield return new WaitForEndOfFrame();

		// 다음 화면 화면 전환
		time = 0f;
		while (time < halfDuration) {
			time += Time.deltaTime;
			toViewCanvasGroup.alpha = Mathf.InverseLerp(0f, 1f, time / halfDuration);
			yield return new WaitForEndOfFrame();
		}

		toViewCanvasGroup.alpha = 1f;
		yield return new WaitForEndOfFrame();
		
		// 다음화면의 인터랙션을 켠다.
		toViewCanvasGroup.blocksRaycasts = true;
	}
}
