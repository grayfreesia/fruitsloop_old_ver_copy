﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PackageDetailViewController : ViewController {
	// 로비 화면 매니저
	[SerializeField] private LobbyViewManager lobbyViewManager;
	// 패키지 제목을 표시하는 텍스트
	[SerializeField] private Text titleLabel;
	// 패키지 난이도를 표시하는 텍스트
	[SerializeField] private Text levelLabel;
	// 패키지 상세 설명을 표시하는 텍스트
	[SerializeField] private Text descriptionLabel;
	// 패키지 상세설명 아웃라인 이미지
	[SerializeField] private Image outline;
	// 패키지 구매하기 버튼의 배경 이미지
	[SerializeField] private Image buyButtonImage;
	// 패키지 구매하기 버튼의 타이틀
	[SerializeField] private Text buyButtonLabel;
	// 패키지 이미지
	[SerializeField] private Image packageImage;
	
	// 패키지 데이터를 저장
	private PackageData packageData;

	// 패키지 상세 보기 화면의 내용을 갱신하는 메서드
	public void UpdateContent(PackageData packageData) {
		// 패키지의 데이터를 저장해둔다.
		this.packageData = packageData;

		titleLabel.text = packageData.title;
		levelLabel.text = "난이도 : " + packageData.GetPackageLevelString(packageData.level);
		levelLabel.color = packageData.GetPackageLevelColor(packageData.level);
		descriptionLabel.text = packageData.detailDescription;
		outline.color = packageData.GetPackageLevelColor(packageData.level);
		buyButtonImage.color = packageData.GetPackageLevelColor(packageData.level);
		buyButtonLabel.text = "(할인가) 구매하기 " + string.Format("{0:#,0} 원", packageData.price);
		packageImage.sprite = packageData.imageSprite;
	}

	// 패키지 상세 보기 화면의 close 버튼을 눌렀을 때 호출되는 메서드
	public void Close() {
		lobbyViewManager.Pop();
	}

	#region 시작버튼을 눌렀을 경우의 기능 구현
	// 챕터 뷰 저장
	[SerializeField] private PackageChapterViewController packageChapterView;

	// 시작하기 버튼을 클릭했을 때 호출되는 메서드
	public void OnClickStartButton() {
		// 패키지 챕터 화면의 내용을 갱신한다.
		packageChapterView.UpdateContent(packageData);
		if (lobbyViewManager != null) {
			lobbyViewManager.Push(packageChapterView);
		}
	}
	#endregion
}
