﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SessionButtonGroup : MonoBehaviour {
	[SerializeField] private List<SessionButton> sessionButtons = new List<SessionButton>();

	 private void OnEnable() {
        CourseState.SessionChanged += SessionChanged;

		// 세션 버튼 이벤트 핸들러 등록
		for (int i = 0; i < sessionButtons.Count; i++) {
			if (sessionButtons[i] == null) {
				Debug.LogWarning("버튼이 연결되어있지 않습니다.");
				return;
			}

			var session = sessionButtons[i].session;
			sessionButtons[i].onClicked.AddListener(
				() => OnSessionSelect (session));
		}
    }

    private void OnDisable() {
        CourseState.SessionChanged -= SessionChanged;

		// 세션 버튼 이벤트 핸들러 제거
		for (int i = 0; i < sessionButtons.Count; i++) {
			sessionButtons[i].onClicked.RemoveAllListeners();
		}
    }

	private void SessionChanged (object sender, SessionChangedEventArgs e) {
		for (int i = 0; i < sessionButtons.Count; i++) {
			sessionButtons[i].SetHighlight(e.CurrentSession == sessionButtons[i].session);
		}
    }

	private void OnSessionSelect (CourseState.Session session) {
        if (CourseState.Instance.CurrentSession == session) {
            // 같은 세션을 눌렀을 경우
            return;
        }

        // 실행중인 블록팔레트가 있으면 실행을 멈춘다.
        if (CourseState.Instance.CurrentExecutionStatus == CourseState.ExecutionStatus.Play
            || CourseState.Instance.CurrentExecutionStatus == CourseState.ExecutionStatus.Pause) {
            AlertController.Show("후르츠루프", "현재 코딩 블록이 실행 중입니다!");
            return;
        }

        CourseState.Instance.CurrentSession = session;
    }
}
