﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackagePagingScrollviewController : PagingScrollviewController {
	#region 패키지를 페이징 스크롤에 생성하는 구현
	// 생성해야할 패키지 리스트
	[SerializeField] private List<Package> packages = new List<Package>();
	private List<Package> cachedPackages = new List<Package>();

	private void Awake() {
		// 패키지 프리팹을 불러온다.
		LoadPackage();
	}

	protected override void OnEnable() {
		base.OnEnable();
		PageChanged += OnPackagePageChanged;
	}

	protected override void OnDisable() {
		base.OnDisable();
		PageChanged -= OnPackagePageChanged;
	}

	private void LoadPackage() {
		// 저장되있는 데이터 삭제
		cachedPackages.Clear();

		// 패키지를 스크롤 목록에 생성하고 패키지 데이터를 저장한다.
		for (int i = 0; i < packages.Count; i++) {
			if (packages[i] == null) {
				Debug.LogWarning("패키지 정보가 연결되어있지 않습니다.");
				return;
			}
			var package = Instantiate(packages[i].gameObject, CachedScrollRect.content.transform).GetComponent<Package>();
			cachedPackages.Add(package);
		}
	}
	#endregion

	#region 패키지 페이지 스냅시 미리보기 정보를 갱신하는 구현
	// 패키지 미리보기 화면 뷰
	[SerializeField] private OverallViewController overallViewController;

	// 패키지 페이지가 스냅시 실행하는 메서드
	private void OnPackagePageChanged(object sender, PageChangedEventArgs e) {
		if (overallViewController != null) {
			int packageIndex;
			if (e.pageIndex < 0) {
				packageIndex = (ItemCount - Mathf.Abs(e.pageIndex % ItemCount)) % ItemCount;
			} else {
				packageIndex = e.pageIndex % ItemCount;
			}
			// 순차적인 페이지 인덱스를 패키지의 인덱스에 대응시킨다.
			overallViewController.UpdateContent(cachedPackages[packageIndex].packageData);
		}
	}
	#endregion
}
