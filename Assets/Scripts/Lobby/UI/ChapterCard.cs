﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ChapterCard : SimpleButton, IBeginDragHandler, IDragHandler, IEndDragHandler {
	public string chapterName;

	public void OnCardClick() {
		// 저장해야 할 데이터가 있는 경우 데이터를 저장하기 위해 이름을 저장한다.
		for (int i = 0; i < Entity.Courses.Length; i++) {
			if (Entity.Courses[i].CourseName == chapterName) {
                Entity.CurrentCourse = Entity.Courses[i];
                break;
            }
		}

		// 비동기로 씬을 불러온다.
		SceneController.Instance.LoadSceneByCourseName(chapterName);
	}
	
	#region 옆으로 드래그 하면서 카드가 클릭되지 않도록 하는 기능 구현
	private ScrollRect cachedParentScroll;
	public ScrollRect CachedParentScroll {
		get {
			if (cachedParentScroll == null) {
				cachedParentScroll = GetComponentInParent<ScrollRect>();
			}
			return cachedParentScroll;
		}
	}
	
	void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) {
		if (CachedParentScroll != null) {
			CachedParentScroll.OnBeginDrag(eventData);
		}
	}
	
	void IDragHandler.OnDrag(PointerEventData eventData) {
		if (CachedParentScroll != null) {
			CachedParentScroll.OnDrag(eventData);

			if (Mathf.Abs(eventData.delta.x) > 4f) {
				isPointerDown = false;
			}
		}
	}

	void IEndDragHandler.OnEndDrag(PointerEventData eventData) {
		if (CachedParentScroll != null) {
			CachedParentScroll.OnEndDrag(eventData);
		}
	}
	#endregion
}
