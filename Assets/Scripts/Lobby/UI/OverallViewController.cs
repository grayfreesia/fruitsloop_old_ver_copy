﻿using UnityEngine;
using UnityEngine.UI;

public class OverallViewController : ViewController {
	// 로비 화면 매니저
	[SerializeField] private LobbyViewManager lobbyViewManager;

	// 패키지 제목을 표시하는 텍스트
	[SerializeField] private Text titleLabel;
	// 패키지 난이도를 표시하는 텍스트
	[SerializeField] private Text levelLabel;
	// 패키지 간단 설명을 표시하는 텍스트
	[SerializeField] private Text descriptionLabel;
	// 패키지 미리보기 아웃라인 이미지
	[SerializeField] private Image outline;
	// 패키지 상세보기 버튼의 배경 이미지
	[SerializeField] private Image detailButtonImage;

	// 패키지 데이터를 저장
	private PackageData packageData;

	private void Start() {
		// 첫 화면으로 설정한다.
		if (lobbyViewManager != null) {
			lobbyViewManager.Push(this, false);
		}

		// Entity가 있다면 챕터카드 보기 화면을 호출한다.
		if (Entity.packageData != null) {
			packageChapterView.UpdateContent(Entity.packageData);
			lobbyViewManager.Push(packageChapterView, false);
		}
	}

	// 패키지 미리 보기 화면의 내용을 갱신하는 메서드
	public void UpdateContent(PackageData packageData) {
		// 패키지의 데이터를 저장해둔다.
		this.packageData = packageData;

		titleLabel.text = packageData.title;
		levelLabel.text = "난이도 : " + packageData.GetPackageLevelString(packageData.level);
		levelLabel.color = packageData.GetPackageLevelColor(packageData.level);
		descriptionLabel.text = packageData.prevDescription;
		outline.color = packageData.GetPackageLevelColor(packageData.level);

		// 게임의 경우 상세보기 버튼을 보이지 않는다.
		if (packageData.name.Equals("game")) {
			detailButtonImage.gameObject.SetActive(false);
		} else {
			// 패키지의 경우 상세보기 버튼을 보이고 색을 갱신한다.
			detailButtonImage.gameObject.SetActive(true);
			detailButtonImage.color = packageData.GetPackageLevelColor(packageData.level);
		}
	}

	#region 디테일 화면으로의 전환 구현
	// 디테일 뷰 저장
	[SerializeField] private PackageDetailViewController packageDetailView;
	// 상세보기 버튼을 클릭했을때 호출되는 메서드
	public void OnClickDetailButton() {
		// 디테일 화면의 내용을 갱신한다.
		packageDetailView.UpdateContent(packageData);
		// 화면을 디테일 하면으로 전환한다.
		if (lobbyViewManager != null) {
			lobbyViewManager.Push(packageDetailView);
		}
	}
	#endregion

	#region 시작버튼을 눌렀을 경우의 기능 구현
	// 챕터 뷰 저장
	[SerializeField] private PackageChapterViewController packageChapterView;
	
	// 시작하기 버튼을 클릭했을때 호출되는 메서드
	public void OnClickStartButton() {
		// 패키지 챕터 화면의 내용을 갱신한다.
		packageChapterView.UpdateContent(packageData);
		if (lobbyViewManager != null) {
			lobbyViewManager.Push(packageChapterView);
		}
	}
	#endregion
}
