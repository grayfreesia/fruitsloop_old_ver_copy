﻿using UnityEngine;
using UnityEngine.UI;

public class OptionalViewController : ViewController {
	[SerializeField] private RectTransform sliderViewport;
	[SerializeField] private Image background;
	[SerializeField] Color connectedColor;
	[SerializeField] Color disconnectedColor;
	[SerializeField] private GameObject sliderController;

	public void SetConnected(bool connected) {
		sliderController.SetActive(!connected);
		sliderViewport.sizeDelta = connected ? new Vector2(200f, 90f) : new Vector2(600f, 90f);
		background.color = connected ? connectedColor : disconnectedColor;
	}
}
