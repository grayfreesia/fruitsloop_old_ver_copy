﻿using UnityEngine;
using UnityEngine.UI;

public struct PartInformation {
	public string partTitle;
	public string partMessage;
	public Image partImage;
}

public class PartInformationPopupOpener : PopupOpener {
	public PartName arduinoPartName;
	public enum PartName { Arduino, Breadboard, Bluetooth, LED, Resistance,
					IlluminanceSensor, NeoPixel, UltrasonicSensor, Buzzer }

	[SerializeField] private Image partImage;

	private string partTitle;
	private string partMessage;

	private PartInformation partInformation;

	public override void OpenPopup() {
		popup = Instantiate(popupPrefab).GetComponent<PartInformationPopup>();
		popup.transform.localScale = Vector3.zero;
		popup.transform.SetParent(rootCanvas.transform, false);
		popup.UpdateContent(GetPartInformation());
		popup.Open();
		popup.gameObject.SetActive(true);
	}

	private PartInformation GetPartInformation() {
		switch (arduinoPartName) {
			case PartName.Arduino:
			{
				partTitle = "아두이노";
				partMessage = "아두이노는 여러가지 회로를\n제어하는 장치입니다.\n사람의 머리와 같은 역할을 해요";
				break;
			}
			case PartName.Breadboard:
			{
				partTitle = "브레드보드";
				partMessage = "브래드보드는 많은 전선을\n쉽게 연결하고 뺄 수 있게 만들었어요.\n잘 연결했는지 확인하기 편해요";
				break;
			}
			case PartName.Bluetooth:
			{
				partTitle = "블루투스";
				partMessage = "블루투스는 다른 블루투스 장치와\n연결하여 정보를 주고 받을 수 있어요.";
				break;
			}
			case PartName.LED:
			{
				partTitle = "LED";
				partMessage = "LED는 전류가 한쪽 방향으로\n흐르면서 불빛이 나요.\n우리말로는 발광다이오드라고 해요";
				break;
			}
			case PartName.Resistance:
			{
				partTitle = "저항";
				partMessage = "저항은 전기부품에 과도한 전류가 흐르지 않도록 해주는 역할을 해요.";
				break;
			}
			case PartName.IlluminanceSensor:
			{
				partTitle = "조도 센서";
				partMessage = "조도센서는 주변이 얼마나 밝은지 감지할 수 있어요. 센서 위에 지그재그 모양이 빛을 감지해요.";
				break;
			}
			case PartName.NeoPixel:
			{
				partTitle = "네오픽셀";
				partMessage = "네오픽셀은 한 줄로 연결된 LED의 한 종류에요. 개수가 아무리 많아져도 사용하는 전선 개수는 똑같아요.";
				break;
			}
			case PartName.UltrasonicSensor:
			{
				partTitle = "초음파 센서";
				partMessage = "초음파센서는 박쥐처럼 초음파를 이용해서 물체와의 거리를 감지할 수 있어요.";
				break;
			}
			case PartName.Buzzer:
			{
				partTitle = "피에조 부저";
				partMessage = "피에조 부저는 피에조를 이용하여 소리를 내는 작은 스피커예요";
				break;
			}
			default:
			{
				Debug.LogWarning("There is no Part information.");
				break;
			}
		}
		partInformation.partTitle = partTitle;
		partInformation.partMessage = partMessage;
		partInformation.partImage = partImage;
		return partInformation;
	}
}
