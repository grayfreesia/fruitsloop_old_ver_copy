﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PageControl : MonoBehaviour {

	[SerializeField] private Toggle indicatorBase;
	private List<Toggle> indicators = new List<Toggle>();

	private void Awake() {
		indicatorBase.gameObject.SetActive(false);
	}

	public void SetNumberOfPages(int number) {
		if (indicators.Count < number) {
			// 페이지 인디케이터 수가 지정된 페이지 수보다 적으면
            //  복사 원본 페이지 인디케이터로부터 새로운 페이지 인디케이터를 작성한다
			for(int i = indicators.Count; i < number; i++)
			{
				Toggle indicator = Instantiate(indicatorBase) as Toggle;
				indicator.gameObject.SetActive(true);
				indicator.transform.SetParent(indicatorBase.transform.parent);
				indicator.transform.localScale = indicatorBase.transform.localScale;
				indicator.isOn = false;
				indicators.Add(indicator);
			}
		} else if (indicators.Count > number) {
			// 페이지 인디케이터 수가 지정된 페이지 수보다 많으면 삭제한다
			for(int i = indicators.Count-1; i >= number; i--)
			{
				Destroy(indicators[i].gameObject);
				indicators.RemoveAt(i);
			}
		}
	}

	public void SetCurrentPage(int index) {
		if(index >= 0 && index <= indicators.Count-1)
		{
			// 지정된 페이지에 대응하되는 페이지 인디케이터를 ON으로 지정한다
            // 토글 그룹을 설정해두었으므로 다른 인디케이터는 자동으로 OFF가 된다
			indicators[index].isOn = true;
		} else {
			// 페이지 인덱스가 마이너스 이거나 범위를 벗어나면
			// 페이지를 맞게 설정한다.
			var pageIndex = Mathf.Abs(index % indicators.Count-1);
			indicators[pageIndex].isOn = true;
		}
	}
}
