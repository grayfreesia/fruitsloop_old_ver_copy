﻿using UnityEngine;
using UnityEngine.UI;

public class KitInformationViewOptions
{
	public string okButtonTitle;
	public System.Action okButtonDelegate;
}

public class KitInformationViewController : MonoBehaviour
{
	public Text titleLabel;
	public Image kitImage;
	public Text messageLabel;
	public Button okButton;
	public Text okButtonLabel;

	private System.Action okButtonDelegate;

	public void Show(
		string title, Image image, string message, KitInformationViewOptions options = null)
	{
		UpdateContent (title, image, message, options);
	}

	public void UpdateContent(
		string title, Image image, string message, KitInformationViewOptions options = null)
	{
		titleLabel.text = title;
		messageLabel.text = message;
		kitImage.sprite = image.sprite;

		if (options != null)
		{
			okButtonLabel.text = options.okButtonTitle ?? "닫기";
			okButtonDelegate = options.okButtonDelegate;
		}
		else
		{
			okButtonLabel.text = "닫기";
		}
	}

	public void OnPressOKButton()
	{
		if (okButtonDelegate != null)
		{
			okButtonDelegate.Invoke();
		}
		Dismiss();
	}

	private void Dismiss()
	{
		gameObject.SetActive (false);
	}
}
