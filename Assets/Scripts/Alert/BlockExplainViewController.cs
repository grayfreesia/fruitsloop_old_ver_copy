﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockExplainViewController : MonoBehaviour {
	public EasyTween focusViewEasyTween;
	public Transform focusingObjectParent;
	public Text messageText;
	public Button overlayButton;

	private Transform defaultParent;
	private GameObject cashedFocusingObject;

	private void Awake() {
		gameObject.SetActive(false);
	}

	public void Show(GameObject focusingObject, string message) {
		gameObject.SetActive(true);
		StartCoroutine(ShowAfterFrame(focusingObject, message));
	}

	private IEnumerator ShowAfterFrame(GameObject focusingObject, string message) {
		yield return null;
		overlayButton.interactable = true;
		UpdateContent(focusingObject, message);
		focusViewEasyTween.OpenCloseObjectAnimation();
	}

	public void Hide() {
		overlayButton.interactable = false;
		messageText.gameObject.SetActive(false);
		focusViewEasyTween.OpenCloseObjectAnimation();
	}

	public void ShowAfterAnimation() {
		messageText.gameObject.SetActive(true);
	}

	public void HideAfterAnimation() {
		Destroy(cashedFocusingObject);
		gameObject.SetActive(false);
	}

	public void UpdateContent(GameObject focusingObject, string message) {
		Vector2 position = ConvertWorldToRect(focusingObject.transform.position);
		defaultParent = focusingObject.transform.parent;
		RectTransform rect = focusingObject.transform as RectTransform;

		focusViewEasyTween.rectTransform.anchoredPosition = position;
		messageText.text = message;

		cashedFocusingObject = Instantiate(focusingObject, defaultParent);
		RectTransform copyRect = cashedFocusingObject.transform as RectTransform;
		copyRect.SetParent(focusingObjectParent);
		copyRect.sizeDelta = rect.sizeDelta;
		copyRect.anchorMin = Vector2.zero;
		copyRect.anchorMax = Vector2.zero;
		copyRect.pivot = Vector2.one * 0.5f;
		copyRect.sizeDelta = rect.sizeDelta;
		copyRect.anchoredPosition = position;
	}

	private Vector2 ConvertWorldToRect(Vector3 worldPos) {
		RectTransform canvasRect = transform as RectTransform;
		return worldPos / canvasRect.localScale.x;
	}
}
