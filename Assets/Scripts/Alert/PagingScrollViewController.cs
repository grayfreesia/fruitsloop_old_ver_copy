﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(ScrollRect))]
public class PagingScrollViewController : ViewController, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    // 부모 스크롤과 겹칠시 가로 스크롤 또는 세로 스크롤 여부 확인
    [SerializeField] bool checkScrollDirection = false;

    // 스냅 스피드
	[Range(5f,25f)]
	[SerializeField] private float scrollSpeed = 15f;

	// 자동 스냅 종료 시간
	[Range(0.1f,1f)]
	[SerializeField] private float autoSnapTime = 0.3f;

    // 페이지 컨트롤러
    [SerializeField] private PageControl pageControl;


    private bool scrollOther = false;
    private bool scrollOtherHorizontally = false;

    #region Properties
    private ScrollRect cachedScrollRect;
    public ScrollRect CachedScrollRect {
        get {
            if (cachedScrollRect == null) {
                cachedScrollRect = GetComponent<ScrollRect>();
            }
            return cachedScrollRect;
        }
    }

    private ScrollRect cachedParentScrollRect;
	private ScrollRect CachedParentScrollRect {
		get {
			if (cachedParentScrollRect == null) {
				cachedParentScrollRect = transform.parent.GetComponentInParent<ScrollRect>();
			}
			return cachedParentScrollRect;
		}
	}

    private GridLayoutGroup cachedGridLayoutGroup;
	public GridLayoutGroup CachedGridLayoutGroup {
		get {
			if (cachedGridLayoutGroup == null) {
				cachedGridLayoutGroup = CachedScrollRect.content.GetComponent<GridLayoutGroup>();
			}
			return cachedGridLayoutGroup;
		}
	}

    // GridLayoutGroup의 cellSize와 spacing을 이용하여 한 페이지의 폭을 계산한다.
    private float PageWidth {
        get {
            return -(CachedGridLayoutGroup.cellSize.x + CachedGridLayoutGroup.spacing.x);
        }
    }
    #endregion

    #region Field variable
	// 자동 스냅 시간 타이머
	private float timer;
	// 자동 스냅 중임을 나타내는 플래그
	private bool isAutoSnaping = false;
	// 최종적인 스크롤 위치
	private Vector2 destPosition;
	// 이전 페이지의 인덱스 저장
	private int prevPageIndex;
	#endregion


    #region Unity Methods
    private void Start() {
        scrollOtherHorizontally = CachedScrollRect.vertical;
        pageControl.SetNumberOfPages(CachedScrollRect.content.childCount);
        InitializePos();
    }

    // 프레임마다 Update 메서드 다음에 호출된다.
    private void LateUpdate()
    {
        if (isAutoSnaping) {
            timer += Time.deltaTime;
            CachedScrollRect.content.anchoredPosition =
                Vector2.Lerp(CachedScrollRect.content.anchoredPosition, destPosition, Time.deltaTime * scrollSpeed);

            // 자동 스냅 시간이 지나면 무조건 스냅을 멈춘다.
            if (timer >= autoSnapTime) {
                CachedScrollRect.content.anchoredPosition = destPosition;
                isAutoSnaping = false;
            }
        }
    }
    #endregion

    #region Public Methods
    public void InitializePos() {
        CachedScrollRect.horizontalNormalizedPosition = 0f;
        pageControl.SetCurrentPage(0);
    }

    public void SetPrevPage() {
        CachedScrollRect.content.anchoredPosition = destPosition;
        pageControl.SetCurrentPage(prevPageIndex);
    }
    #endregion

    #region Event Handlers
    // 드래그 조작이 시작될 때 호출된다.
    public void OnBeginDrag(PointerEventData e)
    {
        if (checkScrollDirection) {
            //Get the absolute values of the x and y differences so we can see which one is bigger and scroll the other scroll rect accordingly
            float horizontal = Mathf.Abs(e.position.x - e.pressPosition.x);
            float vertical = Mathf.Abs(e.position.y - e.pressPosition.y);
            if (scrollOtherHorizontally)
            {
                if (horizontal > vertical)
                {
                    scrollOther = true;
                    //disable the current scroll rect so it doesnt move.
                    CachedScrollRect.enabled = false;
                    CachedParentScrollRect.OnBeginDrag(e);
                    return;
                }
            }
            else if (vertical > horizontal)
            {
                scrollOther = true;
                //disable the current scroll rect so it doesnt move.
                CachedScrollRect.enabled = false;
                CachedParentScrollRect.OnBeginDrag(e);
                return;
            }
        }
        // 애니메이션이 재생 중임을 나타내는 플래그를 초기화한다.
        isAutoSnaping = false;
    }

    public void OnDrag(PointerEventData e) {
        // 스크롤이 부모와 겹치고, 스크롤 방향이 부모 스크롤이라면 부모를 스크롤 한다.
        if (checkScrollDirection && scrollOther) {
            CachedParentScrollRect.OnDrag(e);
        }
    }

    // 드래그 조작이 끝날 때 호출된다.
    public void OnEndDrag(PointerEventData e)
    {
        if (checkScrollDirection && scrollOther) {
            scrollOther = false;
            CachedScrollRect.enabled = true;
            CachedParentScrollRect.OnEndDrag(e);
            return;
        }

        // 스크롤 뷰가 현재 움직이고 있는 것을 멈춘다.
        CachedScrollRect.StopMovement();

        // 스크롤의 현재 위치로부터 맞출 페이지의 인덱스를 계산한다
		int pageIndex = 
			Mathf.RoundToInt((CachedScrollRect.content.anchoredPosition.x) / PageWidth);

        if (pageIndex == prevPageIndex && Mathf.Abs(e.delta.x) >= 4.5f)
        {
            // 일정 속도 이상으로 드래그를 조작하고 있을 때는 그 방향으로 한 페이지 넘긴다.
            CachedScrollRect.content.anchoredPosition += new Vector2(e.delta.x, 0f);
            pageIndex += (int)Mathf.Sign(-e.delta.x);
        }

        // 첫 페이지이거나 마지막 페이지일 때 그 이상 스크롤 하지 않는다.
        // 또한 한 페이지에 보이는 최대 페이지수 이하일 때는 스크롤 하지 않는다.
        if (pageIndex < 0)
        {
            pageIndex = 0;
        }
        else if (pageIndex > CachedGridLayoutGroup.transform.childCount-1)
		{
			pageIndex = CachedGridLayoutGroup.transform.childCount-1;
		}

        // 현재 페이지의 인덱스를 저장해둔다.
        prevPageIndex = pageIndex;

        // 최종적인 스크롤 위치를 계산한다
		float destX = pageIndex * PageWidth;
		destPosition = new Vector2(destX, CachedScrollRect.content.anchoredPosition.y);

        // 애니메이션이 재생 중임을 나타내는 플래그를 설정한다.
        isAutoSnaping = true;
        timer = 0;

        pageControl.SetCurrentPage(pageIndex);
    }
    #endregion
}
