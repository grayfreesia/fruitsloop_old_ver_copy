﻿using UnityEngine;
using UnityEngine.UI;

public class PartInformationPopup : Popup {
	[SerializeField] private Text titleLabel;
	[SerializeField] private Text messageLabel;
	[SerializeField] private Image partImage;

	public override void UpdateContent(object info) {
		var partInformation = (PartInformation) info;
		titleLabel.text = partInformation.partTitle;
		messageLabel.text = partInformation.partMessage;
		partImage.sprite = partInformation.partImage.sprite;
	}
}
