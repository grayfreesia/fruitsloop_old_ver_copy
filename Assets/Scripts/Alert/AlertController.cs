﻿using UnityEngine;
using UnityEngine.UI;

// 알림 표시 옵션을 지정하기 위한 클래스
public class AlertOptions
{
    // 기존에 켜져있는 알림창 무시
    public bool ignoreOpenedAlertView = false;
    // 취소 버튼 타이틀
    public string cancelButtonTitle;
    // 취소 버튼 대리자
    public System.Action cancelButtonDelegate;
    // OK 버튼 타이틀
    public string okButtonTitle;
    // OK 버튼 대리자
    public System.Action okButtonDelegate;
    // 알림창 위치
    public Vector2 viewPosition = Vector2.zero;
    // 알림창 오버레이 여부
    public bool overLayActive = true;
    // 알림창 오버레이 인터렉션 여부
    public bool overLayInteraction = false;
    // 애니메이션 여부
    public bool animate = false;
}

public class AlertController : MonoBehaviour
{
    #region Public Variables
    public RectTransform window;
    public GameObject overlay;
    public Text titleLabel;
    public Text messageLabel;
    public Button cancelButton;
    public Text cancelButtonLabel;
    public Button okButton;
    public Text okButtonLabel;
    public EasyTween windowEasyTween;
    public EasyTween overlayEasyTween;
    public AnimationCurve enterAnimation;
    public AnimationCurve exitAnimation;
    #endregion

    #region Fields
    private static GameObject prefab;
    private static bool isOn = false;
    private System.Action cancelButtonDelegate;
    private System.Action okButtonDelegate;
    private Vector2 viewPosition;
    private bool animate = false;
    private bool overLayActive = true;
    #endregion

    #region Consts
    private readonly string BasicOKButtonTitle = "확인";
    #endregion

    #region Static Methods
    public static AlertController Show(
        string title, string message, AlertOptions options = null)
    {
		// 알림창이 이미 열려 있으면 아무것도 하지 않는다.
        if (isOn && options != null && !options.ignoreOpenedAlertView)
        {
            return null;
        }

        if (prefab == null)
        {
            prefab = Resources.Load("AlertView") as GameObject;
        }

        GameObject go = Instantiate(prefab) as GameObject;
        AlertController alert = go.GetComponent<AlertController>();
        alert.UpdateContent(title, message, options);
		
        if (alert.animate)
        {
            // 알림창 애니메이션이 있다면 알림창 애니메이션 실행
            alert.windowEasyTween.SetAnimationPosition(
                new Vector2(0f, -1200f), alert.viewPosition, alert.enterAnimation, alert.exitAnimation);
            alert.windowEasyTween.OpenCloseObjectAnimation();
        }
        else
        {
            alert.window.anchoredPosition = alert.viewPosition;
        }

        // overLay가 있다면 overLay 애니메이션 실행
        if (alert.overLayActive)
        {
            alert.overlay.SetActive(true);
            alert.overlayEasyTween.OpenCloseObjectAnimation();
        }
        else
        {
            if (options.overLayInteraction)
            {
                alert.overlay.SetActive(false);
            }
            else
            {
                alert.overlay.SetActive(true);
            }
        }

        isOn = true;
        return alert;
    }
    #endregion

    #region Public Methods
    // 알림 뷰의 내용을 갱신한다.
    public void UpdateContent(
        string title, string message, AlertOptions options = null)
    {
        titleLabel.text = title;
        messageLabel.text = message;

        if (options != null)
        {
            // 표시 옵션이 지정돼 있을 때 옵션의 내용에 맞춰 버튼을 표시하거나 표시하지 않는다.
            cancelButton.transform.parent.gameObject.SetActive(
                options.cancelButtonTitle != null || options.okButtonTitle != null);

            cancelButton.gameObject.SetActive(options.cancelButtonTitle != null);
            cancelButtonLabel.text = options.cancelButtonTitle ?? "";
            cancelButtonDelegate = options.cancelButtonDelegate;

            okButton.gameObject.SetActive(options.okButtonTitle != null);
            okButtonLabel.text = options.okButtonTitle ?? "";
            okButtonDelegate = options.okButtonDelegate;

            // 알림창 위치 옵션
            viewPosition = options.viewPosition;
            // 오버레이 여부 옵션
            overLayActive = options.overLayActive;
            // 애니메이션 여부 옵션
            animate = options.animate;
        }
        else
        {
            // 표시 옵션이 지정돼 있지 않을 때 기본 버튼을 표시한다.
            cancelButton.gameObject.SetActive(false);
            okButton.gameObject.SetActive(true);
            okButtonLabel.text = BasicOKButtonTitle;
            viewPosition = Vector2.zero;
            overLayActive = true;
            animate = false;
        }
    }

    // 취소 버튼 클릭 호출 메서드
    public void OnPressCancelButton()
    {
        if (cancelButtonDelegate != null)
        {
            cancelButtonDelegate.Invoke();
        }
        Dismiss();
    }

    // OK 버튼 클릭 호출 메서드
    public void OnPressOKButton()
    {
        if (okButtonDelegate != null)
        {
            okButtonDelegate.Invoke();
        }
        Dismiss();
    }
    #endregion

    #region Private Methods
    // 알림 뷰 닫음
    private void Dismiss()
    {
        if (animate)
        {
            // 애니매이션 여부 
            windowEasyTween.OpenCloseObjectAnimation();
            if (overLayActive)
            {
                overlayEasyTween.OpenCloseObjectAnimation();
                overlay.SetActive(false);
            }
            Destroy(gameObject, 2f);
        }
        else
        {
            Destroy (gameObject);
        }

        isOn = false;
    }
    #endregion
}
