﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualButton : MonoBehaviour {
	[SerializeField] GameObject myContent;

	public void OnClickButton() {
		myContent.SetActive(!myContent.activeSelf);
	}
}
