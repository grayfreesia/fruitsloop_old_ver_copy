﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UploadProgress : MonoBehaviour {

	public Slider progressSlider;
	public Text progressPercentLabel;

	public void UpdateProgressValue(float value) {
		progressSlider.value = value;
	}

	// UI 업데이트
	public void UpdateUIPercent(float value) {
		progressPercentLabel.text = string.Format("{0} {1}", value.ToString(), "%");
	}

	// 업로드 취소
	public void CancelUpload() {
		StopCoroutine(Upload.Instance.uploadProcess);
		SetActive(false);
	}

	public void SetActive(bool on) {
		gameObject.SetActive(on);
		Upload.Instance.ProgressPercent = 0f;
		UpdateProgressValue(0f);
	}
}
