﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PackageData {
	// 패키지의 고유 이름
	public string name;
	// 패키지 이미지 스프라이트
	public Sprite imageSprite;
	// 패키지 제목
	public string title;
	// 패키지 가격
	public int price;
	// 패키지 레벨
	public enum Level { Basic, Middle, Advanced, Game }
	public Level level;
	// 패키지 간단 정보
	[TextArea(2,4)]
	public string prevDescription;
	// 패키지 상세 정보
	[TextArea(3,6)]
	public string detailDescription;

	public List<ChapterCard> chapterCards = new List<ChapterCard>();

	public string GetPackageLevelString(PackageData.Level level) {
		switch (level) {
			case PackageData.Level.Basic: return "초급단계";
			case PackageData.Level.Middle: return "중급단계";
			case PackageData.Level.Advanced: return "고급단계";
			case PackageData.Level.Game: return "입문단계";
			default: return "초급단계";
		}
	}

	public Color GetPackageLevelColor(PackageData.Level level) {
		switch (level) {
			case PackageData.Level.Basic: return new Color32(77, 199, 115, 255);
			case PackageData.Level.Middle: return new Color32(123, 196, 250, 255);
			case PackageData.Level.Advanced: return new Color32(255, 167, 197, 255);
			case PackageData.Level.Game: return new Color32(244, 203, 112, 255);
			default: return new Color32(77, 199, 115, 255);
		}
	}
}

public class Package : MonoBehaviour {
	public PackageData packageData;
}
