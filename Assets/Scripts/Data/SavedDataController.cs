﻿using UnityEngine;

public class SavedDataController : MonoBehaviour
{
    #region Fields
    [Header("Data struct")]
    public Course[] Courses;
    #endregion

    #region Unity Methods
    private void Awake()
    {
        InitializeSavedData();
    }
    #endregion

    #region Private Methods
    // 데이터를 초기화하고, 앱에서 사용할 데이터를 동기화한다.
    private void InitializeSavedData()
    {
        for (int i = 0; i < Courses.Length; i++)
        {
            var courseName = Courses[i].CourseName;
            var unitCount = Courses[i].UnitCount;

            var savedUnitIndex = PlayerPrefs.GetInt (courseName, 0);

            var completedPercent =
                Mathf.RoundToInt (((float)savedUnitIndex / (float)(unitCount)) * 100);

            var completed = completedPercent >= 100 ? true : false;

            // Debug.LogFormat ("{0} completed percent : {1}",
            //     courseName, completedPercent.ToString());
            
            Courses[i].SavedUnitIndex = Mathf.Min (savedUnitIndex, unitCount - 1);
            Courses[i].CompletedPercent = completedPercent;
            Courses[i].Completed = completed;
        }
        
        Entity.Courses = this.Courses;
    }
    #endregion
}
