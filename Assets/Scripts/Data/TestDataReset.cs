﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDataReset : MonoBehaviour
{
	public void ResetData()
	{
		PlayerPrefs.DeleteAll();
		SceneController.Instance.LoadLobbyScene();
	}
}
