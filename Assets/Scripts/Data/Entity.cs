﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public static class Entity
{
	public static Course[] Courses;
	public static Course CurrentCourse;
	public static PackageData packageData;
}

[Serializable]
public struct Course
{
	public string CourseName;
	public bool Completed;
	public int UnitCount;
	public int SavedUnitIndex;
	public int CompletedPercent;
}