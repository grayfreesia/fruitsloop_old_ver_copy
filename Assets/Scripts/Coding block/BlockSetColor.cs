﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockSetColor : Block
{
    NeoPixel neo;

    public readonly int pin = 11;
    public readonly int div = 10;
    // public bool isHigh = false;
    public Text r;
    public Text g;
    public Text b;
    //public float targetR, targetG, targetB;
    void Start()
    {
        neo = Arduino.Instance.modules[pin].GetComponent<NeoPixel>();
    }

    public override void Execute()
    {
        SetColor();
    }


    public override string UploadData()
    {
        string c = 9 + "_" + ((int.Parse(r.text) + div - 1) / div).ToString() + "_" + ((int.Parse(g.text) + div - 1) / div).ToString() + "_" + ((int.Parse(b.text) + div - 1) / div).ToString() + "_";
        return c;
    }


    public void SetColor()
    {
        int sum = int.Parse(r.text) + int.Parse(g.text) + int.Parse(b.text);
        float a =  Mathf.Sqrt(Mathf.Min(sum, 256)) / 16f;
        for (int i = 0; i < neo.colors.Length; i++)
        {

            if (Arduino.Instance.modules.Length > pin && Arduino.Instance.modules[pin] != null)
            {
                Arduino.Instance.modules[pin].SetColor(i, float.Parse(r.text)/sum, float.Parse(g.text)/sum, float.Parse(b.text)/sum, a);
            }
        }
    }

}