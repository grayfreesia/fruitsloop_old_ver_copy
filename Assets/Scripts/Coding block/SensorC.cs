﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SensorC : MonoBehaviour
{


    private Text text;
    private Slider slider;
    public int value = 0;
    public Sprite[] iconImages;
    public Image icon;
    public static SensorC Instance { get; private set; }
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        text = GetComponentInChildren<Text>();
        slider = GetComponentInChildren<Slider>();
        SetValue(0);
    }

    public void SetValue(int v)
    {
        value = v;
        slider.value = v * 0.1f;
        text.text = v.ToString();
    }

    public void SetIcon()
    {
        if (slider.value < 0.25f)
        {
            icon.sprite = iconImages[0];
        }
        else if (slider.value < 0.5f)
        {
            icon.sprite = iconImages[1];
        }
        else if (slider.value < 0.75f)
        {
            icon.sprite = iconImages[2];
        }
        else
        {
            icon.sprite = iconImages[3];
        }
        value = (int)(slider.value * 10f);
        text.text = value.ToString();

    }
}
