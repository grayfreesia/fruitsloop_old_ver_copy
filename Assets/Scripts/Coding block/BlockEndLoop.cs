﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockEndLoop : Block {

	public override string UploadData()
	{		
		return "2_" + Upload.Instance.loopIter.Pop() + "_";
	}
}
 