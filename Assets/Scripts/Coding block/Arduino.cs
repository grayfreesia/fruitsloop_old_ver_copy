﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arduino : MonoBehaviour {

	public Module[] modules;
	public static Arduino Instance{get; private set;}

	void Awake()
	{
		if(Instance == null)
		{
			Instance = this;
		}
	}

	public void OffAllModules()
	{
		for(int i = 0; i < modules.Length; i++)
		{
			if(modules[i] != null)
			{
				modules[i].OffModule();
			}
		}
	}
}
