﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockBuzzer : Block
{
    public Text pinNum;
   // public int digitalPin = 0;
    // public Toggle toggle;

    // public ToggleSlider toggleSlider;
    public Toggle toggle;
   // public bool isHigh = false;

    public override void Execute()
    {
        if (Arduino.Instance.modules.Length > int.Parse(pinNum.text) && Arduino.Instance.modules[int.Parse(pinNum.text)] != null)
        {
			 Arduino.Instance.modules[int.Parse(pinNum.text)].DigitalWrite(toggle.isOn);
        }
        
        //BluetoothController.Instance.SendCommand(GetInfo() + "\n");
    }

    public override string UploadData()
    {
        string pin = (int.Parse(pinNum.text) - 4 + (toggle.isOn ? 8 : 0)).ToString();
        return "7_" + pin + "_";
    }
 
}
