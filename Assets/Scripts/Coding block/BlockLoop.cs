﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockLoop : Block
{

    public Text iterNum;

    private int childNum;

    public override void Execute()
    {
        childNum = GetComponentsInChildren<Block>(true).Length - 1;
        
        int iter;
        if (iterNum.text.Equals("무한"))
        {
            iter = 999999;
        }
        else
        {
            iter = int.Parse(iterNum.text);
        }
        
        for (int i = 0; i < iter - 1; i++)
        {
            Execution.Instance.jumpList[Execution.Instance.blockNum + childNum].Add(Execution.Instance.blockNum);
        }
    }
    
    public override string UploadData()
    {
        int iter;
        if (iterNum.text.Equals("무한"))
        {
            iter = 100;
        }
        else
        {
            iter = int.Parse(iterNum.text)-1;
        }
        Upload.Instance.loopIter.Push(iter);
        return "1_";
    }
}
