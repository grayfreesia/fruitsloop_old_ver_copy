﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockNewLine : Block
{
    public override void Execute()
    {
        if(MyPaint.Instance != null)
        {
            MyPaint.Instance.newLine();
        }
    }
}