﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockColCondition : Block
{

    public Dropdown dropdown;
    
    private int childNum;

    public override void Execute()
    {
        childNum = GetComponentsInChildren<Block>().Length - 1;
		if(!Condition())
		{
			Execution.Instance.jumpList[Execution.Instance.blockNum].Add(Execution.Instance.blockNum + childNum);
		}

        
        MyPaint.Instance.ColHigh();
        
		// MissionChecker.Instance.SensorCheck();
    }

    bool Condition()
    {
        if(MyPaint.Instance != null)
        {
            if(MyPaint.Instance.Cursor % 2 == dropdown.value)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    
}

