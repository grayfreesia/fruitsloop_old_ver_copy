﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockLineCondition : Block
{
    public Toggle toggle;
    // public Dropdown dropdown;
    private int childNum;

    public override void Execute()
    {
        childNum = GetComponentsInChildren<Block>().Length - 1;
		if(!Condition())
		{
			Execution.Instance.jumpList[Execution.Instance.blockNum].Add(Execution.Instance.blockNum + childNum);
		}
        // MyPaint.Instance.LineHigh();
		// MissionChecker.Instance.SensorCheck();
    }

    bool Condition()
    {
        // dropdown 값은 0 이면 홀수 / 1이면 짝수
        // toggle 값은 isOn이 true 이면 짝수 / isOn이 false 이면 홀수
        
        int temp = toggle.isOn ? 1 : 0;
        if(MyPaint.Instance != null)
        {
            if((MyPaint.Instance.Cursor / MyPaint.Instance.size) % 2 == temp)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    
}

