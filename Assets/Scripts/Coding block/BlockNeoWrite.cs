﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockNeoWrite : Block
{
    //public Text pinNum;
	public readonly int pin = 10;
   // public int digitalPin = 0;
    public Toggle toggle;
   // public bool isHigh = false;

    public override void Execute()
    {
        if (Arduino.Instance.modules.Length > pin && Arduino.Instance.modules[pin] != null)
        {
			 Arduino.Instance.modules[pin].DigitalWrite(toggle.isOn);    
			//  Debug.Log(toggle.isOn);
        }
        BluetoothController.Instance.SendCommand(toggle.isOn?"no":"nx" + "\n");
    }
}
