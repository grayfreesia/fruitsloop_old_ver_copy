﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockDropdownColor : MonoBehaviour {

	Image image;
	void Start()
	{
		image = GetComponent<Image>();
		SetColor(transform.parent.GetSiblingIndex());
	}

	void SetColor(int value)
	{
		switch(value)
		{
			case 1:
			{
				image.color = HexToColor32Converter.SoftR;
				break;
			}
			case 2:
			{
				image.color = HexToColor32Converter.SoftG;
				break;
			}
			case 3:
			{
				image.color = HexToColor32Converter.SoftB;
				break;
			}
			default:
			{
				break;
			}
		}
	}
}
