﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NeoPixel : Module
{
    public Image[] colors;
    public Image[] lights;
    public Image black;

    public override void SetColor(int index, float r, float g, float b, float a)
    {
        colors[index].color = new Color(r, g, b, a);
        lights[index].color = new Color(1f, 1f, 1f, a);
    }

    public void Black(bool isOn)
    {
        black.color = new Color(0f, 0f, 0f, isOn?0.8f:0f);
    }

}
