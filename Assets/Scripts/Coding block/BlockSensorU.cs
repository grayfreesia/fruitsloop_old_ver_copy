﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockSensorU : Block
{

    public Text valueLabel;
    public Toggle toggle;
    private int childNum;

    public override void Execute()
    {
        childNum = GetComponentsInChildren<Block>(true).Length - 1;
        if (!Condition())
        {
            Execution.Instance.jumpList[Execution.Instance.blockNum].Add(Execution.Instance.blockNum + childNum);
        }
        // MissionChecker.Instance.SensorCheck();
    }

    public override string UploadData()
    {
        string range;
        if (toggle.isOn) {
            // 거리가 멀면
            range = (121 + int.Parse(valueLabel.text) * 11 + 10).ToString();
        } else {
            // 거리가 가까우면
            range = (121 + int.Parse(valueLabel.text)).ToString();
        }
        // string range = (121 + int.Parse(numbers[0].text) * 11 + int.Parse(numbers[1].text)).ToString();
        return "3_" + range + "_";
    }

    bool Condition()
    {
        if (toggle.isOn) {
            // 거리가 멀면
            if (SensorU.Instance.value >= int.Parse(valueLabel.text)) {
                return true;
            } else {
                return false;
            }
        } else {
            // 거리가 가까우면
            if (SensorU.Instance.value <= int.Parse(valueLabel.text)) {
                return true;
            } else {
                return false;
            }
        }

        // if (int.Parse(numbers[0].text) <= SensorU.Instance.value && SensorU.Instance.value <= int.Parse(numbers[1].text))
        // {
        //     return true;
        // }
        // else
        // {
        //     return false;
        // }
    }
}

