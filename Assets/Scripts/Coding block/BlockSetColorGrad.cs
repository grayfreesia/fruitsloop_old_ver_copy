﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockSetColorGrad : Block
{
    NeoPixel neo;

    public readonly int pin = 11;
    // public bool isHigh = false;
    public Image[] target;
    //public float targetR, targetG, targetB;
    private float r, g, b, a;
    private float grad = 0.04f;
    public static BlockSetColorGrad prev;
    public Coroutine coloring;
    void Start()
    {
        neo = Arduino.Instance.modules[pin].GetComponent<NeoPixel>();
    }

    public override void Execute()
    {
        if(prev != null && prev.coloring != null)
        {
            prev.StopCoroutine(prev.coloring);
        }
        prev = this;
        coloring = StartCoroutine(SetColor());
    }


    public override string UploadData()
    {
        string c = 8 + "_" +(GetColorInfo(0) * 12 + GetColorInfo(1)).ToString() + "_" + (GetColorInfo(2) * 12 + GetColorInfo(3)).ToString() + "_";
        return c;
    }
    IEnumerator SetColor()
    {
        for (int i = 0; i < 100; i++)
        {
            SetColorGrad();
            yield return null;
        }
    }

    public void SetColorGrad()
    {
        for (int i = 0; i < target.Length; i++)
        {
            if (target[i].color == Color.black)
            {
                if (neo.colors[i].color.a > grad)
                {
                    a = neo.colors[i].color.a - grad;
                }
                else
                {
                    a = 0;
                }
                r = neo.colors[i].color.r;
                g = neo.colors[i].color.g;
                b = neo.colors[i].color.b;
                if (Arduino.Instance.modules.Length > pin && Arduino.Instance.modules[pin] != null)
                {
                    Arduino.Instance.modules[pin].SetColor(i, r, g, b, a);
                }
                continue;
            }
            else if (neo.colors[i].color.a + grad < 1f)
            {
                a = neo.colors[i].color.a + grad;
            }
            else
            {
                a = 1f;
            }

            if (neo.colors[i].color.r > target[i].color.r + grad)
            {
                r = neo.colors[i].color.r - grad;
            }
            else if (neo.colors[i].color.r + grad < target[i].color.r)
            {
                r = neo.colors[i].color.r + grad;
            }
            else
            {
                r = target[i].color.r;
            }

            if (neo.colors[i].color.g > target[i].color.g + grad)
            {
                g = neo.colors[i].color.g - grad;
            }
            else if (neo.colors[i].color.g + grad < target[i].color.g)
            {
                g = neo.colors[i].color.g + grad;
            }
            else
            {
                g = target[i].color.g;
            }

            if (neo.colors[i].color.b > target[i].color.b + grad)
            {
                b = neo.colors[i].color.b - grad;
            }
            else if (neo.colors[i].color.b + grad < target[i].color.b)
            {
                b = neo.colors[i].color.b + grad;
            }
            else
            {
                b = target[i].color.b;
            }

            if (Arduino.Instance.modules.Length > pin && Arduino.Instance.modules[pin] != null)
            {
                Arduino.Instance.modules[pin].SetColor(i, r, g, b, a);
            }
        }

    }

    public int GetColorInfo(int index)
    {
        if (target[index].color == Color.red)
        {
            return 0;
        }
        else if (target[index].color == new Color32(255, 127, 0, 255))
        {
            return 1;
        }
        else if (target[index].color == new Color32(255, 255, 0, 255))
        {
            return 2;
        }
        else if (target[index].color == new Color32(127, 255, 0, 255))
        {
            return 3;
        }
        else if (target[index].color == Color.green)
        {
            return 4;
        }
        else if (target[index].color == new Color32(0, 255, 127, 255))
        {
            return 5;
        }
        else if (target[index].color == new Color32(0, 127, 255, 255))
        {
            return 6;
        }
        else if (target[index].color == Color.blue)
        {
            return 7;
        }
        else if (target[index].color == new Color32(127, 0, 255, 255))
        {
            return 8;
        }
        else if (target[index].color == new Color32(255, 0, 255, 255))
        {
            return 9;
        }
        else if (target[index].color == Color.white)
        {
            return 10;
        }
        else if (target[index].color == Color.black)
        {
            return 11;
        }
        else return 11;
    }

    public void SetColorInfo(int index, int value)
    {
        if (value == 0 )
        {
            target[index].color = Color.red;        
        }
        else if (value == 1 )
        {
            target[index].color = new Color32(255, 127, 0, 255);        
        }
        else if (value == 2 )
        {
            target[index].color = new Color32(255, 255, 0, 255);        
        }
        else if (value == 3 )
        {
            target[index].color = new Color32(127, 255, 0, 255);        
        }
        else if (value == 4 )
        {
            target[index].color = Color.green;        
        }
        else if (value == 5 )
        {
            target[index].color = new Color32(0, 255, 127, 255);        
        }
        else if (value == 6 )
        {
            target[index].color = new Color32(0, 127, 255, 255);        
        }
        else if (value == 7 )
        {
            target[index].color = Color.blue;        
        }
        else if (value == 8 )
        {
            target[index].color = new Color32(127, 0, 255, 255);        
        }
        else if (value == 9 )
        {
            target[index].color = new Color32(255, 0, 255, 255);        
        }
        else if (value == 10 )
        {
            target[index].color = Color.white;        
        }
        else if (value == 11 )
        {
            target[index].color = Color.black;        
        }
    }
}