﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockTutorial : Block {
	public override void Execute() {
		string title = "후르츠루프";
		string message = "블록이 실행되었습니다!";
		AlertController.Show(title, message, new AlertOptions {
			okButtonTitle = "확인", okButtonDelegate = () => {
				TutorialManager.Instance.CompletedTutorial();
			},
			animate = false,
		});
	}
}
