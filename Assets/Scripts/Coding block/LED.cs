﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LED : Module
{
    public bool isOn = false;
    public Sprite onLED;
    public Sprite offLED;

    public override void DigitalWrite(bool high)
    {
        GetComponent<Image>().sprite = high ? onLED : offLED;
        isOn = high;
    }

    public override void OffModule()
    {
        GetComponent<Image>().sprite = offLED;
        isOn = false;
    }
}
