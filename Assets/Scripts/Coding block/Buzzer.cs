﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Buzzer : Module
{
    private bool isOn = false;
    public bool IsOn
    {
        get
        {
            return isOn;
        }
        set
        {
            if (isOn != value)
            {
                if (value)
                {
                    GetComponent<AudioSource>().Play();
                }
                else
                {
                    GetComponent<AudioSource>().Stop();
                }
				isOn = value;
            }
			
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            DigitalWrite(!isOn);
        }
    }
    public override void DigitalWrite(bool high)
    {       
        IsOn = high;
    }

    public override void OffModule()
    {
        IsOn = false;
    }
}
