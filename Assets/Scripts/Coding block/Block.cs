﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Block : MonoBehaviour
{
    Image image;
    Color originColor;
    Color32 activeColor = new Color32(113, 192, 255, 255);

    private void Awake()
    {
        image = GetComponent<Image>();
        if(image != null)
		{
			originColor = image.color;
		}
    }

    public virtual void Execute()
    {
    }

    public virtual string UploadData()
    {
        return "";
    }
    public void SetActiveColor(bool active)
    {
        if (gameObject.activeSelf)
        {
            if (active && image != null)
            {
                image.color = activeColor;
            }
            else if (!active && image != null)
            {
                image.color = originColor;
            }
        }
    }
}
