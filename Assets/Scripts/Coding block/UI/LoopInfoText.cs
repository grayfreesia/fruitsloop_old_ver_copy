﻿using UnityEngine;

public class LoopInfoText : MonoBehaviour {

	// info text gameobject
	[SerializeField] private GameObject infoText;

	void Update () {
		infoText.SetActive(transform.childCount < 1);
	}
}
