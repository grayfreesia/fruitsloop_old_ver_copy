﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleSwitch : MonoBehaviour {
	// public bool IsOn { get; set; }

	[SerializeField] Text label;
	[SerializeField] Image backgroundImage;
	[SerializeField] Color onColor;
	[SerializeField] Color offColor;

	[SerializeField] private string onLabel = "켜기";
	[SerializeField] private string offLabel = "끄기";

	private Toggle cachedToggle;
	private Toggle CashedToggle {
		get {
			if (cachedToggle == null) {
				cachedToggle = GetComponent<Toggle>();
			}
			return cachedToggle;
		}
	}

	public void Toggle(bool isOn) {
		label.text = isOn ? onLabel : offLabel;
		backgroundImage.color = isOn ? onColor : offColor;
	}
}
