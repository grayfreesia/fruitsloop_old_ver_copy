﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Button))]
public class InputFieldButton : MonoBehaviour
{
	#region Fields
	public InputPadController.PadType padType;
	public Image buttonBackgroundImage;
	public Text buttonLabel;
	#endregion

	#region Public Methods
	public void OnButtonClick()
	{
		InputPadController.Instance.OpenPad(padType, buttonBackgroundImage, buttonLabel);
	}
	#endregion
}
