﻿using UnityEngine;
using UnityEngine.UI;

public class FoldButton : MonoBehaviour
{
	#region Fields
	public Image buttonImage;
	public Sprite folded;
	public Sprite unfolded;
	public GameObject content;
	public GameObject foldedImage;
	[SerializeField] GameObject infoText;

	private bool isFolded = false;
	#endregion

	#region Public Methods
	public void OnPressButton()
	{
		isFolded = !isFolded;
		buttonImage.sprite = isFolded ? folded : unfolded;
		content.SetActive (!isFolded);
		foldedImage.SetActive (isFolded);
		
		if (infoText != null) {
			infoText.SetActive(!isFolded);
		}
	}
	#endregion
}
