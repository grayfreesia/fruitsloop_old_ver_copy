﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockPaintColor : Block
{
    public Image target;
    public override void Execute()
    {
        if (MyPaint.Instance != null)
        {
            MyPaint.Instance.SetColor(target != null ? target.color : Color.blue);
        }
    }

    public void SetColor()
    {
        switch (target.GetComponent<Dropdown>().value)
        {
            case 0:
                {
                    target.color = HexToColor32Converter.SoftR;
                    break;
                }
            case 1:
                {
                    target.color = HexToColor32Converter.SoftG;
                    break;
                }
            case 2:
                {
                    target.color = HexToColor32Converter.SoftB;
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    public int GetColorInfo()
    {
        if (target.color == Color.red)
        {
            return 0;
        }
        else if (target.color == new Color32(255, 127, 0, 255))
        {
            return 1;
        }
        else if (target.color == new Color32(255, 255, 0, 255))
        {
            return 2;
        }
        else if (target.color == new Color32(127, 255, 0, 255))
        {
            return 3;
        }
        else if (target.color == Color.green)
        {
            return 4;
        }
        else if (target.color == new Color32(0, 255, 127, 255))
        {
            return 5;
        }
        else if (target.color == new Color32(0, 127, 255, 255))
        {
            return 6;
        }
        else if (target.color == Color.blue)
        {
            return 7;
        }
        else if (target.color == new Color32(127, 0, 255, 255))
        {
            return 8;
        }
        else if (target.color == new Color32(255, 0, 255, 255))
        {
            return 9;
        }
        else if (target.color == Color.white)
        {
            return 10;
        }
        else if (target.color == Color.black)
        {
            return 11;
        }
        else if (target.color == HexToColor32Converter.SoftR)
        {
            return 12;
        }
        else if (target.color == HexToColor32Converter.SoftG)
        {
            return 13;
        }
        else if (target.color == HexToColor32Converter.SoftB)
        {
            return 14;
        }
        else return 0;
    }

    public void SetColorInfo(int value)
    {
        if (value == 0 )
        {
            target.color = Color.red;        
        }
        else if (value == 1 )
        {
            target.color = new Color32(255, 127, 0, 255);        
        }
        else if (value == 2 )
        {
            target.color = new Color32(255, 255, 0, 255);        
        }
        else if (value == 3 )
        {
            target.color = new Color32(127, 255, 0, 255);        
        }
        else if (value == 4 )
        {
            target.color = Color.green;        
        }
        else if (value == 5 )
        {
            target.color = new Color32(0, 255, 127, 255);        
        }
        else if (value == 6 )
        {
            target.color = new Color32(0, 127, 255, 255);        
        }
        else if (value == 7 )
        {
            target.color = Color.blue;        
        }
        else if (value == 8 )
        {
            target.color = new Color32(127, 0, 255, 255);        
        }
        else if (value == 9 )
        {
            target.color = new Color32(255, 0, 255, 255);        
        }
        else if (value == 10 )
        {
            target.color = Color.white;        
        }
        else if (value == 11 )
        {
            target.color = Color.black;        
        }
        else if (value == 12 )
        {
            target.color = HexToColor32Converter.SoftR;        
        }
        else if (value == 13 )
        {
            target.color = HexToColor32Converter.SoftG;        
        }
        else if (value == 14 )
        {
            target.color = HexToColor32Converter.SoftB;        
        }
        else
        {
            target.color = Color.red;        
        }
    }
}