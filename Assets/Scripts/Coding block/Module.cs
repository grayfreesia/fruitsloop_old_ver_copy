﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Module : MonoBehaviour {

	public virtual void DigitalWrite(bool high)
	{

	}

	public virtual void SetColor(int index, float r, float g, float b, float a)
	{

	}

	public virtual void OffModule()
	{
		// Debug.Log(name + "Off");
	}
}
