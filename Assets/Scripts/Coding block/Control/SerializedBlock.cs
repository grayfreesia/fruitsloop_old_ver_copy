﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
	public class SerializedBlock
	{
		public int type;
		public int data;
		public SerializedBlock(int type, int data)
		{
			this.type = type;
			this.data = data;
		}
	}
