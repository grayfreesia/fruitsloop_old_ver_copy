﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CreatorBlock : DraggableBlock, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    #region Public Variables
    public bool IsDraggingBlockCreated { get; set; }
    #endregion

    #region Fields
    private Timer timer = new Timer(0.1f);
    private bool isPointerDown = false;
    private PointerEventData cachedPointerEventData;
    #endregion

    private ScrollRect cachedScrollRect;
    public ScrollRect CachedScrollRect
    {
        get
        {
            if (cachedScrollRect == null)
            {
                cachedScrollRect = GetComponentInParent<ScrollRect>();
            }
            return cachedScrollRect;
        }
    }

    #region Event Hadlers
    public void OnPointerDown(PointerEventData pointerEventData)
    {
        isPointerDown = true;
        cachedPointerEventData = pointerEventData;
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        isPointerDown = false;
        timer.Reset();

        if (draggingBlock != null)
        {
            Destroy(draggingBlock);
            IsDraggingBlockCreated = false;
            SetAllCanvasGroupBlocksRaycasts(true);
        }
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        isPointerDown = false;
        timer.Reset();
    }
    #endregion

    #region Unity Methods
    private void Update()
    {
        if (isPointerDown)
        {
            // 블록 생성 버튼 클릭시 타이머를 잰다.
            timer.Update(Time.deltaTime);
            if (timer.IsTimeout)
            {
                // 지정된 타이머 시간이 지나면
                // 블록 생성
                base.OnBeginDrag(cachedPointerEventData);
                IsDraggingBlockCreated = true;
                SetAllCanvasGroupBlocksRaycasts(false);
                IsBlockCreated = false;

                // 타이머 초기화
                isPointerDown = false;
                timer.Reset();

                // 진동
                Vibration.Vibrate(40);
            }
        }
    }
    #endregion

    #region Override Methods
    public override void OnBeginDrag(PointerEventData pointerEventData)
    {
        if (!IsDraggingBlockCreated)
        {
            // 블록이 생성되지 않았다면 스크롤을 실행한다.
            if (CachedScrollRect) {
                CachedScrollRect.OnBeginDrag(pointerEventData);
            }
        }
    }

    public override void OnDrag(PointerEventData pointerEventData)
    {
        // 드래그할 블록이 생성되었다면 On Drag 실행
        if (IsDraggingBlockCreated)
        {
            base.OnDrag(pointerEventData);
        }
        else
        {
            // 드래그할 블록이 없다면 스크롤을 한다
            if (CachedScrollRect) {
                CachedScrollRect.OnDrag(pointerEventData);

                if (Mathf.Abs(pointerEventData.delta.y) > 4f)
                {
                    isPointerDown = false;
                    timer.Reset();
                }
            }
        }
    }

    public override void OnEndDrag(PointerEventData pointerEventData)
    {
        base.OnEndDrag(pointerEventData);
        if (CachedScrollRect) {
            CachedScrollRect.OnEndDrag(pointerEventData);
        }
    }
    #endregion
}
