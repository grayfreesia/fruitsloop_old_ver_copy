﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainBlockZone : DroppableZone
{
    #region Fields
    public EasyTween blockPaletteEasyTween;
    public GameObject infoText;
    public GameObject overlay;
    [Header("Auto Scroll")]
    [Range(20f, 150f)]
    public float scrollPadding = 55f;
    [Range(1f, 10f)]
    public float scrollVelocity = 8f;
    #endregion

    #region Properties
    public Block[] Blocks
    {
        get
        {
            return GetComponentsInChildren<Block>(true);
        }
    }

    public CanvasGroup[] CanvasGroups
    {
        get { return GetComponentsInChildren<CanvasGroup>(); }
    }

    private RectTransform cachedRectTransform;
    public RectTransform CachedRectTransform
    {
        get
        {
            if (cachedRectTransform == null)
            {
                cachedRectTransform = transform as RectTransform;
            }
            return cachedRectTransform;
        }
    }

    private ScrollRect cachedScrollRect;
    public ScrollRect CachedScrollRect
    {
        get
        {
            if (cachedScrollRect == null)
            {
                cachedScrollRect = GetComponent<ScrollRect>();
            }
            return cachedScrollRect;
        }
    }

    public static MainBlockZone Instance { get; private set; }
    #endregion

    #region Public Methods
    public void DeleteAll()
    {
        string title = "후르츠루프";
        string message = "블록 팔레트의 모든 블럭을 삭제할까요?";
        AlertController.Show(title, message, new AlertOptions
        {
            cancelButtonTitle = "아니요",
            okButtonTitle = "삭제",
            okButtonDelegate = () =>
{
DeleteAllImmediately();
},
        });
    }

    public void DeleteAllImmediately()
    {
        foreach (Transform t in transform.GetChild(0))
        {
            Destroy(t.gameObject);
        }
    }
    #endregion

    #region Unity Methods
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        if (SaveLoad.Instance != null)
        {
            SaveLoad.Instance.Load();
        }
    }

    private void OnEnable()
    {
        CourseState.ExecutionStatusChanged += ExecutionStatusChanged;
    }

    private void OnDisable()
    {
        CourseState.ExecutionStatusChanged -= ExecutionStatusChanged;
    }

    private void Update()
    {
        infoText.SetActive(CachedScrollRect.content.childCount <= 0);

        if (CachedScrollRect.content.sizeDelta.y > 300f && canAutoScroll && cachedPointerEventData != null)
        {
            // 콘텐츠 사이즈가 스크롤이 가능하면 위아래 스크롤 가능 영역에선 자동으로 스크롤 한다.
            AutoScroll();
        }
    }
    #endregion

    #region Override Methods
    public override void OnPointerEnter(PointerEventData pointerEventData)
    {
        if (pointerEventData.dragging)
        {
            // Debug.Log("outer");
            CreatorBlock d = pointerEventData.pointerDrag.GetComponent<CreatorBlock>();
            if (d != null)
            {
                if (d.blockType == DraggableBlock.BlockType.Creator && d.IsDraggingBlockCreated)
                {
                    if (d.placeholderBlock == null)
                    {
                        // 드래그중인 블록의 타입이 새로 생성하려는 블록이라면
                        // 드롭 영역에 들어온 블록과 똑같은 에시 블록을 생성한다
                        // 기본값으로 예시블록을 블록팔레트의 가장 상위 캔버스의 자식으로 종속시킨다
                        d.placeholderBlock = Instantiate(d.blockToDrag);
                        d.placeholderBlockParent = transform.GetChild(0);
                        CanvasGroup canvasGroup = d.placeholderBlock.GetComponent<CanvasGroup>();
                        canvasGroup.alpha = 0.4f;
                        canvasGroup.blocksRaycasts = false;
                    }
                }
                else if (d.blockType == DraggableBlock.BlockType.Created)
                {
                    // 드래그중인 블록의 타입이 생성된 블록이라면
                }
            }

            // Main Block Zone 에서만 자동 스크롤이 되게 하는 플래그
            canAutoScroll = true;
        }
    }

    public override void OnPointerExit(PointerEventData pointerEventData)
    {
        if (pointerEventData.dragging)
        {
            // 드래그 도중이라면 드롭 영역을 나갈을 때 예시 블록을 삭제한다
            CreatorBlock d = pointerEventData.pointerDrag.GetComponent<CreatorBlock>();
            if (d != null)
            {
                if (d.blockType == DraggableBlock.BlockType.Creator &&
                    d.placeholderBlock != null && d.IsDraggingBlockCreated)
                {
                    Destroy(d.placeholderBlock);
                    d.placeholderBlock = null;
                }
                else if (d.blockType == DraggableBlock.BlockType.Created)
                {
                    // 드래그중인 블록의 타입이 생성된 블록이라면
                }
            }

            // Main Block Zone 에서만 자동 스크롤이 되게 하는 플래그
            canAutoScroll = false;
        }
    }
    #endregion

    #region Private Methods
    private void ExecutionStatusChanged(object sender, ExecutionStatusChangedEventArgs e)
    {
        switch (e.ExecutionStatus)
        {
            case CourseState.ExecutionStatus.Play:
                {
                    Execution.Instance.Execute();
                    overlay.SetActive(true);
                    blockPaletteEasyTween.OpenCloseObjectAnimation();
                    break;
                }
            case CourseState.ExecutionStatus.Stop:
                {
                    Execution.Instance.Stop();
                    overlay.SetActive(false);
                    blockPaletteEasyTween.OpenCloseObjectAnimation();
                    break;
                }
            default:
                {
                    Debug.LogWarning("execution status is not play or stop.");
                    Execution.Instance.Stop();
                    overlay.SetActive(false);
                    blockPaletteEasyTween.OpenCloseObjectAnimation();
                    break;
                }
        }
    }
    #endregion

    #region 블록을 드래그 하고 있을 때 블록팔레트 상하단에서 자동 드래그 기능 구현

    public static bool canAutoScroll = false;
    public static PointerEventData cachedPointerEventData;

    // 자동 스크롤에 필요한 변수를 초기화 했는지를 확인하는 플래그
    private bool hasAutoScrollInited = false;
    // 자동 스크롤이 되는 상단 경계선
    private float aboveLine;
    // 자동 스크롤이 되는 하단 경계선
    private float belowLine;

    private void AutoScrollInit() {
        hasAutoScrollInited = true;

        var adjustedByScale = CachedRectTransform.rect.yMax * transform.lossyScale.y;
        var halfPositionY = (adjustedByScale + transform.position.y) * 0.5f;

        var startToScrollHeight = halfPositionY - scrollPadding;
        aboveLine = halfPositionY + startToScrollHeight;
        belowLine = halfPositionY - startToScrollHeight;
    }

    private void AutoScroll()
    {
        // 자동 스크롤 변수 초기화 여부를 확인한다.
        if (!hasAutoScrollInited) {
            AutoScrollInit();
        }

        // float startToScrollHeight =
        //     (CachedRectTransform.rect.height * 0.5f) - scrollPadding;
        // float aboveLine = transform.position.y + startToScrollHeight;
        // float belowLine = transform.position.y - startToScrollHeight;

        if (cachedPointerEventData.position.y > aboveLine)
        {
            // 위쪽 자동 스크롤
            // 일정한 속도
            CachedScrollRect.content.anchoredPosition -= Vector2.up * scrollVelocity;
            CachedScrollRect.content.anchoredPosition =
                Vector2.Max(Vector2.zero,
                CachedScrollRect.content.anchoredPosition);

            // 컨텐츠 길이에 비례하는 스크롤 속도
            // CachedScrollRect.verticalNormalizedPosition += scrollVelocity * Time.deltaTime;
            // CachedScrollRect.verticalNormalizedPosition = Mathf.Min (1, CachedScrollRect.verticalNormalizedPosition);
        }
        else if (cachedPointerEventData.position.y < belowLine)
        {
            // 아래 자동 스크롤

            // 일정한 속도
            CachedScrollRect.content.anchoredPosition += Vector2.up * scrollVelocity;
            CachedScrollRect.content.anchoredPosition =
                Vector2.Min(new Vector2(0f, CachedScrollRect.content.sizeDelta.y),
                CachedScrollRect.content.anchoredPosition);

            // 컨텐츠 길이에 비례하는 스크롤 속도
            // scrollRect.verticalNormalizedPosition -= scrollVelocity * Time.deltaTime;
            // scrollRect.verticalNormalizedPosition = Mathf.Max (0, scrollRect.verticalNormalizedPosition);
        }
    }
    #endregion
}
