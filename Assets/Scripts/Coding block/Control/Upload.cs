﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upload : MonoBehaviour {

	public Stack<int> loopIter = new Stack<int>();
	public UploadProgress uploadProgressView;
	float uploadTime = 0.03f;
	private Block[] blocks;
	public static Upload Instance{get; private set;}

	public Coroutine uploadProcess;

	private float progressPercent;
	public float ProgressPercent
	{
		get
		{
			return progressPercent;
		}
		set
		{
			progressPercent = value;
		}
	}

	void Awake()
	{
		if(Instance == null)
		{
			Instance = this;
		}
	}

	public void UploadBlock()
	{
		// 업로드 확인창
		string uploadTitle = "후르츠루프";
		string uploadMessage = "아두이노에 블록코딩한 내용을 전송 할까요?";
		AlertController.Show(uploadTitle, uploadMessage, new AlertOptions {
			cancelButtonTitle = "아니요",
			okButtonTitle = "전송", okButtonDelegate = () => {
				StartUploadProcess();
			},
			animate = false,
		});
	}

	public void StartUploadProcess() {
		blocks = MainBlockZone.Instance.Blocks;
		if (blocks.Length < 1) {
			AlertController.Show("전송 실패", "전송할 블록이 없습니다.", new AlertOptions {
				okButtonTitle = "확인",
				animate = true,
				ignoreOpenedAlertView = true
			});
			return;
		} else if (blocks.Length >= 500) {
			string noBlockTitle = "전송 실패";
			string noBlockMessage = "전송할 블록이 너무 많아요!\n 블록이 너무 많으면 전송할 때 문제가 생길 수 있어요.";
			AlertController.Show(noBlockTitle, noBlockMessage, new AlertOptions {
				okButtonTitle = "확인",
				animate = true,
				ignoreOpenedAlertView = true,
			});
			return;
		}

		// start upload
		uploadProgressView.SetActive(true);
		uploadProcess = StartCoroutine(UploadProcess());
	}

	IEnumerator UploadProcess()
	{
		BluetoothController.Instance.SendCommand("s" + GetCourseNum());
		yield return new WaitForSeconds(uploadTime);
		for(int i = 0; i < blocks.Length; i++)
		{
			// 블루투스 확인
			if (BluetoothController.Instance.Device == null || BluetoothController.Instance.state != BluetoothController.State.Connected)
			{
				// 블루투스 확인 알림창
				string bluetoothFailTitle = "전송 실패";
				string bluetoothFailMessage = "좌측 메뉴 탭에서 블루투스 연결을 확인해 주세요!";
				AlertController.Show(bluetoothFailTitle, bluetoothFailMessage, new AlertOptions {
					okButtonTitle = "확인", okButtonDelegate = () => {
						uploadProgressView.SetActive(false);
					},
					animate = false,
				});
				yield break;
			}
			string s = blocks[i].UploadData();
			BluetoothController.Instance.SendCommand(s);
			yield return new WaitForSeconds(uploadTime * s.Length);

			// UI 업데이트
			progressPercent = Mathf.Floor(((float)i / (float)blocks.Length) * 100);
			uploadProgressView.UpdateProgressValue (progressPercent);
		}
		BluetoothController.Instance.SendCommand("e");
		yield return new WaitForSeconds(uploadTime);

		// 업로드 완료
		// 업로드 완료 알림창
		uploadProgressView.SetActive(false);
		string uploadTitle = "전송 완료";
		string uploadMessage = "아두이노에 블록코딩한 내용이 전송되었습니다!";
		AlertController.Show(uploadTitle, uploadMessage, new AlertOptions {
			okButtonTitle = "확인",
			animate = false,
		});
	}

	string GetCourseNum()
	{
		switch(SceneController.Instance.ActiveSceneName)
		{
			case "TrafficLight" :
			{
				return "1";
			}
			case "StreetLamp" :
			{
				return "2";
			}
			case "Security" :
			{
				return "3";
			}
			case "ColorMixing" :
			{
				return "4";
			}
			case "AcrylicLamp" :
			{
				return "5";
			}
			default :
			{
				Debug.LogWarning("no course name");
				return "0";
			}
		}
	}
}
