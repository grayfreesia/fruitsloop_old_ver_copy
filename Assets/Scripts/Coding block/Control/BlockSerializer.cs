﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockSerializer : MonoBehaviour {

	public SerializedBlock GetSerializedData(Block block)
	{
		int type = 0;
		int data = 0;
		if(block.GetType() == typeof(BlockLoop))
		{
			type = 1;
			Text t = block.GetComponentInChildren<Text>();
			if(t.text == "무한")
			{
				data = 100;
			}
			else
			{
				data = int.Parse(t.text);
			}
		}
		else if(block.GetType() == typeof(BlockEndLoop))
		{
			type = 2;
		}
		else if(block.GetType() == typeof(BlockSensorC))
		{
			type = 3;
			Text v = block.GetComponent<BlockSensorC>().valueLabel;
			Toggle t = block.GetComponent<BlockSensorC>().toggle;
			data = int.Parse(v.text) + (t.isOn?11:0);
			// Text[] t = block.GetComponent<BlockSensorC>().numbers;
			// data = int.Parse(t[0].text) * 11 + int.Parse(t[1].text);
		}
		else if(block.GetType() == typeof(BlockSensorU))
		{
			type = 4;
			Text v = block.GetComponent<BlockSensorU>().valueLabel;
			Toggle t = block.GetComponent<BlockSensorU>().toggle;
			data = int.Parse(v.text) + (t.isOn?11:0);
			// Text[] t = block.GetComponent<BlockSensorU>().numbers;
			// data = int.Parse(t[0].text) * 11 + int.Parse(t[1].text);
		}
		else if(block.GetType() == typeof(BlockEndIf))
		{
			type = 5;
		}
		else if(block.GetType() == typeof(BlockDelay))
		{
			type = 6;
			Text t = block.GetComponentInChildren<Text>();
			data = (int)(float.Parse(t.text) * 10);
		}
		else if(block.GetType() == typeof(BlockWrite))
		{
			type = 7;
			Dropdown d = block.GetComponentInChildren<Dropdown>();
			Toggle t = block.GetComponentInChildren<Toggle>();
			data = d.value + (t.isOn?4:0);
		}
		else if(block.GetType() == typeof(BlockBuzzer))
		{
			type = 8;
			Dropdown d = block.GetComponentInChildren<Dropdown>();
			Toggle t = block.GetComponentInChildren<Toggle>();
			data = d.value + (t.isOn?4:0);
		}
		else if(block.GetType() == typeof(BlockSetColorGrad))
		{
			type = 9;
			BlockSetColorGrad b = block.GetComponent<BlockSetColorGrad>();	
			data = b.GetColorInfo(0) * 1728 + b.GetColorInfo(1) * 144 + b.GetColorInfo(2) * 12 +  b.GetColorInfo(3);
		}
		else if(block.GetType() == typeof(BlockPaintColor))
		{
			if(block.GetComponentInChildren<Dropdown>() != null)
			{
				type = 10;			
			}
			else
			{
				type = 11;
			}
			data = block.GetComponent<BlockPaintColor>().GetColorInfo();
		}
		else if(block.GetType() == typeof(BlockNewLine))
		{
			type = 12;			
		}
		else if(block.GetType() == typeof(BlockLineCondition))
		{
			type = 13;		
			// Dropdown d = block.GetComponentInChildren<Dropdown>();
			// data = d.value;
			Toggle t = block.GetComponent<BlockLineCondition>().toggle;
			data = t.isOn?1:0;
		}
		else if(block.GetType() == typeof(BlockColCondition))
		{
			type = 14;		
			Dropdown d = block.GetComponentInChildren<Dropdown>();
			data = d.value;
		}
		else if(block.GetType() == typeof(BlockSetColor))
		{
			type = 15;		
			Text r = block.GetComponent<BlockSetColor>().r;
			Text g = block.GetComponent<BlockSetColor>().g;
			Text b = block.GetComponent<BlockSetColor>().b;
			data = int.Parse(r.text) * 256 * 256 + int.Parse(g.text) * 256 + int.Parse(b.text);
		}

		

		return new SerializedBlock(type , data);
	}
}
