﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockDeserializer : MonoBehaviour
{

    public GameObject[] blockPrefabs;
    public List<Transform> stack = new List<Transform>();

    public void Deserialize(SerializedBlock blockData)
    {
        GameObject newBlock;
        if (stack.Count == 0 && MainBlockZone.Instance != null)
        {
            stack.Add(MainBlockZone.Instance.transform.GetChild(0));
        }


        if (blockData.type == 1)
        {
            newBlock = Instantiate(blockPrefabs[blockData.type], stack[stack.Count - 1]);
			stack.Add(newBlock.transform.GetChild(1));
            Text t = newBlock.GetComponentInChildren<Text>();
            if (blockData.data == 100)
            {
                t.text = "무한";
            }
            else
            {
				t.text = blockData.data.ToString();
            }
        }
        else if (blockData.type == 2)
        {
			stack.RemoveAt(stack.Count - 1);			
        }
        else if (blockData.type == 3)
        {
			newBlock = Instantiate(blockPrefabs[blockData.type], stack[stack.Count - 1]);
			stack.Add(newBlock.transform.GetChild(1));

			// Text[] t = newBlock.GetComponent<BlockSensorC>().numbers;
			// t[0].text = (blockData.data/11).ToString();
			// t[1].text = (blockData.data%11).ToString();

            Text v = newBlock.GetComponent<BlockSensorC>().valueLabel;
            Toggle t = newBlock.GetComponent<BlockSensorC>().toggle;
            t.isOn = blockData.data > 10;
            v.text = (blockData.data % 11).ToString();

            // v.text = (blockData.data/11)
        }
        else if (blockData.type == 4)
        {
			newBlock = Instantiate(blockPrefabs[blockData.type], stack[stack.Count - 1]);
			stack.Add(newBlock.transform.GetChild(1));

			// Text[] t = newBlock.GetComponent<BlockSensorU>().numbers;			
			// t[0].text = (blockData.data/11).ToString();
			// t[1].text = (blockData.data%11).ToString();

            Text v = newBlock.GetComponent<BlockSensorU>().valueLabel;
            Toggle t = newBlock.GetComponent<BlockSensorU>().toggle;
            t.isOn = blockData.data > 10;
            v.text = (blockData.data % 11).ToString();
        }
        else if (blockData.type == 5)
        {
			stack.RemoveAt(stack.Count - 1);						
        }
        else if (blockData.type == 6)
        {
			newBlock = Instantiate(blockPrefabs[blockData.type], stack[stack.Count - 1]);
			Text t = newBlock.GetComponentInChildren<Text>();
			t.text = ((float)(blockData.data) * 0.1f).ToString();
        }
        else if (blockData.type == 7)
        {
			newBlock = Instantiate(blockPrefabs[blockData.type], stack[stack.Count - 1]);
			Dropdown d = newBlock.GetComponentInChildren<Dropdown>();
			Toggle t = newBlock.GetComponentInChildren<Toggle>();
			t.isOn = blockData.data > 3;
			d.value = blockData.data % 4;	
        }
        else if (blockData.type == 8)
        {
			newBlock = Instantiate(blockPrefabs[blockData.type], stack[stack.Count - 1]);
			Dropdown d = newBlock.GetComponentInChildren<Dropdown>();
			Toggle t = newBlock.GetComponentInChildren<Toggle>();
			t.isOn = blockData.data > 3;
			d.value = blockData.data % 4;
        }
        else if (blockData.type == 9)
        {
			newBlock = Instantiate(blockPrefabs[blockData.type], stack[stack.Count - 1]);
			BlockSetColorGrad b = newBlock.GetComponent<BlockSetColorGrad>();
			b.SetColorInfo(0, blockData.data / 1728);
			b.SetColorInfo(1, (blockData.data % 1728) / 144);
			b.SetColorInfo(2, (blockData.data % 144) / 12);			
			b.SetColorInfo(3, blockData.data % 12);			
        }
        else if (blockData.type == 10)
        {
			newBlock = Instantiate(blockPrefabs[blockData.type], stack[stack.Count - 1]);
			newBlock.GetComponent<BlockPaintColor>().SetColorInfo(blockData.data);
        }
        else if (blockData.type == 11)
        {
			newBlock = Instantiate(blockPrefabs[blockData.type], stack[stack.Count - 1]);
			newBlock.GetComponent<BlockPaintColor>().SetColorInfo(blockData.data);			
        }
		else if (blockData.type == 12)
        {
			newBlock = Instantiate(blockPrefabs[blockData.type], stack[stack.Count - 1]);
        }
		else if (blockData.type == 13)
        {
			newBlock = Instantiate(blockPrefabs[blockData.type], stack[stack.Count - 1]);
			// newBlock.GetComponentInChildren<Dropdown>().value = blockData.data;
            Toggle t = newBlock.GetComponentInChildren<Toggle>();
            t.isOn = blockData.data == 1;
        }
        else if (blockData.type == 14)
        {
			newBlock = Instantiate(blockPrefabs[blockData.type], stack[stack.Count - 1]);
			newBlock.GetComponentInChildren<Dropdown>().value = blockData.data;		
        }
        else if (blockData.type == 15)
        {
			newBlock = Instantiate(blockPrefabs[blockData.type], stack[stack.Count - 1]);
			newBlock.GetComponent<BlockSetColor>().r.text = (blockData.data/(256 * 256)).ToString();
            newBlock.GetComponent<BlockSetColor>().g.text = ((blockData.data/256) % 256).ToString();
            newBlock.GetComponent<BlockSetColor>().b.text = (blockData.data%256).ToString();		
        }
    }
}
