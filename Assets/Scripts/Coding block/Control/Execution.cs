﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Execution : MonoBehaviour
{
    public float interval = 0f;
    [HideInInspector]
    public float intervalTime = 0f;
    [HideInInspector]
    public int blockNum;

    private Block[] blocks;
    private IEnumerator exeCoroutine;
    public IEnumerator delayCoroutine;
    public List<int>[] jumpList;

    //public int cValue = 0;
    //public float uValue = 0f;

    public static Execution Instance { get; private set; }
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void Execute()
    {
        intervalTime = interval;
        blocks = MainBlockZone.Instance.Blocks;
        jumpList = new List<int>[blocks.Length];
        for (int i = 0; i < blocks.Length; i++)
        {
            jumpList[i] = new List<int>();
        }
        exeCoroutine = ExecuteCoroutine();
        StartCoroutine(exeCoroutine);

        //  아두이노 초기화 코드와 관련. 다른 곳으로 옮겨야 함
        if (!SceneController.Instance.ActiveSceneName.Equals("PaintGame")) {
            if(Arduino.Instance.modules[11]!=null)
            {
                Arduino.Instance.modules[11].GetComponent<NeoPixel>().Black(true);
            }
        }
    }

    public void Stop()
    {
        if (exeCoroutine != null)
        {
            StopCoroutine(exeCoroutine);
        }

        if (delayCoroutine != null)
        {
            StopCoroutine(delayCoroutine);
        }

        StartCoroutine(ResetColor());


        //  아두이노 초기화 코드 다른 곳으로 옮겨야 함
        if (!SceneController.Instance.ActiveSceneName.Equals("PaintGame")) {
            if(Arduino.Instance != null)
            {
                Arduino.Instance.OffAllModules();
            }

            if(Arduino.Instance.modules[11]!=null)
            {
                Arduino.Instance.modules[11].GetComponent<NeoPixel>().Black(false);
            }
        }
    }

    IEnumerator ResetColor()
    {
        yield return new WaitForSeconds(.1f);
        for (int i = 0; i < blocks.Length; i++)
        {
            blocks[i].SetActiveColor(false);
        }
    }

    IEnumerator ExecuteCoroutine()
    {
        yield return new WaitForSeconds(intervalTime);//Delay
        if (blocks.Length == 0)
        {
            if (CourseState.Instance.CurrentExecutionStatus == CourseState.ExecutionStatus.Play)
            {
                string title = "후르츠루프";
                string message = "실행할 블록이 아무것도 없어요!\n블록 팔레트에 블록을 꾹 눌러 드래그해 보세요.";
                AlertController.Show(title, message, new AlertOptions
                {
                    okButtonTitle = "확인",
                    okButtonDelegate = () =>
{
    CourseState.Instance.CurrentExecutionStatus = CourseState.ExecutionStatus.Stop;
},
                    animate = true,
                });
            }
            yield return new WaitForSeconds(intervalTime);//Delay
        }
        else
        {
            for (blockNum = 0; blockNum < blocks.Length; blockNum++)
            {
                if (!(blocks[blockNum].GetType() == typeof(BlockEndIf) || blocks[blockNum].GetType() == typeof(BlockEndLoop)))
                {
                    blocks[blockNum].Execute();

                    blocks[blockNum].SetActiveColor(true);
                    // if ((blocks[blockNum].GetType() == typeof(BlockColCondition) || blocks[blockNum].GetType() == typeof(BlockLineCondition)))
                    // {
                    //     yield return new WaitForSeconds(0.02f);//Delay

                    // }
                    // else
                    //펭카소에서 조건블록 시간 빠르게 건너뛰는 부분
                    // {

                        yield return new WaitForSeconds(intervalTime);//Delay
                    // }
                    blocks[blockNum].SetActiveColor(false);

                }
                if (jumpList[blockNum].Count != 0)//Jump
                {
                    int temp = jumpList[blockNum][jumpList[blockNum].Count - 1];
                    jumpList[blockNum].RemoveAt(jumpList[blockNum].Count - 1);
                    blockNum = temp;
                }
            }
            CourseState.Instance.CurrentExecutionStatus = CourseState.ExecutionStatus.Stop;
        }
    }
}
