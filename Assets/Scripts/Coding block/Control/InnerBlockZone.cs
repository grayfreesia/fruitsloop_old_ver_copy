﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InnerBlockZone : DroppableZone
{
    #region Override Methods
	public override void OnPointerEnter(PointerEventData pointerEventData)
	{
		// 드래그 도중이라면 Content 영역을 들어왔을 때 예시 블록을 자식으로 만든다
        // if (pointerEventData.dragging)
        // {
        //     DraggableBlock d = pointerEventData.pointerDrag.GetComponent<DraggableBlock>();
        //     if (d != null)
        //     {
        //         d.placeholderBlockParent = transform;
        //     }
        // }
	}
	
	public override void OnPointerExit(PointerEventData pointerEventData)
	{
		// if (pointerEventData.dragging)
        // {
        //     // 드래그 도중이라면 Content 영역을 나갔을 때 예시 블록을 상위 content의 자식으로 만든다
        //     DraggableBlock d = pointerEventData.pointerDrag.GetComponent<DraggableBlock>();
        //     if (d != null && d.placeholderBlockParent == transform)
        //     {
        //         DroppableZone droppableZone = transform.parent.GetComponentInParent<DroppableZone>();
        //         if (droppableZone.type == DroppableZone.Type.Main)
        //         {
        //             d.placeholderBlockParent = droppableZone.transform.GetChild(0);
        //         }
        //         else if (droppableZone.type == DroppableZone.Type.Inner)
        //         {
        //             d.placeholderBlockParent = droppableZone.transform;
        //         }
        //     }
        // }
	}
    #endregion
}
