﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CreatedBlock : DraggableBlock, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    #region Fields
    public bool IsDraggingBlockCreated { get; set; }
    private Timer timer = new Timer(0.1f);
    private bool isPointerDown = false;
    private PointerEventData cashedPointerEventData;
    #endregion

    #region Properties
    private DeleteBlockZone cachedDeleteBlockZone;
    public DeleteBlockZone CachedDeleteBlockZone
    {
        get
        {
            if (cachedDeleteBlockZone == null)
            {
                cachedDeleteBlockZone = FindObjectOfType<ViewContainer>().GetComponentInChildren<DeleteBlockZone>(true);
            }
            return cachedDeleteBlockZone;
        }
    }

    private ScrollRect cachedScrollRect;
    public ScrollRect CachedScrollRect
    {
        get
        {
            if (cachedScrollRect == null)
            {
                cachedScrollRect = GetComponentInParent<ScrollRect>();
            }
            return cachedScrollRect;
        }
    }
    #endregion

    #region Event Hadlers
    public void OnPointerDown(PointerEventData pointerEventData)
    {
        isPointerDown = true;
        cashedPointerEventData = pointerEventData;
    }
    public void OnPointerUp(PointerEventData pointerEventData)
    {
        isPointerDown = false;
        timer.Reset();

        if (draggingBlock != null)
        {
            Destroy(draggingBlock);
            IsDraggingBlockCreated = false;
            SetAllCanvasGroupBlocksRaycasts(true);

            // 삭제영역을 끈다.
            CachedDeleteBlockZone.SetActive(false);
            CachedDeleteBlockZone.gameObject.SetActive(false);

            if (placeholderBlock != null)
            {
                // 예시 블록이 있다면 예시 블록을 활성화 시킨다
                if (onDeleteBlockZone)
                {
                    Destroy(placeholderBlock);
                    placeholderBlock = null;
                }
                else
                {
                    CanvasGroup canvasGroup = placeholderBlock.GetComponent<CanvasGroup>();
                    canvasGroup.alpha = 1f;
                    canvasGroup.blocksRaycasts = true;
                    placeholderBlock = null;
                    IsBlockCreated = true;
                }
            }
        }
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        isPointerDown = false;
        timer.Reset();
    }
    #endregion

    #region Unity Methods
    private void Update()
    {
        if (isPointerDown)
        {
            // 블록 생성 버튼 다운시 타이머를 잰다.
            timer.Update(Time.deltaTime);
            if (timer.IsTimeout)
            {
                // 지정된 타이머 시간이 지나면
                // 블록 생성
                base.OnBeginDrag(cashedPointerEventData);
                IsDraggingBlockCreated = true;
                SetAllCanvasGroupBlocksRaycasts(false);

                // 예시 블록을 자기 자신으로 만든다.
                placeholderBlock = blockToDrag;
                placeholderBlockParent = placeholderBlock.transform.parent;
                CanvasGroup canvasGroup = placeholderBlock.GetComponent<CanvasGroup>();
                if (canvasGroup == null)
                {
                    canvasGroup = placeholderBlock.AddComponent<CanvasGroup>();
                }
                canvasGroup.alpha = 0.4f;
                canvasGroup.blocksRaycasts = false;

                // 헤더 부분을 드래그 지점으로 지정한다.
                draggingOffset = blockToDrag.transform.position - transform.position;
                draggingOffset = new Vector2(0f, draggingOffset.y);

                // 삭제영역을 켠다.
                CachedDeleteBlockZone.gameObject.SetActive(true);

                // 타이머 초기화
                isPointerDown = false;
                timer.Reset();

                // 진동
                Vibration.Vibrate(40);
            }
        }
    }
    #endregion

    #region Override Methods
    public override void OnBeginDrag(PointerEventData pointerEventData)
    {
        if (!IsDraggingBlockCreated)
        {
            if (CachedScrollRect && CachedScrollRect.enabled) {
                CachedScrollRect.OnBeginDrag(pointerEventData);
            }
        }
    }

    public override void OnDrag(PointerEventData pointerEventData)
    {
        // 드래그할 블록이 생성되었다면 On Drag 실행
        if (IsDraggingBlockCreated)
        {
            base.OnDrag(pointerEventData);
        }
        else
        {
            if (CachedScrollRect && CachedScrollRect.enabled) {
                CachedScrollRect.OnDrag(pointerEventData);
            }
            if (pointerEventData.delta.y > 4f || pointerEventData.delta.y < -4f)
            {
                isPointerDown = false;
                timer.Reset();
            }
        }
    }

    public override void OnEndDrag(PointerEventData pointerEventData)
    {
        base.OnEndDrag(pointerEventData);
        
        if (CachedScrollRect && CachedScrollRect.enabled) {
            CachedScrollRect.OnEndDrag(pointerEventData);
        }
    }
    #endregion
}
