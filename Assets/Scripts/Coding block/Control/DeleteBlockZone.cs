﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DeleteBlockZone : DroppableZone, IDropHandler
{
	#region Fields
	public Image deleteBlockZoneImage;
	public Image closedTrashcanIcon;
	public Image openedTrashcanIcon;
	public Color normalColor;
	public Color highlightedColor;
	#endregion

	#region Override Methods
	public override void OnPointerEnter(PointerEventData pointerEventData)
	{
        if (pointerEventData.dragging)
        {
			// 드래그 도중이라면 삭제 영역을 하이라이트 색으로 바꾼다
			// 예시 블록은 잠시 끈다.
            DraggableBlock d = pointerEventData.pointerDrag.GetComponent<DraggableBlock>();
			if (d != null)
			{
				d.onDeleteBlockZone = true;
				SetActive (true);
			}
        }
	}

	public override void OnPointerExit(PointerEventData pointerEventData)
	{
		if (pointerEventData.dragging)
        {
			// 드래그 도중이라면 삭제 영역의 색을 되돌린다
			// 예시 블록은 다시 켠다
            DraggableBlock d = pointerEventData.pointerDrag.GetComponent<DraggableBlock>();
			if (d != null)
			{
				d.onDeleteBlockZone = false;
				SetActive (false);
			}
        }
	}
	#endregion

	#region EventHandlers
	public void OnDrop(PointerEventData pointerEventData)
	{
		SetActive (false);
	}
	#endregion

	public void SetActive(bool on)
	{
		deleteBlockZoneImage.color = on ? highlightedColor : normalColor;
		openedTrashcanIcon.enabled = on;
		closedTrashcanIcon.enabled = !on;
	}
}
