﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DraggableBlock : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    #region Fields
    // 드래그 할 블록
    [Header("The code block want to drag")]
    public GameObject blockToDrag;

    // 드래그 조작 중인 블록의 오프셋
    public Vector2 draggingOffset = new Vector2(0F, 0f);

    // 블록의 타입 - 생성자 블록 /생성된 블록
    public enum BlockType { Creator, Created }
    public BlockType blockType;

    // 캔버스의 Rect Transform 을 저장
    private RectTransform canvasRectTransform;
    #endregion

    #region Public variables
    // 드래그 조작 중인 블록의 게임 오브젝트를 저장
    public GameObject draggingBlock;

    // 예시 블록
    public GameObject placeholderBlock;

    // 예시 블록의 부모
    public Transform placeholderBlockParent;

    // 삭제영역 여부
    public bool onDeleteBlockZone = false;

    public bool IsBlockCreated { get; set; }
    #endregion

    #region Virtual Methods
    public virtual void OnBeginDrag(PointerEventData pointerEventData)
    {
        if (draggingBlock != null)
        {
            Destroy(draggingBlock);
        }

        // 블록의 태그를 비교해 생성할 블록의 프리팹을 가지고 와서
        // 드래그 조작 중인 블록의 게임 오브젝트를 생성한다.
        draggingBlock = Instantiate(blockToDrag);
        // 캔버스의 자식 요소로 종속시켜 가장 바깥면에 표시한다.
        draggingBlock.transform.SetParent(transform.root);
        draggingBlock.transform.SetAsLastSibling();

        RectTransform draggingRect = draggingBlock.transform as RectTransform;
        draggingRect.sizeDelta = blockToDrag.GetComponent<RectTransform>().sizeDelta;
        draggingRect.localScale = Vector3.one;
        // 드래그 하는 블럭을 약간 회전시킨다.
        draggingRect.Rotate(Vector3.forward, 8f);

        // Canvas Group 컴포넌트의 Block Raycasts 속성을 사용해 레이캐스트가 블록되지 않도록 한다.
        CanvasGroup canvasGroup = draggingBlock.GetComponent<CanvasGroup>();
        if (canvasGroup == null)
        {
            draggingBlock.AddComponent<CanvasGroup>().blocksRaycasts = false;
        }
        else
        {
            canvasGroup.blocksRaycasts = false;
        }

        // 캔버스의 Rect Transform을 저장해둔다.
        canvasRectTransform = draggingBlock.transform as RectTransform;

        // 드래그를 조작 중인 블록의 위치를 갱신한다.
        UpdateDraggingBlockPos(pointerEventData);

        // 자동 그래그 기능 설정을 위한 드래그 중이라는 플래그 설정
        MainBlockZone.canAutoScroll = true;
        MainBlockZone.cachedPointerEventData = pointerEventData;

        IsBlockCreated = false;
    }

    public virtual void OnDrag(PointerEventData pointerEventData)
    {
        // 드래그 중인 블록의 위치를 갱신한다
        UpdateDraggingBlockPos(pointerEventData);

        // 예시 블록이 있다면 예시 블록의 위치를 갱신한다
        if (placeholderBlock != null && !onDeleteBlockZone)
        {
            UpdatePlaceholderParent(pointerEventData);
            UpdatePlaceholderBlockPos(pointerEventData);
        }
    }

    public virtual void OnEndDrag(PointerEventData pointerEventData)
    {
        // 드래그 중인 게임 오브젝트를 삭제한다.
        if (draggingBlock != null)
        {
            Destroy(draggingBlock);
        }

        if (placeholderBlock != null)
        {
            // 예시 블록이 있다면 예시 블록을 활성화 시킨다
            if (onDeleteBlockZone)
            {
                Destroy(placeholderBlock);
                placeholderBlock = null;
            }
            else
            {
                CanvasGroup canvasGroup = placeholderBlock.GetComponent<CanvasGroup>();
                canvasGroup.alpha = 1f;
                canvasGroup.blocksRaycasts = true;
                placeholderBlock = null;
                IsBlockCreated = true;
            }
        }

        // 자동 드래그 기능을 위한 드래그 중이라는 플래그 해제
        MainBlockZone.canAutoScroll = false;
        MainBlockZone.cachedPointerEventData = null;
    }
    #endregion

    #region Private Methods
    private void UpdateDraggingBlockPos(PointerEventData pointerEventData)
    {
        if (draggingBlock != null)
        {
            // 드래그 중인 아이콘의 스크린 좌표를 계산한다.
            Vector3 screenPos = pointerEventData.position + draggingOffset;
            // 스크린 좌표를 월드 좌표로 변환한다.
            Camera camera = pointerEventData.pressEventCamera;
            Vector3 newPos;
            if (RectTransformUtility.ScreenPointToWorldPointInRectangle(
                canvasRectTransform, screenPos, camera, out newPos))
            {
                // 드래그 중인 아이콘의 위치를 월드 좌표로 설정한다.
                draggingBlock.transform.position = newPos;
                draggingBlock.transform.rotation = canvasRectTransform.rotation;
            }
        }
    }

    private void UpdatePlaceholderParent(PointerEventData pointerEventData) {
        if (pointerEventData.pointerCurrentRaycast.gameObject != null) {
            switch (pointerEventData.pointerCurrentRaycast.gameObject.tag.ToString()) {
                case "Block Container Zone": {
                    placeholderBlockParent = pointerEventData.pointerEnter.transform.GetChild(0);
                    break;
                }
                case "Block Container": {
                    placeholderBlockParent = pointerEventData.pointerEnter.transform;
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }

    private void UpdatePlaceholderBlockPos(PointerEventData pointerEventData)
    {
        if (placeholderBlock.transform.parent != placeholderBlockParent)
        {
            placeholderBlock.transform.SetParent(placeholderBlockParent, false);
        }

        // 드래그 중인 블록의 y 포지션 과 기존 블록의 y 포지션을 비교해 예시 블록의 자식 순서를 결정한다
        // 예시 블록이 속해 있는 부모의 자식 개수만큼 반복해서 검사한다
        // 드래그 중인 블록의 y 포지션이 기존 블록의 y 포지션보다 높은 경우 해당 반복구문 블록의 자식 순서를 저장한다
        // 해당 반복구문 블록의 자식 순서와 예시 블록의 자식 순서를 비교해서 예시 블록은 해당 반복구문 블록의 자식 순서보다 하나 낮게 설정한다
        int newSiblingIndex = placeholderBlockParent.childCount;
        for (int i = 0; i < placeholderBlockParent.childCount; i++)
        {
            if (pointerEventData.position.y >
                placeholderBlockParent.GetChild(i).position.y)
            {
                newSiblingIndex = i;

                if (placeholderBlock.transform.GetSiblingIndex() <
                    newSiblingIndex)
                {
                    newSiblingIndex--;
                }
                break;
            }
        }

        placeholderBlock.transform.SetSiblingIndex(newSiblingIndex);
    }

    protected void SetAllCanvasGroupBlocksRaycasts(bool on) {
        CanvasGroup[] canvasGroups = MainBlockZone.Instance.CanvasGroups;
        for (int i = 0; i < canvasGroups.Length; i++) {
            var rect = canvasGroups[i].transform as RectTransform;
            if (rect.rect.height < 200f) {
                canvasGroups[i].blocksRaycasts = on;
            }
        }
    }
    #endregion
}
