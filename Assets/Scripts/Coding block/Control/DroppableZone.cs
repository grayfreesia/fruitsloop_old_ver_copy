﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DroppableZone : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    #region Fields
	public enum Type { Main, Inner, Delete }
	public Type type;
    #endregion

    #region Virtual Methods
    public virtual void OnPointerEnter(PointerEventData pointerEventData)
    {
    }

    public virtual void OnPointerExit(PointerEventData pointerEventData)
    {
    }
    #endregion
}
