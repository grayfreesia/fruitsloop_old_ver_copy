﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockDelay : Block {
	public Text delayNum;
	
	public override void Execute()
	{
		Execution.Instance.delayCoroutine = Delay();		
		Execution.Instance.StartCoroutine (Execution.Instance.delayCoroutine);
	}


	public override string UploadData()
	{
		string time = ((int)(float.Parse(delayNum.text) * 10)).ToString();
		return "5_" + time + "_";
	}
	IEnumerator Delay()
	{		
		Execution.Instance.intervalTime += float.Parse(delayNum.text);
		yield return new WaitForSeconds(Execution.Instance.intervalTime - float.Parse(delayNum.text));
		Execution.Instance.intervalTime -= float.Parse(delayNum.text);		
	}

	
	
}
