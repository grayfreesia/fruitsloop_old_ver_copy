﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockSensorC : Block
{
    public Text valueLabel;
    public Toggle toggle;
    private int childNum;

    public override void Execute()
    {
        childNum = GetComponentsInChildren<Block>(true).Length - 1;
        if (!Condition())
        {
            Execution.Instance.jumpList[Execution.Instance.blockNum].Add(Execution.Instance.blockNum + childNum);
        }
        // MissionChecker.Instance.SensorCheck();
    }
    
    public override string UploadData()
    {
        string range;
        if (toggle.isOn) {
            // 빛이 밝으면
            range = (int.Parse(valueLabel.text) * 11 + 10).ToString();
        } else {
            // 빛이 어두우면
            range = valueLabel.text;
        }

        // string range = (int.Parse(numbers[0].text) * 11 + int.Parse(numbers[1].text)).ToString();
        return "3_" + range + "_";
    }
    bool Condition()
    {
        if (toggle.isOn) {
            // 빛이 밝으면
            if (SensorC.Instance.value >= int.Parse(valueLabel.text)) {
                return true;
            } else {
                return false;
            }
        } else {
            // 빛이 어두우면
            if (SensorC.Instance.value <= int.Parse(valueLabel.text)) {
                return true;
            } else {
                return false;
            }
        }

        // if (int.Parse(numbers[0].text) <= SensorC.Instance.value && SensorC.Instance.value <= int.Parse(numbers[1].text))
        // {
        //     return true;
        // }
        // else
        // {
        //     return false;
        // }
    }
}
