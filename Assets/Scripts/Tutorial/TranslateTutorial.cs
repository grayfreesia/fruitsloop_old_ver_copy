﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateTutorial : Tutorial {
	private bool initialized = false;
	Timer timer = new Timer(1f);
	public Vector3 cashedPosition;
	public override void CheckIfHappening() {
		if (!initialized) {
			timer.Update(Time.deltaTime);
			if (timer.IsTimeout) {
				cashedPosition = transform.position;
				initialized = true;
				timer.Reset();
			}
			return;
		}

		Vector3 distance = transform.position - cashedPosition;
		if (distance.magnitude >= 3.5f) {
			TutorialManager.Instance.CompletedTutorial();
		}
	}
}
