﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteAllBlockTutorial : Tutorial {
	public void OnPressButton() {
		string title = "후르츠루프";
        string message = "블록 팔레트의 모든 블럭을 삭제할까요?";
        AlertController.Show (title, message, new AlertOptions
        {
            cancelButtonTitle = "아니요",
            okButtonTitle = "삭제", okButtonDelegate = () =>
            {
				MainBlockZone.Instance.DeleteAllImmediately();
				TutorialManager.Instance.CompletedTutorial();
            },
        });
	}
}
