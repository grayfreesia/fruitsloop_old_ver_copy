﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragKitTutorial : Tutorial {

    private KitDraggable cashedKitDraggable;
    private KitDraggable CashedKitDraggable {
        get {
            if (cashedKitDraggable == null) {
                cashedKitDraggable = GetComponent<KitDraggable>();
            }
            return cashedKitDraggable;
        }
    }

    private bool completed = false;

    private void Update() {
        if (TutorialManager.Instance.CurrentTutorial == this) {
            if (CashedKitDraggable.IsImageCreated) {
                TutorialViewController.Instance.HideTutorialView();
                TutorialViewController.Instance.overlay.SetActive(false);
            } else {
                if (!completed) {
                    TutorialViewController.Instance.ShowTutorialView(this);
                    TutorialViewController.Instance.overlay.SetActive(true);
                }
            }
        }
    }

	public override void CheckIfHappening() {
        if (KitManager.Instance.slots[CashedKitDraggable.kitNum].isAssembled && !completed) {
            completed = true;
            string title = "후르츠루프!";
            string message = "잘했어요! 블록코딩하기 탭으로 넘어가볼까요?";
            AlertController.Show(title, message, new AlertOptions {
                cancelButtonTitle = "블록코딩하기", cancelButtonDelegate = () => {
                    CourseState.Instance.CurrentSession = CourseState.Session.BlockCoding;
                    TutorialManager.Instance.CompletedTutorial();
                },
                animate = false,
            });
        }
    }
}
