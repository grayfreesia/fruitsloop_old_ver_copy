﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class DraggableAssemblyPartTutorial : Tutorial, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    #region Fields
    public GameObject prefabToDrag;
    public DroppableAssemblyPartTutorial target;
    [SerializeField] private Image raycastImage;

    private GameObject draggingObject;
    private Timer timer = new Timer(0.1f);
    private bool isPointerDown = false;
    private PointerEventData cashedPointerEventData;
    private bool isObjectCreated = false;
    #endregion

    #region Properties
    private Camera cachedCamera;
    public Camera CachedCamera
    {
        get
        {
            if (cachedCamera == null)
            {
                cachedCamera = Camera.main;
            }
            return cachedCamera;
        }
    }

    #endregion

    #region Unity Methods
    private void Update()
    {
        if (isPointerDown)
        {
            // 블록 생성 버튼 다운시 타이머를 잰다.
            timer.Update(Time.deltaTime);
            if (timer.IsTimeout)
            {
                // 지정된 타이머 시간이 지나면
                // 블록 생성
                CreateDraggingObject(cashedPointerEventData);
                isObjectCreated = true;
                raycastImage.raycastTarget = false;

                // 타이머 초기화
                isPointerDown = false;
                timer.Reset();

                // 진동
                Vibration.Vibrate(40);
            }
        }

        if (TutorialManager.Instance.CurrentTutorial == this) {
            if (isObjectCreated) {
                TutorialViewController.Instance.HideTutorialView();
                TutorialViewController.Instance.overlay.SetActive(false);
            } else {
                TutorialViewController.Instance.ShowTutorialView(this);
                TutorialViewController.Instance.overlay.SetActive(true);
            }
        }
    }
    #endregion

    #region Private Methods
    private void CreateDraggingObject(PointerEventData pointerEventData)
    {
        if (draggingObject != null)
        {
            Destroy(draggingObject);
        }

        draggingObject = Instantiate(prefabToDrag);
        draggingObject.transform.rotation = target.body.transform.rotation;
        draggingObject.transform.localScale = target.body.transform.lossyScale;
        SetActiveTargetBoundary(true);
        UpdateDraggingObjectPos(pointerEventData);
    }

    private void SetActiveTargetBoundary(bool on)
    {
		target.Boundary.enabled = on ? !target.body.activeSelf : false;
    }

    private void UpdateDraggingObjectPos(PointerEventData pointerEventData)
    {
        Vector2 screenPos = pointerEventData.position;
        Vector3 newPos = CachedCamera.ScreenToWorldPoint(new Vector3(screenPos.x, screenPos.y, 10f));
        draggingObject.transform.position = newPos;
    }
    #endregion

    #region Event Handlers
    public void OnPointerDown(PointerEventData pointerEventData)
    {
        isPointerDown = true;
        cashedPointerEventData = pointerEventData;
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        isPointerDown = false;
        timer.Reset();
        if (draggingObject != null)
        {
            Destroy(draggingObject);
            isObjectCreated = false;
            raycastImage.raycastTarget = true;
            SetActiveTargetBoundary(false);
        }
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        isPointerDown = false;
        timer.Reset();
    }

    public void OnBeginDrag(PointerEventData pointerEventData)
    {
        if (!isObjectCreated)
        {
        }
    }

    public void OnDrag(PointerEventData pointerEventData)
    {
        if (isObjectCreated)
        {
            UpdateDraggingObjectPos(pointerEventData);
        }
        else
        {
            if (Mathf.Abs(pointerEventData.delta.y) > 4f)
            {
                isPointerDown = false;
                timer.Reset();
            }
        }
    }

    public void OnEndDrag(PointerEventData pointerEventData)
    {
    }
    #endregion
}
