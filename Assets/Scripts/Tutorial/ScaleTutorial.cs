﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleTutorial : Tutorial {
	public override void CheckIfHappening() {
		if (transform.localScale.x >= 50f) {
			TutorialManager.Instance.CompletedTutorial();
		}
	}
}
