﻿using UnityEngine;

public class DragCodingblockTutorial : Tutorial {

	private CreatorBlock cashedCreatorBlock;
	private CreatorBlock CashedCreatorBlock {
		get {
			if (cashedCreatorBlock == null) {
				cashedCreatorBlock = GetComponent<CreatorBlock>();
			}
			return cashedCreatorBlock;
		}
	}

	private MainBlockZone cashedMainblockZone;
	private MainBlockZone CashedMainblockZone {
		get {
			if (cashedMainblockZone == null) {
				cashedMainblockZone = FindObjectOfType<MainBlockZone>();
			}
			return cashedMainblockZone;
		}
	}

	private void Update() {
		if (TutorialManager.Instance.CurrentTutorial == this) {
			if (CashedCreatorBlock.IsDraggingBlockCreated) {
				TutorialViewController.Instance.HideTutorialView();
				TutorialViewController.Instance.overlay.SetActive(false);
			} else {
				TutorialViewController.Instance.ShowTutorialView(this);
				TutorialViewController.Instance.overlay.SetActive(true);
			}
		}
	}

	public override void CheckIfHappening() {
		if (CashedCreatorBlock.IsBlockCreated) {
			var tutorial = CashedMainblockZone.GetComponent<DragCreatedBlockTutorial>();
			if (tutorial && tutorial.FocusingObject == null) {
				var createdBlock = CashedMainblockZone.CachedScrollRect.content.GetComponentInChildren<CreatedBlock>();
				tutorial.createdBlock = createdBlock;
				tutorial.FocusingObject = createdBlock.gameObject;
				tutorial.ActiveNextTutorialButton = true;
				tutorial.NextTutorialButtonPosition = new Vector2(0f, -670f);
			}
			TutorialManager.Instance.CompletedTutorial();
			CashedCreatorBlock.IsBlockCreated = false;
		}
	}
}
