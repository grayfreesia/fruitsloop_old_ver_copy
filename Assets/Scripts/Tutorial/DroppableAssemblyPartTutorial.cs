﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DroppableAssemblyPartTutorial : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDropHandler
{
	#region Fields
	public GameObject body;
	public GameObject guide;
	#endregion
	
	#region Properties
	private Collider boundary;
	public Collider Boundary
	{
		get
		{
			if (boundary == null)
			{
				boundary = GetComponent<Collider>();
			}
			return boundary;
		}
	}
	#endregion

	#region Unity Methods
	private void Awake()
	{
		Boundary.enabled = false;
	}
	#endregion

	#region Event Handlers
	public void OnPointerEnter (PointerEventData pointerEventData)
	{
		if (pointerEventData.dragging)
		{
			guide.SetActive (true);
		}
	}

	public void OnPointerExit (PointerEventData pointerEventData)
	{
		if (pointerEventData.dragging)
		{
			guide.SetActive (false);
		}
	}

	public void OnDrop (PointerEventData pointerEventData)
	{
		guide.SetActive (false);
		body.SetActive (true);
		
		TutorialManager.Instance.CompletedTutorial();
	}
	#endregion
}
