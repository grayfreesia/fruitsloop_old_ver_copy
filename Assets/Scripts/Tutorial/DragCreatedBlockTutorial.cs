﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragCreatedBlockTutorial : Tutorial {

	public CreatedBlock createdBlock;

	private void Update() {
		if (TutorialManager.Instance.CurrentTutorial == this) {
			if (createdBlock) {
				if (createdBlock.IsDraggingBlockCreated) {
					TutorialViewController.Instance.HideTutorialView();
					TutorialViewController.Instance.overlay.SetActive(false);
				} else {
					TutorialViewController.Instance.ShowTutorialView(this);
					TutorialViewController.Instance.overlay.SetActive(true);
				}
			}
		}
	}

	public override void CheckIfGotoBefore() {
		if (TutorialViewController.Instance.focusingObjectParent.childCount <= 0 && TutorialViewController.Instance.FocusingObjectCreated) {
			TutorialManager.Instance.SetBeforeTutorial();
		}
	}
}
