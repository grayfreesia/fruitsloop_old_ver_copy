﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour {
	public enum Type { BasicView, TouchGuideView }
	public Type type = Type.BasicView;

	public int Order;
	[TextArea(3, 10)]
	public string Explanation;
	public GameObject FocusingObject;
	public Vector2 TextPosition;
	public bool ActiveNextTutorialButton;
	public Vector2 NextTutorialButtonPosition;
	public System.Action OnPressNextTutorialButtonDelegate;
	public string TriggerName;

	protected virtual void Awake () {
		TutorialManager.Instance.Tutorials.Add(this);
	}	

	public virtual void CheckIfHappening() {

	}

	public virtual void CheckIfGotoBefore() {

	}
}
