﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitButtonTutorial : Tutorial {
	public EasyTween sidetabEasytween;
	protected override void Awake() {
		base.Awake();
		OnPressNextTutorialButtonDelegate = () => {
			sidetabEasytween.OpenCloseObjectAnimation();
			TutorialManager.Instance.CompletedTutorial();
		};
	}
}
