﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTutorial : Tutorial {
	private bool initialized = false;
	Timer timer = new Timer(1f);
	public Quaternion cashedRotation;
	public override void CheckIfHappening() {
		if (!initialized) {
			timer.Update(Time.deltaTime);
			if (timer.IsTimeout) {
				cashedRotation = transform.rotation;
				initialized = true;
				timer.Reset();
			}
			return;
		}

		if (Quaternion.Angle(cashedRotation, transform.rotation) >= 40f) {
			TutorialManager.Instance.CompletedTutorial();
		}
	}
}
