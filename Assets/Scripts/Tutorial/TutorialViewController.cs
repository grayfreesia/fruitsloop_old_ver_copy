﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialViewController : MonoBehaviour {
	public EasyTween focusViewEasyTween;
	public EasyTween focusRoundEasyTween;
	public GameObject overlay;
	public Text expText;
	public GameObject touchGuideView;
	public Text touchGuideText;
	public Animator touchGuidAnimator;
	public Button nextTutorialButton;
	public Transform focusingObjectParent;

	private Transform defaultParent;
	private Tutorial cachedTutorial;
	private Coroutine repeatFocusRoundCoroutine;


	public bool ActiveRoundNow { get; set; }
	public bool FocusingObjectCreated { get; set; }
	public static TutorialViewController Instance { get; private set; }


	private void Awake() {
		if (Instance == null) {
			Instance = this;
		}
	}

	public void ShowTutorialView(Tutorial tutorial) {
		switch (tutorial.type) {
			case Tutorial.Type.BasicView: {
				if (!ActiveRoundNow) {
					ActiveRoundNow = true;

					if (cachedTutorial != tutorial) {
						cachedTutorial = tutorial;
						UpdateContent(tutorial);
					}

					focusViewEasyTween.OpenCloseObjectAnimation();

					if (repeatFocusRoundCoroutine != null) {
						StopCoroutine(repeatFocusRoundCoroutine);
					}
					repeatFocusRoundCoroutine = StartCoroutine(RepeatFocusRound());
				}
				break;
			}
			case Tutorial.Type.TouchGuideView: {
				if (cachedTutorial != tutorial) {
					cachedTutorial = tutorial;
					UpdateTouchGuideContent(tutorial);
					touchGuideView.SetActive(true);
					touchGuidAnimator.SetTrigger(tutorial.TriggerName);
					overlay.SetActive(false);
				}
				break;
			}
			default: {
				Debug.Log("There is no tutorial type.");
				break;
			}
		}
	}

	public void HideTutorialView() {
		switch (cachedTutorial.type) {
			case Tutorial.Type.BasicView: {
				if (ActiveRoundNow) {
					focusViewEasyTween.OpenCloseObjectAnimation();
					ActiveRoundNow = false;
					expText.gameObject.SetActive(false);
					nextTutorialButton.gameObject.SetActive(false);
				}
				break;
			}
			case Tutorial.Type.TouchGuideView: {
				touchGuidAnimator.SetTrigger("Idle");
				touchGuideView.SetActive(false);
				overlay.SetActive(true);
				break;
			}
			default: {
				Debug.Log("There is no tutorial type.");
				break;
			}
		}
	}

	IEnumerator RepeatFocusRound() {
		Timer timer = new Timer(1f);
		while (true) {
			if (ActiveRoundNow) {
				timer.Update(Time.deltaTime);
				if (timer.IsTimeout) {
					focusRoundEasyTween.OpenCloseObjectAnimation();
					timer.Reset();
				}
			} else {
				yield break;
			}
			yield return null;
		}
	}

	public void ResetFocusingObject() {
		if (cachedTutorial.FocusingObject) {
			cachedTutorial.FocusingObject.transform.SetParent(defaultParent);
		}
		FocusingObjectCreated = false;
	}

	private void UpdateContent(Tutorial tutorial) {
		// focusView의 위치 업데이트
		focusViewEasyTween.rectTransform.anchoredPosition = ConvertWorldPosToRectPos(tutorial.FocusingObject.transform.position);

		// Text 내용 업데이트
		expText.rectTransform.anchoredPosition = tutorial.TextPosition;
		expText.text = tutorial.Explanation;

		// focusing 오브젝트 복사해서 옮김
		defaultParent = tutorial.FocusingObject.transform.parent;
		tutorial.FocusingObject.transform.SetParent(focusingObjectParent);
		FocusingObjectCreated = true;

		// 다음으로 넘어가기 버튼이 있으면 갱신
		if (tutorial.ActiveNextTutorialButton) {
			RectTransform rectTrans = nextTutorialButton.transform as RectTransform;
			rectTrans.anchoredPosition = tutorial.NextTutorialButtonPosition;
		}
	}

	private void UpdateTouchGuideContent(Tutorial tutorial) {
		touchGuideText.text = tutorial.Explanation;
	}

	public void ExitAnimationAction() {
	}

	public void IntroAnimationAction() {
		expText.gameObject.SetActive(true);
		if (cachedTutorial.ActiveNextTutorialButton) {
			nextTutorialButton.gameObject.SetActive(true);
		}
	}

	public void OnPressNextTutorialButton() {
		if (cachedTutorial.OnPressNextTutorialButtonDelegate != null) {
			cachedTutorial.OnPressNextTutorialButtonDelegate.Invoke();
		} else {
			TutorialManager.Instance.CompletedTutorial();
		}
	}

	private Vector2 ConvertWorldPosToRectPos(Vector3 worldPos) {
		RectTransform canvasRect = transform as RectTransform;
		return worldPos / canvasRect.localScale.x;
	}
}
