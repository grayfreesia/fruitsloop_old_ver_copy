﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour {

	public List<Tutorial> Tutorials = new List<Tutorial>();

	public TutorialViewController tutorialViewController;

	private static TutorialManager instance;
	public static TutorialManager Instance {
		get {
			if (instance == null) {
				instance = FindObjectOfType<TutorialManager>();
			}

			if (instance == null) {
				Debug.LogWarning("There is no TutorialManager.");
			}
			return instance;
		}
	}

	private Tutorial currentTutorial;
	public Tutorial CurrentTutorial {
		get { return currentTutorial; }
		set { currentTutorial = value; }
	}

	#region Unity Methods
	private void Start() {
		StartCoroutine(SetNextTutorial(0));
	}

	private void Update() {
		if (currentTutorial) {
			currentTutorial.CheckIfHappening();
			currentTutorial.CheckIfGotoBefore();
		}
	}
	#endregion

	public void CompletedTutorial() {
		tutorialViewController.HideTutorialView();
		tutorialViewController.ResetFocusingObject();
		StartCoroutine(SetNextTutorial(currentTutorial.Order + 1));
	}

	public void SetBeforeTutorial() {
		tutorialViewController.HideTutorialView();
		tutorialViewController.ResetFocusingObject();
		StartCoroutine(SetNextTutorial(currentTutorial.Order - 1));
	}

	IEnumerator SetNextTutorial(int currentOrder) {
		currentTutorial = GetTutorialByOrder(currentOrder);

		yield return new WaitForSeconds(0.3f);
		if (!currentTutorial) {
			CompletedAllTutorials();
			yield break;
		}

		tutorialViewController.ShowTutorialView(currentTutorial);
	}

	public void ShowReallyExitTutorialAlertView() {
		string title = "후르츠루프";
		string message = "정말로 튜토리얼을 종료하고 홈으로 가시겠어요?";
		AlertController.Show(title, message, new AlertOptions {
			cancelButtonTitle = "아니요",
			okButtonTitle = "네", okButtonDelegate = () => {
				//정보 저장
				PlayerPrefs.SetInt("Tutorial", 1);
				SceneController.Instance.LoadLobbyScene();
			},
			animate = false,
			ignoreOpenedAlertView = true,
		});
	}


	public void CompletedAllTutorials() {
		string title = "후르츠루프!";
		string message = "잘했어요! 튜토리얼을 모두 마쳤어요! 홈으로 돌아가 다른 코스를 진행해 보세요.";
		AlertController.Show(title, message, new AlertOptions {
			okButtonTitle = "홈으로", okButtonDelegate = () => {
				// 정보 저장
				PlayerPrefs.SetInt("Tutorial", 1);
				SceneController.Instance.LoadLobbyScene();
			},
			animate = false,
			ignoreOpenedAlertView = true,
		});
	}

	public Tutorial GetTutorialByOrder(int order) {
		for (int i = 0; i < Tutorials.Count; i++) {
			if (Tutorials[i].Order == order) {
				return Tutorials[i];
			}
		}
		return null;
	}
}
