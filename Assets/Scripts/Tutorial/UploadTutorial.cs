﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UploadTutorial : Tutorial {
	protected override void Awake() {
		base.Awake();
		OnPressNextTutorialButtonDelegate = () => {
			string title = "후르츠루프!";
            string message = "잘했어요! 모델 조립하기 탭으로 넘어가볼까요?";
            AlertController.Show(title, message, new AlertOptions {
                cancelButtonTitle = "모델 조립하기", cancelButtonDelegate = () => {
                    CourseState.Instance.CurrentSession = CourseState.Session.Assembly;
                    TutorialManager.Instance.CompletedTutorial();
                },
                animate = false,
            });
		};
	}
}
