﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtonTutorial : Tutorial {
	public EasyTween sidetabEasytween;

	public void OnPressButton() {
		if (TutorialManager.Instance.CurrentTutorial == this) {
			sidetabEasytween.OpenCloseObjectAnimation();
			TutorialManager.Instance.CompletedTutorial();
		}
	}
}
