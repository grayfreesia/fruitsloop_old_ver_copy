﻿using UnityEngine;
using UnityEngine.UI;

public class Popup : MonoBehaviour {
	// 팝업창을 제외한 바탕화면의 색
	[SerializeField] private Color backgroundColor = new Color(10.0f / 255.0f, 10.0f / 255.0f, 10.0f / 255.0f, 0.6f);

	private GameObject background;

	// 팝업 내용을 갱신하는 메서드
	public virtual void UpdateContent(object popupData) {
		// 실제 구현은 상속받은 클래스에서 한다.
	}

	public virtual void Open() {
		AddBackground();
	}

	// 팝업창의 close 버튼을 누르면 호출되는 메서드
	public virtual void Close() {
		var animator = GetComponent<Animator>();
		if (animator.GetCurrentAnimatorStateInfo(0).IsName("Open"))
			animator.Play("Close");

		RemoveBackground();
	}

	public virtual void CloseNotAnim() {
		DestroyPopup();
		RemoveBackground();
	}

	// 애니메이션 이벤트에 의해 호출된다.
	public void DestroyPopup() {
		Destroy(background);
		Destroy(gameObject);
	}

	// 팝업을 제외한 배경에 검은색 바탕화면을 깐다.
	public void AddBackground() {
		var bgTex = new Texture2D(1, 1);
		bgTex.SetPixel(0, 0, backgroundColor);
		bgTex.Apply();

		background = new GameObject("PopupBackground");
		var image = background.AddComponent<Image>();
		var rect = new Rect(0, 0, bgTex.width, bgTex.height);
		var sprite = Sprite.Create(bgTex, rect, new Vector2(0.5f, 0.5f), 1);
		image.material.mainTexture = bgTex;
		image.sprite = sprite;
		var newColor = image.color;
		image.color = newColor;
		image.canvasRenderer.SetAlpha(0.0f);
		image.CrossFadeAlpha(1.0f, 0.4f, false);

		var canvas = GetComponentInParent<Canvas>();
		background.transform.localScale = new Vector3(1, 1, 1);
		background.GetComponent<RectTransform>().sizeDelta = canvas.GetComponent<RectTransform>().sizeDelta;
		background.transform.SetParent(canvas.transform, false);
		background.transform.SetSiblingIndex(transform.GetSiblingIndex());
	}

	// 바탕화면을 제거한다.
	public void RemoveBackground() {
		var image = background.GetComponent<Image>();
		if (image != null)
			image.CrossFadeAlpha(0.0f, 0.2f, false);
	}
}
