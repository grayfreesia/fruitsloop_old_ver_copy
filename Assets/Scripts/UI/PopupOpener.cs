﻿using UnityEngine;

public class PopupOpener : MonoBehaviour {

	// 오픈할 팝업창의 prefab
	public GameObject popupPrefab;

	// 가장 부모 캔버스
	protected Canvas rootCanvas;

	[HideInInspector] public Popup popup;

	protected void Start () {
		rootCanvas = GetComponentInParent<Canvas>();
	}

	// 연결된 팝업창 프리팹의 팝업창을 연다.
	public virtual void OpenPopup() {
		if (popup != null) {
			Destroy(popup);
		}

		popup = Instantiate(popupPrefab).GetComponent<Popup>();
		popup.transform.localScale = Vector3.zero;
		popup.transform.SetParent(rootCanvas.transform, false);
		popup.Open();
		popup.gameObject.SetActive(true);
	}

	public virtual void OpenPopupNotAnim() {
		if (popup != null) {
			Destroy(popup);
		}
		
		popup = Instantiate(popupPrefab).GetComponent<Popup>();
		popup.transform.SetParent(rootCanvas.transform, false);
		popup.Open();
		popup.gameObject.SetActive(true);
	}
}
