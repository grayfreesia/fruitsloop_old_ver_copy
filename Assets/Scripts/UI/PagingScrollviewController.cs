﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

[RequireComponent(typeof(ScrollRect))]
public class PagingScrollviewController : ViewController, IBeginDragHandler, IEndDragHandler {

	private ScrollRect cachedScrollRect;
	public ScrollRect CachedScrollRect {
		get {
			if (cachedScrollRect == null) {
				cachedScrollRect = GetComponent<ScrollRect>();
			}
			return cachedScrollRect;
		}
	}

	private GridLayoutGroup cachedGridLayoutGroup;
	public GridLayoutGroup CachedGridLayoutGroup {
		get {
			if (cachedGridLayoutGroup == null) {
				cachedGridLayoutGroup = CachedScrollRect.content.GetComponent<GridLayoutGroup>();
			}
			return cachedGridLayoutGroup;
		}
	}

	public int ItemCount {
		get {
			return CachedScrollRect.content.childCount;
		}
	}

	// 스냅 스피드
	[Range(5f,25f)]
	[SerializeField] private float scrollSpeed = 15f;

	// 자동 스냅 종료 시간
	[Range(0.1f,1f)]
	[SerializeField] private float autoSnapTime = 0.3f;

	// 무한 스크롤 플래그 (사용자 설정)
	[SerializeField] private bool infiniteScroll = false;

	// 이전 버튼
	[SerializeField] private SimpleButton prevButton;

	// 다음 버튼
	[SerializeField] private SimpleButton nextButton;

	// 페이지 컨트롤러
	[SerializeField] private PageControl pageControl;

	#region Field variable
	// 자동 스냅 시간 타이머
	private float timer;
	// 자동 스냅 중임을 나타내는 플래그
	private bool isAutoSnaping = false;
	// 최종적인 스크롤 위치
	private Vector2 destPosition;
	// 이전 페이지의 인덱스 저장
	private int prevPageIndex;
	// 페이지 가로 길이
	private float pageWidth;
	#endregion

	private void Start() {
		// 페이지 컨트롤러가 있으면 페이지를 설정한다.
		if (pageControl != null) {
			pageControl.SetNumberOfPages(ItemCount);
			pageControl.SetCurrentPage(0);
		}

		// 페이지가 하나 밖에 없고, 버튼이 있으면 버튼을 비활성화 한다.
		if (ItemCount <= 1 &&
			prevButton != null && nextButton != null) {
				prevButton.SetActive(false);
				nextButton.SetActive(false);
		}

		// 페이지의 가로 길이를 정한다.
		pageWidth = -(CachedGridLayoutGroup.cellSize.x + CachedGridLayoutGroup.spacing.x);

		// 무한 스크롤의 경우 두 개 이상일 경우 무한 스크롤 값을 초기화한다.
		if (infiniteScroll && ItemCount > 1) {
			InfiniteScrollInit();
		} else {
			infiniteScroll = false;
		}

		// 처음 보여줄 페이지를 정한다.
		SetPage(0);
	}

	// 이벤트 리스너 등록
	protected virtual void OnEnable() {
		// 버튼 클릭 이벤트 등록
		if (prevButton != null && nextButton != null) {
			prevButton.onClicked.AddListener(() => OnSnapButtonClick(false));
			nextButton.onClicked.AddListener(() => OnSnapButtonClick(true));
		}
	}

	// 이벤트 리스너 해제
	protected virtual void OnDisable() {
		// 무한 스크롤의 경우 ScrollRect의 이벤트 해제
		if (infiniteScroll) {
			CachedScrollRect.onValueChanged.RemoveAllListeners();
		}
		// 버튼 클릭 이벤트 해제
		if (prevButton != null && nextButton != null) {
			prevButton.onClicked.RemoveAllListeners();
			nextButton.onClicked.RemoveAllListeners();
		} 
	}

	// 프레임마다 Update 메서드 다음에 호출된다.
	private void LateUpdate() {
		if (isAutoSnaping) {
			timer += Time.deltaTime;
			CachedScrollRect.content.anchoredPosition =
				Vector2.Lerp(CachedScrollRect.content.anchoredPosition, destPosition, Time.deltaTime * scrollSpeed);
			
			// 자동 스냅 시간이 지나면 무조건 스냅을 멈춘다.
			if (timer >= autoSnapTime) {
				CachedScrollRect.content.anchoredPosition = destPosition;
				isAutoSnaping = false;
			}
		}
	}

	#region 페이지 변경시 이벤트 발생
	// 페이지 변경 이벤트 발생시 전달할 변수 클래스
	public class PageChangedEventArgs : EventArgs {
		public readonly int pageIndex;

		public PageChangedEventArgs(int pageIndex) {
			this.pageIndex = pageIndex;
		}
	}

	// 페이지 변경 이벤트 핸들러
	protected event EventHandler<PageChangedEventArgs> PageChanged;
	protected virtual void OnPageChanged (PageChangedEventArgs e) {
		var handler = PageChanged;
		if (handler != null) {
			handler (this, e);
		}
	}
	#endregion

	// 드래그 조작이 시작될 때 호출된다.
	public virtual void OnBeginDrag(PointerEventData pointerEventData) {
		// 자동스냅 중임을 나타내는 플래그를 초기화한다.
		isAutoSnaping = false;
	}

	// 드래그 조작이 끝날 때 호출된다.
	public virtual void OnEndDrag(PointerEventData pointerEventData) {
		// 스크롤 뷰가 현재 움직이고 있는 것을 멈춘다.
		CachedScrollRect.StopMovement();

		// 현재 스크롤 위치를 토대로 페이지의 인덱스를 계산한다.
		int pageIndex =
			Mathf.RoundToInt((CachedScrollRect.content.anchoredPosition.x) / pageWidth);

		if (pageIndex == prevPageIndex && Mathf.Abs(pointerEventData.delta.x) >= 4f) {
            // 일정 속도 이상으로 드래그를 조작하고 있을 때는 그 방향으로 한 페이지 넘긴다.
            CachedScrollRect.content.anchoredPosition += new Vector2(pointerEventData.delta.x, 0f);
            pageIndex += (int)Mathf.Sign(-pointerEventData.delta.x);
        }

		// 무한 페이지 스크롤이 아니면
		// 첫 페이지이거나 마지막 페이지일 때 그 이상 스크롤 하지 않는다.
		if (!infiniteScroll) {
			if (pageIndex < 0) {
				pageIndex = 0;
			} else if (pageIndex >= ItemCount-1) {
				pageIndex = ItemCount-1;
			}
		}

		// 현재 페이지의 인덱스를 저장해둔다.
		prevPageIndex = pageIndex;

		SetPage(pageIndex);
	}

	private void SetPage(int pageIndex) {
		// 최종적인 스크롤 위치를 계산한다.
		float dextX = pageIndex * pageWidth;

		// 페이지에 해당하는 목표 벡터를 계산한다.
		destPosition = new Vector2(dextX, CachedScrollRect.content.anchoredPosition.y);

		// 자동스냅 중임을 나타내는 플래그를 설정한다.
		isAutoSnaping = true;
		timer = 0f;

		// 페이지 변경 이벤트 발생
		OnPageChanged(new PageChangedEventArgs(pageIndex));

		// 페이지 컨트롤러가 있으면 페이지 컨트롤러를 갱신한다.
		if (pageControl != null) {
			pageControl.SetCurrentPage(pageIndex);
		}

		// 스냅 버튼이 있고 무한 스크롤이 아닐 경우 UI를 업데이트 한다.
		if (!infiniteScroll && prevButton != null && nextButton != null) {
			UpdateSnapButtonUI(pageIndex);
		}
	}

	#region 스냅 버튼을 사용했을 경우 기능 구현

	// 스냅 버튼을 클릭했을때 호출되는 메서드
	private void OnSnapButtonClick(bool next) {
		if (next) {
			// Next page 스냅
			SetPage(++prevPageIndex);
		} else {
			// Prev page 스냅
			SetPage(--prevPageIndex);
		}
	}

	// 버튼의 UI를 갱신하는 메서드
	private void UpdateSnapButtonUI(int pageIndex) {
		if (pageIndex <= 0) {
			prevButton.EnableInteraction(false);
			nextButton.EnableInteraction(true);
		} else if (pageIndex >= ItemCount-1) {
			prevButton.EnableInteraction(true);
			nextButton.EnableInteraction(false);
		} else {
			prevButton.EnableInteraction(true);
			nextButton.EnableInteraction(true);
		}
	}
	#endregion

	#region Infinite scroll 기능을 구현

	private float disableMarginX = 0f;
	private float treshold = 100f;
	private float offsetX = 0f;
	private Vector2 newAnchoredPosition = Vector2.zero;
	private List<RectTransform> items = new List<RectTransform>();

	private bool hasDisabledGridComponents = false;

	private void InfiniteScrollInit() {
		// 무한 스크롤의 경우 ScrollRect에 이벤트 등록
		CachedScrollRect.onValueChanged.AddListener(OnScroll);

		CachedScrollRect.movementType = ScrollRect.MovementType.Unrestricted;

		for (int i = 0; i < ItemCount; i++) {
			items.Add(CachedScrollRect.content.GetChild(i).GetComponent<RectTransform>());
		}
	}

	// grid layout 을 작동시키지 않는다.
	private void DisableGridComponents() {
		offsetX = -pageWidth;
		disableMarginX = offsetX * ItemCount / 2;
		CachedGridLayoutGroup.enabled = false;

		hasDisabledGridComponents = true;
	}
	
	// 현재 scroll rect의 위치에 따라 item의 위치를 옮기는 구현
	protected virtual void OnScroll(Vector2 pos) {
		if (!hasDisabledGridComponents) {
			DisableGridComponents();
		}

		for (int i = 0; i < ItemCount; i++) {
			if (CachedScrollRect.transform.InverseTransformPoint(items[i].position).x > disableMarginX + treshold) {
				newAnchoredPosition = items[i].anchoredPosition;
				newAnchoredPosition.x -= ItemCount * offsetX;
				items[i].anchoredPosition = newAnchoredPosition;
				CachedScrollRect.content.GetChild(ItemCount - 1).transform.SetAsFirstSibling();
			}
			else if (CachedScrollRect.transform.InverseTransformPoint(items[i].position).x < -disableMarginX) {
				newAnchoredPosition = items[i].anchoredPosition;
				newAnchoredPosition.x += ItemCount * offsetX;
				items[i].anchoredPosition = newAnchoredPosition;
				CachedScrollRect.content.GetChild(0).transform.SetAsLastSibling();
			}
		}
	}
	#endregion
}
