﻿using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SimpleButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {
	public bool interactable = true;
	public float onClickAlpha = 0.6f;

	[Serializable] public class ButtonClickedEvent : UnityEvent { }
	public ButtonClickedEvent onClicked = new ButtonClickedEvent();

	protected bool isPointerDown = false;
	protected CanvasGroup canvasGroup;

	protected virtual void Awake() {
		canvasGroup = GetComponent<CanvasGroup>();
		if (canvasGroup == null) {
 			canvasGroup = gameObject.AddComponent<CanvasGroup>();
		}
	}

	#region Event Handlers
	public virtual void OnPointerDown(PointerEventData eventData) {
		if (interactable) {
			isPointerDown = true;
			canvasGroup.alpha = onClickAlpha;
		}
	}

	public virtual void OnPointerUp(PointerEventData eventData) {
		if (interactable) {
			if (isPointerDown) {
				// 버튼 실행
				onClicked.Invoke();
			}
			canvasGroup.alpha = 1f;
		}
	}

	public virtual void OnPointerExit(PointerEventData eventData) {
		if (interactable) {
			isPointerDown = false;
			canvasGroup.alpha = 1f;
		}
	}
	#endregion

	// 버튼의 인터랙션을 유효화/무효화하는 메서드
	public virtual void EnableInteraction(bool isEnabled) {
		canvasGroup.blocksRaycasts = isEnabled;
		interactable = isEnabled ? true : false;
		canvasGroup.alpha = isEnabled ? 1f : 0.4f;
	}

	// 버튼의 활성화/비활성화하는 메서드
	public virtual void SetActive(bool isActive) {
		canvasGroup.blocksRaycasts = isActive;
		interactable = isActive ? true : false;
		canvasGroup.alpha = isActive ? 1f : 0f;
	}

	// 버튼의 하이라이트/비하이라이트하는 메서드
	public virtual void SetHighlight(bool isHighlighted) {

	}
}
