﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonInsideScroll : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {

	private ScrollRect scrollRect;

	private void Start () {
		scrollRect = GetComponentInParent<ScrollRect>();
	}

	void IPointerDownHandler.OnPointerDown(PointerEventData eventData) {
		if (scrollRect != null) {
			scrollRect.StopMovement();
			scrollRect.enabled = false;
		}
	}

	void IPointerUpHandler.OnPointerUp(PointerEventData eventData) {
		if (scrollRect != null && !scrollRect.enabled) {
			scrollRect.enabled = true;
		}
	}

	void IDragHandler.OnDrag(PointerEventData eventData) {
		// nothing. just block scroll
	}
}
